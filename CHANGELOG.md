# Changelog
All notable changes to this package will be documented in this file.

## [0.1.0] - 2022-03-15
### Added already existing features accumulated during the past years. 

## [0.1.1] - 2022-03-16
### Added new operations for Vectors and Lists. 
### New class for Primitives and Mesh operations.

## [0.1.2] - 2022-03-24
### Added examples for SensingComponents' detectables.
### Releaseable version.