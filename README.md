# About Custom Utilities

Use Barbaragno's Custom Utilities to speed up and optimize your work.

# Installing Custom Utilities

To install this package, follow the instructions in the [Package Manager documentation](https://docs.unity3d.com/Packages/com.unity.package-manager-ui@latest/index.html).

# Using Custom Utilities

# Technical details

## Requirements

This version of Custom Utilities is compatible with the following versions of the Unity Editor:

* 2019.4 and later (recommended)

## Package contents

The following table indicates the folder structure of the Sprite package:

|Location|Description|
|---|---|
|`<Runtime>`|Root folder containing the source for the Custom Utilities.|
|`<AI>`|Folder containing SensingComponents' source code and configuration examples for AI.|
|`<Audio>`|Folder containing audio management scripts.|
|`<EventHandlerSO>`|Folder containing ScriptableObject event handler templates .|
|`<Operations>`|Folder containing a bunch of utility functions (Math, Vectors, Lists, Curves, Meshes, Primitives).|
|`<Serializable>`|Folder containing serializable type presets (Dictionaries, Lists, Tuples).|
|`<Singleton Template>`|Folder containing singleton templates.|

## Document revision history

|Date|Reason|
|---|---|
|March 15, 2022|Document created. Matches package version 0.1.0|
|March 16, 2022|Document updated. Added new utility scripts. Matches package version 0.1.1|
|March 24, 2022|Document updated. Releasable version. Matches package version 0.1.2|