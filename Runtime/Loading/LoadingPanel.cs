using UnityEngine;
using UnityEngine.UI;

public class LoadingPanel : LoadingSceneListener {
    [Header("References")]
    [SerializeField] private Image image;

    protected override void OnLoadingScene(float progress) {
        image.fillAmount = progress;
        Debug.Log($"Loading: {progress}");
    }

    protected override void OnLoadSceneBegin(SceneTypes scene) {
        image.fillAmount = 0;
        gameObject.SetActive(true);
        Debug.Log($"Loading begin.");
    }

    protected override void OnLoadSceneEnd(SceneTypes scene) {
        image.fillAmount = 1;
        gameObject.SetActive(false);
        Debug.Log($"Loading end.");
    }
}
