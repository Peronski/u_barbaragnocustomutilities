using UnityEngine;
using UpSurgeOn.MetaverseRuntimeRuntime.Singletons;

public abstract class LoadingSceneListener : MonoBehaviour
{
    private bool lateInitialization = false;

    protected void Awake() {
        if (SceneLoader.Instance == null) {
            lateInitialization = true;
            return;
        }

        SceneLoader.Instance.OnLoadSceneBegin += OnLoadSceneBegin;
        SceneLoader.Instance.OnLoadingScene += OnLoadingScene;
        SceneLoader.Instance.OnLoadSceneEnd += OnLoadSceneEnd;
    }

    protected void Start() {
        if (lateInitialization) {
            SceneLoader.Instance.OnLoadSceneBegin += OnLoadSceneBegin;
            SceneLoader.Instance.OnLoadingScene += OnLoadingScene;
            SceneLoader.Instance.OnLoadSceneEnd += OnLoadSceneEnd;
        }
    }

    protected abstract void OnLoadSceneBegin(SceneTypes scene);

    protected abstract void OnLoadingScene(float progress);

    protected abstract void OnLoadSceneEnd(SceneTypes scene);
}
