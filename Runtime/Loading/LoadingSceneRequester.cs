using UnityEngine;
using UnityEngine.SceneManagement;
using UpSurgeOn.MetaverseRuntimeRuntime.Singletons;

namespace UpSurgeOn.MetaverseRuntime.Utilities {
    public class LoadingSceneRequester : MonoBehaviour {
        [Header("Settings")]
        [SerializeField] private SceneTypes sceneToLoad;
        [Space]
        [SerializeField] private LoadSceneMode loadingMode;
        [Space]
        [SerializeField] private bool asyncLoading;
        [Space]
        [SerializeField] private bool unloadCurrent;

        private void Start() {
            SceneLoader.Instance.LoadScene(sceneToLoad, asyncLoading, unloadCurrent, loadingMode);
        }
    }
}
