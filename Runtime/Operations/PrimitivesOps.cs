using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomUtilities.Operations {
    public static class PrimitivesOps {
        #region Mathematical Constants
        public static float TAU = 6.28318530718f;
        #endregion

        #region Primitive Functions
        /// <summary>
        /// Calculates <b>Vector2 points</b> which compose a <i>regolar polygon of tot vertices</i>.
        /// </summary>
        /// <param name="vertices">Number of polygon's vertices.</param>
        /// <returns>List of vector2 points.</returns>
        public static List<Vector2> RegolarPolygonComposer(int vertices) {
            List<Vector2> points = new List<Vector2>();

            for (int i = 0; i < vertices; ++i) {
                float t = i / (float)vertices;
                float angRad = TAU * t;
                float x = Mathf.Cos(angRad);
                float y = Mathf.Sin(angRad);
                Vector3 point = new Vector3(x, y);

                points.Add(point);
            }

            return points;
        }
        #endregion

        #region Draw Primitive Functions
        /// <summary>
        /// Draws a polygon from list of Vector2 points. 
        /// <b>WARNING</b>: use this method in OnDrawGizmos callback. 
        /// </summary>
        /// <param name="points">List of Vector2 points to draw.</param>
        /// <param name="sideToSkip">Number of sides to skip.</param>
        public static void RegolarPolygonDrawer(List<Vector2> points, int sideToSkip = 0) {
            for (int i = 0; i < points.Count; ++i) {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(points[i], points[(int)Mathf.Repeat(i + sideToSkip, points.Count)]);
            }
        }
        #endregion
    }
}
