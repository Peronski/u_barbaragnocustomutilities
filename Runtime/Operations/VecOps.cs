﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomUtilities.Operations
{
    public enum Axis
    {
        X, Y, Z
    }

    public static class VecOps
    {
        #region Mathematical Constants
        public static float TAU = 6.28318530718f;
        #endregion

        #region Vector3 Functions
        /// <summary>
        /// Converts the given 2D vector into a 3D vector that as if it was seen from above.
        /// </summary>
        /// <param name="value">The base vector to convert.</param>
        /// <returns>The 3D vector generated from the given 2D vector.</returns>
        public static Vector3 TopView (this Vector2 value) => new Vector3(value.x, 0, value.y);

        /// <summary>
        /// Flattens a given vector on a given axis.
        /// </summary>
        /// <param name="value">The given vector.</param>
        /// <param name="axis">The given axis.</param>
        /// <returns>The vector with <paramref name="axis"/> zeroed out.</returns>
        public static Vector3 Flatten (this Vector3 value, Axis axis) {
            switch(axis) {
                case Axis.X:
                    value.x = 0f;
                    break;
                case Axis.Y:
                    value.y = 0f;
                    break;
                case Axis.Z:
                    value.z = 0f;
                    break;
                default:
                    break;
            }

            return value;
        }

        /// <summary>
        /// Converts the given vector's direction from World Space to Local Space of the <paramref name="reference"/> vector.
        /// </summary>
        /// <param name="v">The given vector.</param>
        /// <param name="reference">The reference vector for conversion.</param>
        /// <returns>The given vector rotated to match the <paramref name="reference"/>'s vector Local Space.</returns>
        public static Vector3 RelativeTo (this Vector3 v, Vector3 reference) {
            float angle = Vector3.SignedAngle(reference, v, Vector3.up) * Mathf.Deg2Rad;
            Vector3 dest = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * v.magnitude;
            return dest;
        }

        /// <summary>
        /// Rotates the given vector along the given Axis.
        /// </summary>
        /// <param name="v">The given vector.</param>
        /// <param name="axis">The given axis.</param>
        /// <param name="angle">The angle of rotation (in degrees).</param>
        /// <returns>The given vector rotated by <paramref name="angle"/> degrees.</returns>
        public static Vector3 Rotate (this Vector3 v, Axis axis, float angle) {
            /**
             * ASSE Z
             * |cos θ   −sin θ   0| |x|   |x cos θ − y sin θ|   |x'|
             * |sin θ    cos θ   0| |y| = |x sin θ + y cos θ| = |y'|
             * |  0       0      1| |z|   |        z        |   |z'|
             *
             * ASSE Y
             * | cos θ    0   sin θ| |x|   | x cos θ + z sin θ|   |x'|
             * |   0      1       0| |y| = |         y        | = |y'|
             * |−sin θ    0   cos θ| |z|   |−x sin θ + z cos θ|   |z'|
             *
             * ASSE X
             * |1     0           0| |x|   |        x        |   |x'|
             * |0   cos θ    −sin θ| |y| = |y cos θ − z sin θ| = |y'|
             * |0   sin θ     cos θ| |z|   |y sin θ + z cos θ|   |z'|
            */
            float radAngle = angle * Mathf.Deg2Rad;
            float newX, newY, newZ;

            switch(axis) {
                case Axis.X:
                    newY = v.y * Mathf.Cos(radAngle) - v.z * Mathf.Sin(radAngle);
                    newZ = v.y * Mathf.Cos(radAngle) + v.z * Mathf.Sin(radAngle);

                    v = new Vector3(v.x, newY, newZ);
                    break;
                case Axis.Y:
                    newX = v.x * Mathf.Cos(radAngle) + v.z * Mathf.Sin(radAngle);
                    newZ = -v.x * Mathf.Sin(radAngle) + v.z * Mathf.Cos(radAngle);

                    v = new Vector3(newX, v.y, newZ);
                    break;
                case Axis.Z:
                    newX = v.x * Mathf.Cos(radAngle) - v.y * Mathf.Sin(radAngle);
                    newY = v.x * Mathf.Cos(radAngle) + v.y * Mathf.Sin(radAngle);

                    v = new Vector3(newX, newY, v.z);
                    break;
            }

            return v;
        }

        /// <summary>
        /// Rotates the vector based on the given <paramref name="rotation"/>.
        /// </summary>
        /// <param name="v">The given vector.</param>
        /// <param name="rotation">The angles of the rotation (in degrees).</param>
        /// <returns>The rotated vector.</returns>
        public static Vector3 RotateSimple(this Vector3 v, Vector3 rotation) {
            return Quaternion.Euler(rotation) * v;
        }

        /// <summary>
        /// Rotates the vector based on the given <paramref name="rotation"/>.
        /// </summary>
        /// <param name="v">The given vector.</param>
        /// <param name="rotation">The angles of the rotation (in degrees).</param>
        public static void DoRotateSimple(this ref Vector3 v, Vector3 rotation) {
            v = Quaternion.Euler(rotation) * v;
        }

        /// <summary>
        /// Rotates the vector based on the given angles.
        /// </summary>
        /// <returns>The rotated vector.</returns>
        public static Vector3 RotateSimple(this Vector3 v, float x, float y, float z) {
            return Quaternion.Euler(x, y, z) * v;
        }

        /// <summary>
        /// Rotates the vector based on the given angles.
        /// </summary>
        public static void DoRotateSimple(this ref Vector3 v, float x, float y, float z) {
            v = Quaternion.Euler(x, y, z) * v;
        }

        /// <summary>
        /// Calculates the horizontal difference of two points.
        /// </summary>
        /// <param name="p1">First point.</param>
        /// <param name="p2">Second point.</param>
        /// <returns>The delta X.</returns>
        public static float DeltaX (Vector2 p1, Vector2 p2) => (p1.x - p2.x).Abs();

        /// <summary>
        /// Calculates the vertical difference of two points.
        /// </summary>
        /// <param name="p1">First point.</param>
        /// <param name="p2">Second point.</param>
        /// <returns>The delta Y.</returns>
        public static float DeltaY (Vector2 p1, Vector2 p2) => (p1.y - p2.y).Abs();

        /// <summary>
        /// Halfens the given vector.
        /// </summary>
        /// <param name="v2">The vector to cut in half.</param>
        /// <returns>Half the given vector.</returns>
        public static Vector2 Half (this Vector2 v2) => v2 * 0.5f;
        #endregion

        #region Vector2 Functions
        /// <summary>
        /// Retrieves the X sign of the given vector.
        /// </summary>
        /// <param name="value">The given vector.</param>
        /// <returns>The sign as an integer.</returns>
        public static int GetSignX (this Vector2 value) => value.x.SignToInt();

        /// <summary>
        /// Retrieves the Y sign of the given vector.
        /// </summary>
        /// <param name="value">The given vector.</param>
        /// <returns>The sign as an integer.</returns>
        public static int GetSignY (this Vector2 value) => value.y.SignToInt();

        /// <summary>
        /// Sets the X sign of the given vector.
        /// </summary>
        /// <param name="value">The given vector.</param>
        /// <returns>The modified vector.</returns>
        public static Vector2 SetSignX (this Vector2 v, float sign) {
            v.x = v.x.Abs();
            v.x *= sign.Sign();
            return v;
        }

        /// <summary>
        /// Sets the Y sign of the given vector.
        /// </summary>
        /// <param name="value">The given vector.</param>
        /// <returns>The modified vector.</returns>
        public static Vector2 SetSignY (this Vector2 v, float sign) {
            v.y = v.y.Abs();
            v.y *= sign.Sign();
            return v;
        }

        /// <summary>
        /// Retrieves the unit vector which represents the quadrant the given vector lies in.
        /// </summary>
        /// <param name="v">The given vector.</param>
        /// <returns>The quadrant vecotr.</returns>
        public static Vector2Int Quadrant (this Vector2 v) => new Vector2Int(v.x.SignToInt(), v.y.SignToInt());

        /// <summary>
        /// Rotates a <b>Vector2</b> by <i>angle</i> degrees.
        /// </summary>
        /// <param name="v">The vector to rotate.</param>
        /// <param name="angle">The angle rotate the vector of.</param>
        /// <returns>The rotated vector.</returns>
        public static Vector2 Rotate (this Vector2 v, float angle) {
            //x' = x cos θ − y sin θ
            //y' = x sin θ + y cos θ

            float radAngle = angle * Mathf.Deg2Rad;

            float newX = v.x * Mathf.Cos(radAngle) - v.y * Mathf.Sin(radAngle);
            float newY = v.x * Mathf.Sin(radAngle) + v.y * Mathf.Cos(radAngle);

            return new Vector2(newX, newY);
        }

        /// <summary>
        /// Gets the <i>radian angle</i> of <b>Vector2</b>.
        /// </summary>
        /// <param name="v">The vector.</param>
        /// <returns>Radian angle.</returns>
        public static float RadAngle(this Vector2 v) {
            float opposite = v.y;
            float adjacent = v.x;

            return Mathf.Atan2(opposite, adjacent);
        }

        /// <summary>
        /// Gets the <i>degree angle</i> of <b>Vector2</b>.
        /// </summary>
        /// <param name="v">The vector.</param>
        /// <returns>Degree angle.</returns>
        public static float DegAngle(this Vector2 v) {
            float opposite = v.y;
            float adjacent = v.x;

            return Mathf.Atan2(opposite, adjacent) * Mathf.Rad2Deg;
        }

        /// <summary>
        /// Transforms a <i>radians angle</i> in relative <b>Vector2</b>.
        /// </summary>
        /// <param name="radAng">Radians angle.</param>
        /// <returns>Vector2.</returns>
        public static Vector2 AngToDir(float radAng) => new Vector2(Mathf.Cos(radAng), Mathf.Sin(radAng));

        /// <summary>
        /// Converts <b>Vector2</b> from its <i>local position</i> to <i>world position</i>.
        /// </summary>
        /// <param name="v">The vector needs to be transformed.</param>
        /// <param name="orientationX">Reference X axis.</param>
        /// <param name="orientationY">Reference Y axis.</param>
        /// <param name="worldReferencePoint">Position of reference in world space respect to transformation takes place.</param>
        /// <returns>Position in world space.</returns>
        public static Vector2 LocalToWorld(this Vector2 v, Vector2 orientationX, Vector2 orientationY, Vector2 worldReferencePoint) {
            Vector2 localWorldOffset = orientationX * v.x + orientationY * v.y;

            return worldReferencePoint + localWorldOffset;
        }

        /// <summary>
        /// Converts <b>Vector2</b> from its <i>world position</i> to <i>local position</i>.
        /// </summary>
        /// <param name="v">The vector needs to be transformed.</param>
        /// <param name="orientationX">Reference X axis.</param>
        /// <param name="orientationY">Reference Y axis.</param>
        /// <param name="localReferencePoint">Position of reference in world space respect to transformation takes place.</param>
        /// <returns>Position in local space respect local reference point.</returns>
        public static Vector2 WorldToLocal(this Vector2 v, Vector2 orientationX, Vector2 orientationY, Vector2 localReferencePoint) {
            Vector2 worldLocalOffset = localReferencePoint - v;

            return new Vector2(Vector3.Dot(worldLocalOffset, orientationX), Vector3.Dot(worldLocalOffset, orientationY));
        }

        /// <summary>
        /// Converts <b>Vector3</b> from its <i>local position</i> to <i>world position</i>.
        /// </summary>
        /// <param name="v">The vector needs to be transformed.</param>
        /// <param name="orientationX">Reference X axis.</param>
        /// <param name="orientationY">Reference Y axis.</param>
        /// <param name="orientationZ">Reference Z axis.</param>
        /// <param name="worldReferencePoint">Position of reference in world space respect to transformation takes place.</param>
        /// <returns>Position in world space respect world reference point..</returns>
        public static Vector3 LocalToWorld(this Vector3 v, Vector3 orientationX, Vector3 orientationY, Vector3 orientationZ, Vector3 worldReferencePoint) {
            Vector3 localWorldOffset = orientationX * v.x + orientationY * v.y + orientationZ * v.z;

            return worldReferencePoint + localWorldOffset;
        }

        /// <summary>
        /// Converts <b>Vector3</b> from its <i>world position</i> to <i>local position</i>.
        /// </summary>
        /// <param name="v">The vector needs to be transformed.</param>
        /// <param name="orientationX">Reference X axis.</param>
        /// <param name="orientationY">Reference Y axis.</param>
        /// <param name="localReferencePoint">Position of reference in world space respect to transformation takes place.</param>
        /// <returns>Position in local space respect local reference point.</returns>
        public static Vector3 WorldToLocal(this Vector3 v, Vector3 orientationX, Vector3 orientationY, Vector3 orientationZ, Vector3 localReferencePoint) {
            Vector2 worldLocalOffset = localReferencePoint - v;

            return new Vector3(Vector3.Dot(worldLocalOffset, orientationX), Vector3.Dot(worldLocalOffset, orientationY), Vector3.Dot(worldLocalOffset, orientationZ));
        }

        /// <summary>
        /// Calculates the <b>reflection</b> of this Vector3 on a surface respect a direction.
        /// </summary>
        /// <param name="v">The vector.</param>
        /// <param name="direction">Vector3 direction.</param>
        /// <returns>Vector3 reflection respect a surface. Vector3.zero in case no surfaces are hit.</returns>
        public static Vector3 GetReflection(this Vector3 v, Vector3 direction) {
            if (Physics.Raycast(new Ray(v, direction), out RaycastHit hit, Mathf.Infinity)) {
                Vector3 positiveNormal = hit.normal * Vector3.Dot(direction, hit.normal);

                Vector3 reflection = direction - (2 * positiveNormal);

                return reflection;
            }

            return Vector3.zero;
        }

        /// <summary>
        /// Calculates the <b>reflection</b> of this Vector3 on a surface respect a direction.
        /// </summary>
        /// <param name="v">The vector.</param>
        /// <param name="direction">Vector3 direction.</param>
        /// <param name="recursive">In case we want to calculate the reflection of reflection.</param>
        /// <returns>The last Vector3 reflection respect surfaces. Vector3.zero in case no surfaces are hit.</returns>
        public static Vector3 GetReflection(this Vector3 v, Vector3 direction, bool recursive = true) {
            if (Physics.Raycast(new Ray(v, direction), out RaycastHit hit, Mathf.Infinity)) {
                Vector3 positiveNormal = hit.normal * Vector3.Dot(direction, hit.normal);

                Vector3 reflection = direction - (2 * positiveNormal);

                if (recursive)
                    return GetReflection(hit.point, reflection, recursive);

                return reflection;
            }

            return Vector3.zero;
        }

        /// <summary>
        /// Checks if two Vector2 have same direction. Calculates the angle between them and verifies if is minor of a threshold.
        /// </summary>
        /// <param name="v">The vector.</param>
        /// <param name="vector">The vector to be compared with.</param>
        /// <param name="threshold">Degree threshold. If it's 0, the two vectors should be stackable.</param>
        /// <returns>Returns if the angle between vectors is minor respect threshold.</returns>
        public static bool LookAtDirectionAs(this Vector2 v, Vector2 vector, float threshold = 0) {
            float dot = Vector3.Dot(v.normalized, vector.normalized);
            float radAngle = Mathf.Acos(dot);
            float radThreshold = threshold * Mathf.Deg2Rad;
            return radAngle < radThreshold;
        }

        /// <summary>
        /// Checks if two Vector3 have same direction. Calculates the angle between them and verifies if is minor of a threshold.
        /// </summary>
        /// <param name="v">The vector.</param>
        /// <param name="vector">The vector to be compared with.</param>
        /// <param name="threshold">Degree threshold. If it's 0, the two vectors should be stackable.</param>
        /// <returns>Returns if the angle between vectors is minor respect threshold.</returns>
        public static bool LookAtDirectionAs(this Vector3 v, Vector3 vector, float threshold = 0) {
            float dot = Vector3.Dot(v.normalized, vector.normalized);
            float radAngle = Mathf.Acos(dot);
            float radThreshold = threshold * Mathf.Deg2Rad;
            return radAngle < radThreshold;
        }
        #endregion
    }
}