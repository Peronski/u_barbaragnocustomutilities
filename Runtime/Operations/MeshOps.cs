using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomUtilities.Operations {
    public static class MeshOps {
        #region Mesh Functions
        /// <summary>
        /// Calculates the <i>surface area</i> of <b>Mesh</b>.
        /// </summary>
        /// <param name="mesh">The mesh.</param>
        /// <returns>The surface area.</returns>
        public static float CalculateMeshSurface(this Mesh mesh) {
            float sumArea = 0.0f;

            for (int i = 0; i < mesh.triangles.Length; i += 3) {
                Vector3 p1 = mesh.vertices[mesh.triangles[i + 0]];
                Vector3 p2 = mesh.vertices[mesh.triangles[i + 1]];
                Vector3 p3 = mesh.vertices[mesh.triangles[i + 2]];

                Vector3 p1p2 = p2 - p1;
                Vector3 p1p3 = p3 - p1;

                float areaTriangle = Vector3.Cross(p1p2, p1p3).magnitude;
                Debug.Log(areaTriangle);

                sumArea += areaTriangle;
            }

            return sumArea /= 2;
        }
        #endregion
    }
}
