using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomUtilities.Operations
{
    public static class ListOps
    {
        #region List Functions
        /// <summary>
        /// Cleans the list from its nulled values.
        /// </summary>
        /// <typeparam name="T">The type of the list's content.</typeparam>
        /// <param name="list">The list to clean.</param>
        public static void Clean<T> (this List<T> list) {
            for(int i = 0; i < list.Count; i++) {
                if(list[i] == null)
                    list.RemoveAt(i);
            }
        }

        /// <summary>
        /// Handles list as Enumerator. Check if there is a subsequent element.
        /// </summary>
        /// <typeparam name="T">The type of the list's content.</typeparam>
        /// <param name="list">The list to clean.</param>
        /// <param name="index">The current index (in enumeration).</param>
        /// <returns></returns>
        public static bool MoveNext<T>(this List<T> list, int index) {
            index++;
            return index < list.Count;
        }

        /// <summary>
        /// Resets the index to -1 value.
        /// </summary>
        /// <typeparam name="T">The type of the list's content.</typeparam>
        /// <param name="list">The list whose index should be reset.</param>
        /// <param name="index">The current index examined.</param>
        public static void Reset<T>(this List<T> list, int index) {
            index = -1;
        }
        #endregion
    }
}