using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomUtilities.Operations
{
    public enum CurveRank
    {
        Quadratic,
        Cubic,
        Tetric,
        Pentic,
        Hexic
    };

    public static class CurvOps
    {
        #region Public Functions

        #region Single Functions
        /// <summary>
        /// Basically it's the same as Vector2.Lerp
        /// </summary>
        /// <param name="startPos"></param>
        /// <param name="endPos"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        /// <see cref="Vector2.Lerp(Vector2, Vector2, float)"/>
        public static Vector2 LinearCurve (Vector2 startPos, Vector2 endPos, float t) {
            //Matematic equation: y = [(1 - t) p0] + [t * p1]

            float u = 1 - t;

            Vector2 curvePoint = u * startPos + t * endPos;

            return curvePoint;
        }

        /// <summary>
        /// Calculates a position on a Bezier Quadratic Curve built using 3 given points.
        /// </summary>
        /// <param name="startPos">The start position of the Curve.</param>
        /// <param name="midPoint">The tanget generator position of the Curve.</param>
        /// <param name="endPos">The final position of the Curve.</param>
        /// <param name="t">The position of the point on the Curve (0 ... 1).</param>
        /// <returns>The position on the Curve at point <i>t</i>.</returns>
        public static Vector2 QuadraticCurve (Vector2 startPos, Vector2 midPoint, Vector2 endPos, float t) {
            //Matematic equation: y = [(1 - x)^2 * p0] + [2(1 - x)x * p2] + [x^2 * p1]

            float u = 1 - t;
            float sqr = Mathf.Pow(t, 2);
            float uSqr = Mathf.Pow(u, 2);

            Vector2 curvePoint = (startPos * uSqr) + (midPoint * 2 * u * t) + (endPos * sqr);

            return curvePoint;
        }

        public static Vector3 QuadraticCurvePoints (List<Vector3> points, Vector3 startPos, Vector3 midPoint, Vector3 endPos, float lengthPerc) {
            //Matematic equation: y = [(1 - x)^2 * p0] + [2(1 - x)x * p2] + [x^2 * p1]
            float x = lengthPerc * (1 / (float)(points.Count - 1));

            for(int i = 0; i < points.Count; i++) {
                float u = 1 - x * i;
                float sqr = Mathf.Pow(x * i, 2);
                float uSqr = Mathf.Pow(u, 2);

                Vector3 curvePoint = (startPos * uSqr) + (midPoint * 2 * u * x * i) + (endPos * sqr);

                points[i] = curvePoint;
            }

            return points[points.Count - 1];
        }

        public static Vector3 CubicCurvePoints (List<Vector3> points, List<Vector3> handles, float lengthPerc) {
            //Matematic equation: y = [p0*(1-t)^3] + [3*p1*t*(1-t)^2] + [3*p2*t^2*(1-t)] + [p3*t^3]
            float step = lengthPerc * (1 / (float)(points.Count - 1));

            for(int i = 0; i < points.Count; i++) {
                float t = step * i;

                Vector3 curvePoint = (handles[0] * Mathf.Pow(1 - t, 3))
                    + (3 * handles[1] * t * Mathf.Pow(1 - t, 2))
                    + (3 * handles[2] * Mathf.Pow(t, 2) * (1 - t))
                    + (handles[3] * Mathf.Pow(t, 3));

                points[i] = curvePoint;
            }

            return points[points.Count - 1];
        }

        public static Vector3 TetricCurvePoints (List<Vector3> points, List<Vector3> handles, float lengthPerc) {
            //Matematic equation: y = [p0*(1-t)^4] + [4*p1*t*(1-t)^3] + [6*p2*t^2*(1-t)^2] + [4*p1*t*3*(1-t)] + [p3*t^4]
            float step = lengthPerc * (1 / (float)(points.Count - 1));

            for(int i = 0; i < points.Count; i++) {
                float t = step * i;

                Vector3 curvePoint = (handles[0] * Mathf.Pow(1 - t, 4))
                    + (4 * handles[1] * t * Mathf.Pow(1 - t, 3))
                    + (6 * handles[2] * Mathf.Pow(t, 2) * Mathf.Pow(1 - t, 2))
                    + (4 * handles[3] * Mathf.Pow(t, 3) * (1 - t))
                    + (handles[4] * Mathf.Pow(t, 4));

                points[i] = curvePoint;
            }

            return points[points.Count - 1];
        }
        #endregion

        #region Uniqe Function
        public static Vector3 CurvePoints (List<Vector3> points, List<Vector3> handles, CurveRank rank, float lengthPerc) {
            switch(rank.GetHashCode()) {
                case 0:
                    if(handles.Count < 3) {
                        Debug.LogWarningFormat("<b>!</b>The curve type is set to Quadratic but it doesn't have enough Handles.");
                        return Vector3.zero;
                    }
                    return QuadraticCurvePoints(points, handles[0], handles[1], handles[2], lengthPerc);
                //break;
                case 1:
                    if(handles.Count < 4) {
                        Debug.LogWarningFormat("<b>!</b>The curve type is set to Cubic but it doesn't have enough Handles.");
                        return Vector3.zero;
                    }
                    return CubicCurvePoints(points, handles, lengthPerc);
                //break;
                case 2:
                    if(handles.Count < 5) {
                        Debug.LogWarningFormat("<b>!</b>The curve type is set to Tetric but it doesn't have enough Handles.");
                        return Vector3.zero;
                    }
                    return TetricCurvePoints(points, handles, lengthPerc);
                //break;
                case 3:
                    if(handles.Count < 6) {
                        Debug.LogWarningFormat("<b>!</b>The curve type is set to Pentic but it doesn't have enough Handles.");
                        return Vector3.zero;
                    }
                    return Vector3.zero;
                //break;
                case 4:
                    if(handles.Count < 7) {
                        Debug.LogWarningFormat("<b>!</b>The curve type is set to Hexic but it doesn't have enough Handles.");
                        return Vector3.zero;
                    }
                    return Vector3.zero;
                    //break;
            }

            return Vector3.zero;
        }
        #endregion

        #region Global
        public static void MoveAlongPath (this Transform target, List<Vector3> handles, float t) {
            //Generic Equation: B(t) = Sigma[i=0->n]((n | i) * p(i) * (1 - t)^(n - i) * t^i)

            int n = handles.Count - 1;

            if(n <= 0) {
                Debug.LogAssertion("Not enough points to use for generating Bezier Curve");
                return;
            }

            Vector3 pos = (handles[0] * Mathf.Pow(1 - t, n)) + (handles[n] * Mathf.Pow(t, n));//Primo e ultimo elemento della serie

            for(int i = 1; i < n; i++)//Esclusi gli estremi perch� includono azzeramenti che fanno andare in pappa Unishit
            {
                float binomial = MathOps.Combination(n, i);
                float powInverse = Mathf.Pow(1 - t, n - i);
                float powDirect = Mathf.Pow(t, i);

                pos += handles[i] * binomial * powInverse * powDirect;
            }

            target.position = pos;
        }

        public static Vector3 DrawDotOnPath (List<Vector3> handles, float t) {
            //Generic Equation: B(t) = Sigma[i=0->n]((n | i) * p(i) * (1 - t)^(n - i) * t^i)

            int n = handles.Count - 1;

            if(n <= 0) {
                Debug.LogAssertion("Not enough points to use for generating Bezier Curve");
                return Vector3.zero;
            }

            Vector3 pos = (handles[0] * Mathf.Pow(1 - t, n)) + (handles[n] * Mathf.Pow(t, n));//Primo e ultimo elemento della serie

            for(int i = 1; i < n; i++)//Esclusi gli estremi perch� includono azzeramenti che fanno andare in pappa Unishit
            {
                float binomial = MathOps.Combination(n, i);
                float powInverse = Mathf.Pow(1 - t, n - i);
                float powDirect = Mathf.Pow(t, i);

                pos += handles[i] * binomial * powInverse * powDirect;
            }

            return pos;
        }
        #endregion

        #endregion
    }
}