using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Barbaragno.DataAsset.EventHandlerSO { 
    /// <summary>
    /// Abstract serializable ScriptableObject class used to handle events with one subject.
    /// </summary>
    [System.Serializable]
    [CreateAssetMenu(fileName = "New Event Handler SO", menuName = "Scriptable Objects/Data Asset/Event Handler 1 Subject")]
    public abstract class EventHandlerSO<T> : ScriptableObject {
        #region Private Variables
        protected T _subject;
        #endregion

        #region Public Variables
        public System.Action<T> OnEvent;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        /// <inheritdoc />
        /// <summary>
        /// Assigns the argument of event invocation.
        /// </summary>
        /// <param name="subject">Param of event invocation.</param>
        /// <inheritdoc />
        public abstract void AllocateSubject(T subject);

        /// <inheritdoc />
        /// <summary>
        /// Links the param callback to the Event handled by this ScriptableObject.
        /// </summary>
        /// <param name="callback">Method to subscribe.</param>
        /// <inheritdoc />
        public abstract void Subscribe(System.Action<T> callback);

        /// <inheritdoc />
        /// <summary>
        /// Delinks the param callback from the Event handled by this ScriptableObject.
        /// </summary>
        /// <param name="callback">Method to unsubscribe.</param>
        /// <inheritdoc />
        public abstract void Unsubscribe(System.Action<T> callback);

        /// <inheritdoc />
        /// <summary>
        /// Method to raise event handled by this ScriptableObject.
        /// </summary>
        /// <inheritdoc />
        public abstract void RaiseEvent();
        #endregion
    }
}
