using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Barbaragno.DataAsset.EventHandlerSO {
    /// <summary>
    /// Abstract serializable ScriptableObject class used to handle events with two subjects.
    /// </summary>
    [System.Serializable]
    [CreateAssetMenu(fileName = "New Event Handler SO", menuName = "Scriptable Objects/Data Asset/Event Handler 2 Subjects")]
    public abstract class EventHandler2SubjectSO<T, V> : ScriptableObject {
        #region Private Variables
        private T _subjectOne;
        private V _subjectTwo;
        #endregion

        #region Public Variables
        public System.Action<T, V> OnEvent;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        /// <inheritdoc />
        /// <summary>
        /// Assigns the first argument of event invocation.
        /// </summary>
        /// <param name="subject">Param of event invocation.</param>
        /// <inheritdoc />
        public abstract void AllocateSubject(T subject);

        /// <inheritdoc />
        /// <summary>
        /// Assigns the second argument of event invocation.
        /// </summary>
        /// <param name="subject">Param of event invocation.</param>
        /// <inheritdoc />
        public abstract void AllocateSubject(V subject);

        /// <inheritdoc />
        /// <summary>
        /// Links the param callback to the Event handled by this ScriptableObject.
        /// </summary>
        /// <param name="callback">Method to subscribe.</param>
        /// <inheritdoc />
        public abstract void Subscribe(System.Action<T,V> callback);

        /// <inheritdoc />
        /// <summary>
        /// Delinks the param callback from the Event handled by this ScriptableObject.
        /// </summary>
        /// <param name="callback">Method to unsubscribe.</param>
        /// <inheritdoc />
        public abstract void Unsubscribe(System.Action<T,V> callback);

        /// <inheritdoc />
        /// <summary>
        /// Method to raise event handled by this ScriptableObject.
        /// </summary>
        /// <inheritdoc />
        public abstract void RaiseEvent();
        #endregion
    }
}
