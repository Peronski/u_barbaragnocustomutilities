using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.SerializableData {
    public abstract class LogicSO : ScriptableObject {
        public virtual T Clone<T>() where T : LogicSO {
            return (T)Instantiate(this);
        }
    }
}
