using System.Collections.Generic;
using UnityEngine;

public class AuthorizationObserver : MonoBehaviour {
    [Header("Settings")]
    [SerializeField] private Authorization defaultAuthorization;

    [Header("References")]
    [SerializeField] private List<MonoBehaviour> observedComponents = new List<MonoBehaviour>();

    private bool alreadyEnabled = true;

    private void Awake() {
        AuthorizationManager.Instance.OnAuthorization += OnAuthorizationChanged;
    }

    private void SubscribeObservedComponent(MonoBehaviour component) {
        if (observedComponents.Contains(component))
            return;

        observedComponents.Add(component);
    }
    private void UnSubscribeObservedComponent(MonoBehaviour component) {
        if (!observedComponents.Contains(component))
            return;

        observedComponents.Remove(component);
    }

    private void OnAuthorizationChanged(Authorization authorization) {
        if (authorization != defaultAuthorization) {
            DisableComponents();
            return;
        }

        if (authorization == defaultAuthorization) {
            EnableComponents();
            return;
        }
    }

    private void DisableComponents() {
        observedComponents.ForEach(component => component.enabled = false);
        alreadyEnabled = false;
    }
    private void EnableComponents() {
        if (alreadyEnabled)
            return;

        observedComponents.ForEach(component => component.enabled = false);
        alreadyEnabled = true;
    }
}
