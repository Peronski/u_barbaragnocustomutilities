using System;
using UnityEngine;

public class AuthorizationManager : SceneSingleton<AuthorizationManager> {
    [Header("Settings")]
    [SerializeField] private Authorization startingAuthorization;

    private Authorization currentAuthorization;

    public event Action<Authorization> OnAuthorization;

    public Authorization CurrentAuthorization { 
        get => currentAuthorization;
        private set {
            currentAuthorization = value;
            OnAuthorization?.Invoke(value);
        }
    }
    public Authorization StartingAuthorization {
        get => startingAuthorization;
        private set {
            startingAuthorization = value;
            OnAuthorization?.Invoke(value);
        }
    }

    public void ChangeAuthorization(Authorization authorization) {
        if (authorization == currentAuthorization)
            return;

        currentAuthorization = authorization;
    }
}
