using UnityEngine;
using Barbaragno.AI.SensingComponents.Interfaces;
using Barbaragno.DataAsset.EventHandlerSO;

namespace Barbaragno.AI.SensingComponents.Examples {
    /// <summary>
    /// Example class explains how "ISmellMaker" interface should be implemented.
    /// </summary>
    public class Smell : MonoBehaviour, ISmell {
        #region Private Variables
        private float _cooldown;
        private float _startingIntensity;
        private float _currentIntensity;
        private ISmellMaker _emitter;
        #endregion

        #region Public Variables
        [SerializeField] private float movementVelocity;
        [SerializeField] private float decreaseRate;
        [SerializeField] private float decreaseIntensity;
        #endregion

        #region Properties
        public ISmellMaker Emitter => _emitter;
        public GameObject GameObj => gameObject;
        public float StartingIntensity => _startingIntensity;
        public float CurrentIntensity => _currentIntensity;
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        private void Update() {
            Move();
            LosingIntensity();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Simply example of movement function. Object not affected by physics. 
        /// </summary>
        private void Move() {
            transform.position += transform.right * movementVelocity * Time.deltaTime;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Decreases intensity value during time.
        /// Simulates realistic smell which, moving away from its source, loses its odor's intensity.
        /// </summary>
        public void LosingIntensity() {
            _cooldown += Time.deltaTime;

            if (_cooldown >= decreaseRate) {
                _currentIntensity -= decreaseIntensity;
                _cooldown = 0;
            }

            if(_currentIntensity <= 0f)
                Destroy(gameObject);
        }

        /// <summary>
        /// Configuration method. Called on spawn by emitter. 
        /// </summary>
        /// <param name="food">The emitter of the smell.</param>
        public void OnEmission(ISmellMaker food) {
            _emitter = food;
            _startingIntensity = _emitter.SmellIntensity;
            _currentIntensity = _startingIntensity;
        }
        #endregion
    }
}
