using System;
using UnityEngine;
using Barbaragno.DataAsset.EventHandlerSO;
using Barbaragno.AI.SensingComponents.Interfaces;

namespace Barbaragno.AI.SensingComponents.Examples {
    /// <summary>
    /// Example class explains how "IViewDetectable" interface should be implemented.
    /// </summary>
    public class ViewDetectable : MonoBehaviour, IViewDetectable {
        #region Private Variables
        private OnBeingDetectedEventSO _currentOnBeingDetectedEventSO;
        private OnDestroyEventSO _currentOnDestroyEventSO;
        #endregion

        #region Public Variables
        [SerializeField] [Range(0, 1)] private float threat;
        [SerializeField] [Range(0, 1)] private float bother;
        [field: SerializeField] public OnBeingDetectedEventSO OnBeingDetectedEventSOSource { get; set; }
        [field: SerializeField] public OnDestroyEventSO OnDestroyEventSOSource { get; set; }
        #endregion

        #region Properties
        public float Threat => threat;
        public float Bother => bother;
        public Vector3 PositionDetected => transform.position;
        public Transform TransformDetected => transform;
        public GameObject GameObj => gameObject;
        public OnBeingDetectedEventSO OnBeingDetectedEventSO => _currentOnBeingDetectedEventSO;
        public OnDestroyEventSO OnDestroyEventSO => _currentOnDestroyEventSO;
        #endregion

        #region Behaviour Callbacks
        private void Awake() {
            _currentOnBeingDetectedEventSO = Instantiate(OnBeingDetectedEventSOSource);
            _currentOnDestroyEventSO = Instantiate(OnDestroyEventSOSource);
        }

        private void OnDestroy() {
            _currentOnDestroyEventSO.RaiseEvent();
        }
        #endregion

        #region Public Functions
        #endregion

        #region Private Methods
        #endregion

        #region Interfaces
        #endregion
    }
}
