using System;
using UnityEngine;
using Barbaragno.AI.SensingComponents.Interfaces;
using Barbaragno.DataAsset.EventHandlerSO;

namespace Barbaragno.AI.SensingComponents.Examples {
    /// <summary>
    /// Example class explains how "INoiseMaker" interface should be implemented.
    /// </summary>
    public class NoiseMaker : MonoBehaviour, INoiseMaker {
        #region Private Variables
        private OnBeingDetectedEventSO _currentOnBeingDetectedEventSO;
        private OnDestroyEventSO _currentOnDestroyEventSO;
        #endregion

        #region Public Variables
        [SerializeField] [Range(0, 1)] private float loudness;
        [SerializeField] [Range(0, 1)] private float threat;
        [SerializeField] [Range(0, 1)] private float bother;
        [field : SerializeField]public OnBeingDetectedEventSO OnBeingDetectedEventSOSource { get; set; }
        [field: SerializeField] public OnDestroyEventSO OnDestroyEventSOSource { get; set; }

#pragma warning disable
        public event Action<INoiseMaker> OnMakingNoiseEvent;
#pragma warning restore
        #endregion

        #region Properties
        public float Loudness => loudness;
        public float Threat => threat;
        public float Bother => bother;
        public GameObject GameObj => gameObject;
        public Vector3 PositionDetected => transform.position;
        public Transform TransformDetected => transform;
        public OnBeingDetectedEventSO OnBeingDetectedEventSO => _currentOnBeingDetectedEventSO;
        public OnDestroyEventSO OnDestroyEventSO => _currentOnDestroyEventSO;
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        private void Awake() {
            _currentOnBeingDetectedEventSO = Instantiate(OnBeingDetectedEventSOSource);
            _currentOnDestroyEventSO = Instantiate(OnDestroyEventSOSource);
        }

        private void OnDestroy() {
            _currentOnDestroyEventSO.RaiseEvent();
        }
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        public void MakeNoise() {
            OnMakingNoiseEvent?.Invoke(this);
        }
        #endregion
    }
}
