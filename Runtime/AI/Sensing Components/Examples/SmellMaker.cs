using UnityEngine;
using Barbaragno.AI.SensingComponents.Interfaces;
using Barbaragno.DataAsset.EventHandlerSO;

namespace Barbaragno.AI.SensingComponents.Examples {
    /// <summary>
    /// Example class explains how "ISmellMaker" interface should be implemented.
    /// 
    /// </summary>
    public class SmellMaker : MonoBehaviour, ISmellMaker {
        #region Private Variables
        private OnBeingDetectedEventSO _currentOnBeingDetectedEventSO;
        private OnDestroyEventSO _currentOnDestroyEventSO;
        #endregion

        #region Public Variables
        [SerializeField] [Range(0, 1)] private float intensity;
        [SerializeField] [Range(0, 1)] private float threat;
        [SerializeField] [Range(0, 1)] private float bother;
        [SerializeField] private GameObject smellPrefab;
        [field: SerializeField] public OnBeingDetectedEventSO OnBeingDetectedEventSOSource { get; set; }
        [field: SerializeField] public OnDestroyEventSO OnDestroyEventSOSource { get; set; }
        #endregion

        #region Properties
        public float Threat => threat;
        public float Bother => bother;
        public float SmellIntensity => intensity;
        public GameObject GameObj => gameObject;
        public Vector3 PositionDetected => transform.position;
        public Transform TransformDetected => transform;
        public OnBeingDetectedEventSO OnBeingDetectedEventSO => _currentOnBeingDetectedEventSO;
        public OnDestroyEventSO OnDestroyEventSO => _currentOnDestroyEventSO;
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        private void Awake() {
            _currentOnBeingDetectedEventSO = Instantiate(OnBeingDetectedEventSOSource);
            _currentOnDestroyEventSO = Instantiate(OnDestroyEventSOSource);
        }

        private void OnDestroy() {
            _currentOnDestroyEventSO.RaiseEvent();
        }
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// Spawning method for ISmell objects.
        /// Tecnically should be spawned more than one smell, one for each directions.
        /// </summary>
        public void EmitSmell() {
            ISmell Smell = Instantiate(smellPrefab, transform).GetComponent<ISmell>();
            Smell.OnEmission(this);
        }
        #endregion
    }
}
