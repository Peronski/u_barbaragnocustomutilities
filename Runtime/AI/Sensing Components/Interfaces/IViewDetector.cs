using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Barbaragno.AI.SensingComponents.Interfaces {
    /// <summary>
    /// Interface implemented by actors using sight component to detect.
    /// </summary>
    public interface IViewDetector : IDetector {
        #region Private Variables
        #endregion

        #region Public Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// Executes logic of SightComponent.
        /// </summary>
        public void LookingFor();
        #endregion
    }
}