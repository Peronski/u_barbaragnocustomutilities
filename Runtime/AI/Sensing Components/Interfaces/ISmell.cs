using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Barbaragno.AI.SensingComponents.Interfaces {
    /// <summary>
    /// Interface implemented by smells.
    /// </summary>
    public interface ISmell {
        #region Private Variables
        #endregion

        #region Public Variables
        #endregion

        #region Properties
        /// <summary>
        /// Who have spawned this gameobject.
        /// </summary>
        public ISmellMaker Emitter { get; }
        /// <summary>
        /// ref to this GameObject.
        /// </summary>
        public GameObject GameObj { get; }
        /// <summary>
        /// Smell's intensity at the starting point.
        /// </summary>
        public float StartingIntensity { get; }
        /// <summary>
        /// Detectable smell's intensity decreased during time and movement.
        /// </summary>
        public float CurrentIntensity { get; }
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// Configures starting values of "ISmell" object spawned by an ISmellMaker.
        /// </summary>
        /// <param name="food">Who spawn the object</param>
        public void OnEmission(ISmellMaker emitter);

        /// <summary>
        /// Decreases smell intensity during time and movement.
        /// </summary>
        public void LosingIntensity();
        #endregion
    }
}