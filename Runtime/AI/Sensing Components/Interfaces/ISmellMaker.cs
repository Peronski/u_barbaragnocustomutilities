using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Barbaragno.AI.SensingComponents.Interfaces {
    /// <summary>
    /// Interface implemented by actors those secret smells.
    /// </summary>
    public interface ISmellMaker : IDetectable {
        #region Private Variables
        #endregion

        #region Public Variables
        #endregion

        #region Properties
        /// <summary>
        /// Smell's intensity.
        /// </summary>
        public float SmellIntensity { get; }
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// Spawn method for "ISmell" objects.
        /// </summary>
        public void EmitSmell();
        #endregion
    }
}