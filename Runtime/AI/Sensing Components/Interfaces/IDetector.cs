using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Barbaragno.DataAsset.EventHandlerSO;

namespace Barbaragno.AI.SensingComponents.Interfaces {
    /// <summary>
    /// Interface implemented by all actors (and sensing components) which should detected "IDetectable" actors.
    /// </summary>
    public interface IDetector {
        #region Private Variables
        #endregion

        #region Public Variables
        /// <summary>
        /// EventHandlerSO source.
        /// WARNING: you must not work with this ref, but instantiate a copy for this script.
        /// </summary>
        public OnDetectingEventSO OnDetectingEventSOSource { get; set; }
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
