using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Barbaragno.DataAsset.EventHandlerSO;

namespace Barbaragno.AI.SensingComponents.Interfaces {
    /// <summary>
    /// Interface implemented by all actors which should be detected by one or more sensing coponents.
    /// </summary>
    public interface IDetectable {
        #region Private Variables
        #endregion

        #region Public Variables
        /// <summary>
        /// EventHandlerSO source. It should handle actions after the "IDetectable" actor is detected.
        /// WARNING: you must not work with this ref, but instantiate a copy for this script.
        /// </summary>
        public OnBeingDetectedEventSO OnBeingDetectedEventSOSource { get; set; }
        /// <summary>
        /// EventHandlerSO source. It should handle actions after the "IDetectable" actor is going to be destroyed.
        /// WARNING: you must not work with this ref, but instantiate a copy for this script.
        /// </summary>
        public OnDestroyEventSO OnDestroyEventSOSource { get; set; }

        /// <summary>
        /// EventHandlerSO copy. It should handle actions after the "IDetectable" actor is detected.
        /// </summary>
        public OnBeingDetectedEventSO OnBeingDetectedEventSO { get; }

        /// <summary>
        /// EventHandlerSO copy. It should handle actions after the "IDetectable" actor is going to be destroyed.
        /// </summary>
        public OnDestroyEventSO OnDestroyEventSO { get; }
        #endregion

        #region Properties
        /// <summary>
        /// Threat grade.
        /// </summary>
        public float Threat { get; }
        /// <summary>
        /// Bother grade.
        /// </summary>
        public float Bother { get; }
        /// <summary>
        /// Ref to this GameObject.
        /// </summary>
        public GameObject GameObj {get;}
        /// <summary>
        /// Ref to this transform.position.
        /// </summary>
        public Vector3 PositionDetected { get; }
        /// <summary>
        /// Ref to this Transform.
        /// </summary>
        public Transform TransformDetected { get; }
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        #endregion
    }
}