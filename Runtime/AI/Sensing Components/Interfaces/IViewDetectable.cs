using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Barbaragno.AI.SensingComponents.Interfaces {
    /// <summary>
    /// Interface implemented by actors who can be "view detectable".
    /// </summary>
    public interface IViewDetectable : IDetectable {
        #region Private Variables
        #endregion

        #region Public Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        #endregion
    }
}