using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Barbaragno.AI.SensingComponents.Interfaces {
    /// <summary>
    /// Interface implemented by actors using SmellComponent.
    /// </summary>
    public interface ISmellSniffer : IDetector {
        #region Private Variables
        #endregion

        #region Public Variables
        #endregion

        #region Properties
        /// <summary>
        /// Detectable intensity of smells.
        /// </summary>
        public float SniffingSkill { get; }
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// Verifies if a smell is detectable.
        /// </summary>
        /// <param name="smell">The smell to be analyzed.</param>
        public bool EvaluateDetectability(ISmell smell);
        #endregion
    }
}