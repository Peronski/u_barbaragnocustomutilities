using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Barbaragno.DataAsset.EventHandlerSO;

namespace Barbaragno.AI.SensingComponents.Interfaces {
    /// <summary>
    /// Interface implemented by actors who can be detected by noise.
    /// </summary>
    public interface INoiseMaker : IDetectable {
        #region Private Variables
        #endregion

        #region Public Variables
        /// <summary>
        /// Temporaly event used to subscribe OnNoise method of INoiseListener.
        /// The subscription should happen when an "INoiseMaker" object enters the trigger area of HearingComponent.
        /// </summary>
        public event System.Action<INoiseMaker> OnMakingNoiseEvent;
        #endregion

        #region Properties
        /// <summary>
        /// Sound's intensity.
        /// </summary>
        public float Loudness { get; }
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// Triggers noise and OnMakingNoiseEvent.
        /// </summary>
        public void MakeNoise();
        #endregion
    }
}
