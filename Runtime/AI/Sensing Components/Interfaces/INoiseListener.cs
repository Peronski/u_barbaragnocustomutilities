using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Barbaragno.AI.SensingComponents.Interfaces {
    /// <summary>
    /// Interface implemented by actors using heard component to detect noises.
    /// </summary>
    public interface INoiseListener : IDetector {
        #region Private Variables
        #endregion

        #region Public Variables
        #endregion

        #region Properties
        /// <summary>
        /// Detectable loudness of noises.
        /// </summary>
        public float ListeningSkill { get; set; }
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// This method should be subscribed to OnMakingNoiseEvent of target INoiseMaker, which entered the trigger area of this "INoiseListener".
        /// </summary>
        /// <param name="noise"></param>
        public void OnNoise(INoiseMaker noise);
        #endregion
    }
}