using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Barbaragno.AI.SensingComponents.Interfaces;
using Barbaragno.DataAsset.EventHandlerSO;

namespace Barbaragno.AI.SensingComponents {
    /// <summary>
    /// This class is responsable for sight (sense) logic.
    /// </summary>
    public class SightComponent : MonoBehaviour, IViewDetector {

        #region Private Variables
        private float _currentFocusTime;
        /// <summary>
        /// Copy of EventHandlerSOSource. It handles the detection of an "IViewDetectable/IDetectable" object.
        /// </summary>
        private OnDetectingEventSO _currentOnDetectingEventSO;
        #endregion

        #region Public Variables
        [Header("Receiver")]
        [SerializeField] private BrainComponent brain;

        [Header("Sense conditions")]
        [SerializeField] private bool is3D = false;
        [SerializeField] private bool canSee = true;
        [Tooltip("Function repeat rate.")] [SerializeField] [Range(0, 1)] private float repeatRate;
        [Tooltip("How much time player must remain in FOV before being seen.")] [SerializeField] private float focusTime;
        [Tooltip("Length of view.")] [SerializeField] private float rangeFieldOfView;
        [Tooltip("Vertical view angle.")] [SerializeField] private float angleYFieldOfView;
        [Tooltip("Horizontal view angle.")] [SerializeField] private float angleXFieldOfView;
        [Tooltip("Layers of visible objects.")] [SerializeField] private LayerMask layerMaskVisible;

        /// <summary>
        /// EventHandlerSO Source.
        /// WARNING: you must not work with this ref, but instantiate a copy for this script.
        /// </summary>
        [field: SerializeField] public OnDetectingEventSO OnDetectingEventSOSource { get; set; }
        #endregion

        #region Properties
        /// <summary>
        /// Forward direction respect the world space (2D = transform.right / 3D = transform.forward)
        /// </summary>
        public Vector3 LookDirection => is3D ? transform.forward : transform.right;
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        private void Awake() {
            _currentOnDetectingEventSO = Instantiate(OnDetectingEventSOSource);
        }

        private void Start() {
            _currentOnDetectingEventSO.Subscribe(brain.AnalyzeDetectedObject);
            InvokeRepeating(nameof(LookingFor), 0, repeatRate);
        }

        private void OnDrawGizmos() {
            if (is3D)
                Draw3DView();
            else
                Draw2DView();

            LookingFor();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// OnDrawGizmos method. Draws 2D cone of view. 
        /// </summary>
        private void Draw2DView() {
            Gizmos.color = Color.gray;

            Vector3 positiveViewY = Quaternion.AngleAxis((angleYFieldOfView / 2), Vector3.forward) * LookDirection * rangeFieldOfView;
            Vector3 negativeViewY = Quaternion.AngleAxis(-(angleYFieldOfView / 2), Vector3.forward) * LookDirection * rangeFieldOfView;

            Gizmos.DrawLine(transform.position, transform.position + positiveViewY);
            Gizmos.DrawLine(transform.position, transform.position + negativeViewY);
        }

        /// <summary>
        /// OnDrawGizmos method. Draws 3D cone of view. 
        /// </summary>
        private void Draw3DView() {
            Gizmos.color = Color.gray;

            Vector3 positiveViewY = Quaternion.AngleAxis((angleYFieldOfView / 2), Vector3.right) * LookDirection * rangeFieldOfView;
            Vector3 negativeViewY = Quaternion.AngleAxis(-(angleYFieldOfView / 2), Vector3.right) * LookDirection * rangeFieldOfView;

            Vector3 positiveViewX = Quaternion.AngleAxis((angleXFieldOfView / 2), Vector3.up) * LookDirection * rangeFieldOfView;
            Vector3 negativeViewX = Quaternion.AngleAxis(-(angleXFieldOfView / 2), Vector3.up) * LookDirection * rangeFieldOfView;

            Gizmos.DrawLine(transform.position, transform.position + positiveViewY);
            Gizmos.DrawLine(transform.position, transform.position + negativeViewY);
            Gizmos.DrawLine(transform.position, transform.position + positiveViewX);
            Gizmos.DrawLine(transform.position, transform.position + negativeViewX);
        }

        /// <summary>
        /// Verifies if some "IViewDetectable" object is detectable in 3D cone of view.
        /// </summary>
        private void View3D() {
            if (!canSee)
                return;

            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, transform.position + LookDirection * rangeFieldOfView);

            Collider[] hitInfo = Physics.OverlapSphere(transform.position, rangeFieldOfView, layerMaskVisible);

            foreach (Collider c in hitInfo) {

                Vector3 distance = c.transform.position - transform.position;

                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, transform.position + distance);

                if (Vector3.Dot(distance.normalized, LookDirection.normalized) < 0.1f)
                    continue;

                Vector3 COM = c.attachedRigidbody.worldCenterOfMass;
                Vector3 distanceCOM = COM - transform.position;

                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(transform.position, transform.position + distanceCOM);

                Vector3 temp = distanceCOM;
                temp.y = 0f;

                float degAngleX = Vector3.SignedAngle(LookDirection, temp, Vector3.up);
                if (!(degAngleX > -(angleXFieldOfView / 2) && degAngleX < (angleXFieldOfView / 2))) {
                    continue;
                }

                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position, transform.position + distanceCOM);

                temp = distanceCOM;
                temp.x = 0f;

                float degAngleY = Vector3.SignedAngle(LookDirection, temp, Vector3.right);
                if (!(degAngleY > -(angleYFieldOfView / 2) && degAngleY < (angleYFieldOfView / 2))) {
                    continue;
                }

                if (Physics.Raycast(transform.position, distanceCOM, out RaycastHit hit, Mathf.Infinity)) {
                    IViewDetectable VD = hit.collider.GetComponent<IViewDetectable>();

                    if (!ReferenceEquals(null, VD)) {
                        _currentFocusTime += 1.0f / repeatRate; //REV: is correct?

                        if (_currentFocusTime >= focusTime) {
                            Gizmos.color = Color.magenta;
                            Gizmos.DrawLine(transform.position, transform.position + distanceCOM);

                            _currentFocusTime = 0f;
                            _currentOnDetectingEventSO.AllocateSubject(VD);
                            _currentOnDetectingEventSO.RaiseEvent();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Verifies if some "IViewDetectable" object is detectable in 2D cone of view.
        /// </summary>
        private void View2D() {
            if (!canSee)
                return;

            Collider[] hitInfo = Physics.OverlapSphere(transform.position, rangeFieldOfView, layerMaskVisible);

            foreach (Collider c in hitInfo) {

                Vector3 distance = c.transform.position - transform.position;

                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, transform.position + distance);

                if (Vector3.Dot(distance.normalized, LookDirection) < 0.1f)
                    continue;

                Vector3 COM = c.attachedRigidbody.worldCenterOfMass;
                Vector3 distanceCOM = COM - transform.position;

                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(transform.position, transform.position + distanceCOM);

                float degAngle = Vector3.SignedAngle(LookDirection, distanceCOM, Vector3.forward);
                if (!(degAngle > (-angleYFieldOfView / 2) && degAngle < (angleYFieldOfView / 2))) {
                    continue;
                }

                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position, transform.position + distanceCOM);

                if (Physics.Raycast(transform.position, distanceCOM, out RaycastHit hit, Mathf.Infinity)) {
                    IViewDetectable VD = hit.collider.GetComponent<IViewDetectable>();

                    if (!ReferenceEquals(null, VD)) {
                        _currentFocusTime += 1.0f / repeatRate; //REV: is correct?

                        if (_currentFocusTime >= focusTime) {
                            Gizmos.color = Color.magenta;
                            Gizmos.DrawLine(transform.position, transform.position + distanceCOM);

                            _currentFocusTime = 0f;
                            _currentOnDetectingEventSO.AllocateSubject(VD);
                            _currentOnDetectingEventSO.RaiseEvent();
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// This method is called X times per second by InvokeRepeating function.
        /// If npc can see, a overlap sphere (of radius equal npc's FOV radius) is cast around him. 
        /// If an object "IViewDetectable" is visible (no walls), is added to targetable list.
        /// When all checks are done, the highest priority detectable is considerated the current target.
        /// </summary>
        public void LookingFor() {
            if (is3D)
                View3D();
            else
                View2D();
        }
        #endregion
    }
}