using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Barbaragno.AI.SensingComponents.Interfaces;
using Barbaragno.DataAsset.EventHandlerSO;

namespace Barbaragno.AI.SensingComponents { 
    /// <summary>
    /// This class is responsable for smell (sense) logic.
    /// This class requires a SphereCollider to detected smell actors.
    /// </summary>
    [System.Serializable]
    [RequireComponent(typeof(SphereCollider))]
    public class SmellComponent : MonoBehaviour, ISmellSniffer {
        #region Private Variables
        /// <summary>
        /// Copy of EventHandlerSOSource. It handles the detection of an "ISmellMaker/IDetectable" object.
        /// </summary>
        private OnDetectingEventSO _currentOnDetectingEventSO;
        #endregion

        #region Public Variables
        [Header("Receiver")]
        [SerializeField] private BrainComponent brain;

        [Header("Sense conditions")]
        [SerializeField] private bool canSniff;
        [SerializeField] [Range(0, 1)] private float sniffingSkill;
        /// <summary>
        /// EventHandlerSO Source.
        /// WARNING: you must not work with this ref, but instantiate a copy for this script.
        /// </summary>
        [field : SerializeField]public OnDetectingEventSO OnDetectingEventSOSource { get; set; }
        #endregion

        #region Properties
        public float SniffingSkill => sniffingSkill;
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        private void Awake() {
            _currentOnDetectingEventSO = Instantiate(OnDetectingEventSOSource);
        }

        private void Start() {
            _currentOnDetectingEventSO.Subscribe(brain.AnalyzeDetectedObject);
        }

        private void OnTriggerEnter(Collider other) {
            if (!canSniff)
                return;

            ISmell smell = other.GetComponent<ISmell>();

            if (!ReferenceEquals(null, smell)) {
                if (EvaluateDetectability(smell)) {
                    _currentOnDetectingEventSO.AllocateSubject(smell.Emitter);
                    _currentOnDetectingEventSO.RaiseEvent();
                }
            }
        }
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// Evaluates the intesity of passed smell respect sniffing skill.
        /// </summary>
        /// <param name="smell">The smell being evaluated.</param>
        /// <returns>Perceptible(true)/Not perceptible(false).</returns>
        public bool EvaluateDetectability(ISmell smell) {
            return smell.CurrentIntensity < SniffingSkill;
        }
        #endregion
    }
}