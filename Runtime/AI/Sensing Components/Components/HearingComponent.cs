using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Barbaragno.AI.SensingComponents.Interfaces;
using Barbaragno.DataAsset.EventHandlerSO;

namespace Barbaragno.AI.SensingComponents {
    /// <summary>
    /// This component class is responsable for hearing (sense) logic.
    /// This class requires a SphereCollider to detected objects will make noises.
    /// </summary>
    [System.Serializable]
    [RequireComponent(typeof(SphereCollider))]
    public class HearingComponent : MonoBehaviour, INoiseListener {
        #region Private Variables
        /// <summary>
        /// Copy of EventHandlerSOSource. It handles the detection of an "INoiseMaker/IDetectable" object.
        /// </summary>
        private OnDetectingEventSO _currentOnDetectingEventSO;
        #endregion

        #region Public Variables
        [Header("Receiver")]
        [SerializeField] private BrainComponent brain;

        [Header("Sense conditions")]
        [SerializeField] private bool canHear;
        [field : SerializeField] public float ListeningSkill { get; set; }
        /// <summary>
        /// EventHandlerSO Source.
        /// WARNING: you must not work with this ref, but instantiate a copy for this script.
        /// </summary>
        [field : SerializeField] public OnDetectingEventSO OnDetectingEventSOSource { get; set; }
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        private void Awake() {
            _currentOnDetectingEventSO = Instantiate(OnDetectingEventSOSource);    
        }

        private void Start() {
            _currentOnDetectingEventSO.Subscribe(brain.AnalyzeDetectedObject);
        }

        /// <summary>
        /// Registers all NoiseMaker objects entering in audible radius of npc.
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerEnter(Collider other) {
            if (!canHear)
                return;

            INoiseMaker NM = other.gameObject.GetComponent<INoiseMaker>();

            if (!ReferenceEquals(null, NM)) {
                NM.OnMakingNoiseEvent += OnNoise;
            }
        }
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        public void OnNoise(INoiseMaker noise) {
            _currentOnDetectingEventSO.AllocateSubject(noise);
            _currentOnDetectingEventSO.RaiseEvent();
        }
        #endregion
    }
}