using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Barbaragno.AI.SensingComponents.Interfaces; 
using Barbaragno.DataAsset.EventHandlerSO;

namespace Barbaragno.AI.SensingComponents {
    /// <summary>
    /// Serializable class which stores and process npc senses informations.
    /// </summary>
    [System.Serializable]
    public class BrainComponent : MonoBehaviour {
        #region Private Variables
        private float _currentCalm;
        /// <summary>
        /// List contains all "IDetectable" actors found by SensingComponents.
        /// </summary>
        private List<IDetectable> _detectedObjects;
        /// <summary>
        /// The current "IDetectable" actor considered as a threat.
        /// </summary>
        private IDetectable _currentThreat;
        /// <summary>
        /// Copy of EventHandlerSOSource. It should advise all actors interested about new threatened status, resetting the current "IDetectable" threat.
        /// </summary>
        private OnFeelingThreatenedEventSO _currentOnFeelingThreatenedEventSO;
        private OnNotFeelingThreatenedEventSO _currentOnNotFeelingThreatenedEventSO;
        #endregion

        #region Public Variables
        [Header("Specifics")]
        [SerializeField] private bool useCalm = false;
        [Tooltip("Starting calm value.")] [Range(0, 1)] [SerializeField] private float startingCalm;
        [Tooltip("The lesser value that triggers threatened status.")] [Range(0, 1)] [SerializeField] private float threatThreshold;
        [Tooltip("Minimum distance to reset threatened status.")][SerializeField] private float resetThreatDistance = Mathf.Infinity;

        /// <summary>
        /// It should advise all actors interested about new threatened status, reporting the new "IDetectable" threat.
        /// WARNING: you must not work with this ref, but instantiate a copy for this script.
        /// </summary>
        [field: SerializeField]public OnFeelingThreatenedEventSO OnFeelingThretenedEventSOSource { get; set; }
        /// <summary>
        /// It should advise all actors interested about new threatened status, resetting the current "IDetectable" threat.
        /// WARNING: you must not work with this ref, but instantiate a copy for this script.
        /// </summary>
        [field: SerializeField]public OnNotFeelingThreatenedEventSO OnNotFeelingThretenedEventSOSource { get; set; }
        #endregion

        #region Properties
        /// <summary>
        /// List contains all "IDetectable" actors found by SensingComponents.
        /// </summary>
        public List<IDetectable> DetectedObjects => _detectedObjects;
        /// <summary>
        /// The current "IDetectable" actor considered as a threat.
        /// </summary>
        public IDetectable CurrentThreat => _currentThreat;
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        private void Awake() {
            _currentOnFeelingThreatenedEventSO = Instantiate(OnFeelingThretenedEventSOSource);
            _currentOnNotFeelingThreatenedEventSO = Instantiate(OnNotFeelingThretenedEventSOSource);
        }

        private void Start() {
            _detectedObjects = new List<IDetectable>();

            if(useCalm)
                _currentCalm = startingCalm;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Removes from detected objects the passed detectable.
        /// </summary>
        /// <param name="detectable">Detectable to remove</param>
        private void RemoveDetectable(IDetectable detectable) {
            if(_detectedObjects.Contains(detectable))
                _detectedObjects.Remove(detectable);
        }

        /// <summary>
        /// Resets stored threat information. Applies starting values of calm.
        /// </summary>
        private void ResetStatus() {
            RemoveDetectable(_currentThreat);
            _currentOnNotFeelingThreatenedEventSO.RaiseEvent();
            _currentThreat = null;

            if(useCalm)
                _currentCalm = startingCalm;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Verifies the threatened status.
        /// If current threat's distance from this actor's position is more the a certain range, the threat is reset.
        /// </summary>
        /// <returns>Threatened(true)/Not Threatened(false)</returns>
        public bool IsThreatened() {
            if (ReferenceEquals(null, _currentThreat))
                return false;

            if (IsThreatInsideRange(resetThreatDistance))
                return true;
            else {
                ResetStatus();
                return false;
            }
        }

        /// <summary>
        /// Verifies the threatened status.
        /// If current threat's distance from reference position is more the a certain range, the threat is reset.
        /// </summary>
        /// <param name="range"></param>
        /// <param name="position"></param>
        /// <returns>Threatened(true)/Not Threatened(false)</returns>
        public bool IsThreatened(float range, Vector3 position) {
            if (ReferenceEquals(null, _currentThreat))
                return false;

            if (IsThreatInsideRange(range, position))
                return true;
            else {
                ResetStatus();
                return false;
            }
        }

        /// <summary>
        /// Verifies if threat is inside a specific range, calculing if distance between this actor and threat is minor of range.
        /// </summary>
        /// <param name="range"></param>
        /// <returns>Threat in range(true)/Threat not in range(false)</returns>
        public bool IsThreatInsideRange(float range) {
            return Vector3.Distance(_currentThreat.PositionDetected, transform.position) <= range;
        }

        /// <summary>
        /// Verifies if threat is inside a specific range, calculing if distance between position and threat is minor of range.
        /// </summary>
        /// <param name="position">The position used as reference point to calculate distance from threat.</param>
        /// <returns>Threat in range(true)/Threat not in range(false)</returns>
        public bool IsThreatInsideRange(float range, Vector3 position) {
            return Vector3.Distance(_currentThreat.PositionDetected, position) <= range;
        }

        /// <summary>
        /// Analizies the "IDetectable" actor passed as parameter by one of SensingComponents. 
        /// Verifies the actor's threat grade and eventually update its threatened status.
        /// </summary>
        /// <typeparam name="T">Where T : IDetectable.</typeparam>
        /// <param name="objDetected">"IDetectable" actor passed by one of SensingComponents.</param>
        public virtual void AnalyzeDetectedObject<T>(T objDetected) where T : IDetectable {
            Debug.Log("<color=magenta>Analyzing...</color>\n");

            if (ReferenceEquals(null, objDetected) || _detectedObjects.Contains(objDetected))
                return;

            if(useCalm)
                _currentCalm -= objDetected.Bother;

            Debug.Log("<color=magenta>Analyzing visible object.</color>\n");

            _detectedObjects.Add(objDetected);

            Debug.Log($"<color=magenta>Adding object</color> {objDetected.TransformDetected.name} <color=magenta>to list.</color>\n");
            objDetected.OnDestroyEventSO.Subscribe(RemoveDetectable);

            if (IsThreatened(resetThreatDistance, transform.position))
                return;

            if (objDetected.Threat >= threatThreshold || _currentCalm <= 0) {
                _currentThreat = objDetected;
                _currentOnFeelingThreatenedEventSO.AllocateSubject(_currentThreat);
                _currentOnFeelingThreatenedEventSO.RaiseEvent();
            }
        }

        /// <summary>
        /// Orders "IDetectable" actors by their threat grade.
        /// </summary>
        /// <param name="descending">Descending(true)/Ascending(false)</param>
        /// <returns></returns>
        public List<IDetectable> OrderDetectablesByThreat(bool descending) {
            if(descending)
                return _detectedObjects.OrderByDescending(x => x.Threat).ToList();
            else
                return _detectedObjects.OrderBy(x => x.Threat).ToList();
        }
        #endregion
    }
}