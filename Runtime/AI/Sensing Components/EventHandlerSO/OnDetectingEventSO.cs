using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Barbaragno.AI.SensingComponents.Interfaces;

namespace Barbaragno.DataAsset.EventHandlerSO {
    /// <summary>
    /// Serializable event handler scriptable object. It should handle actions after an IDetector detects an IDetectable.
    /// </summary>
    [Serializable]
    [CreateAssetMenu(fileName = "New Detecting Event SO", menuName = "Scriptable Objects/Data Asset/Event Handler/OnDetecting Event")]
    public class OnDetectingEventSO : EventHandlerSO<IDetectable> {
        #region Private Variables
        #endregion

        #region Public Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        public override void AllocateSubject(IDetectable subject) {
            _subject = subject;
        }

        public override void Subscribe(Action<IDetectable> callback) {
            OnEvent += callback;
        }

        public override void Unsubscribe(Action<IDetectable> callback) {
            OnEvent -= callback;
        }

        public override void RaiseEvent() {
            OnEvent?.Invoke(_subject);
        }
        #endregion
    }
}
