using UnityEngine;

public class UnityObject : MonoBehaviour {

    private GameObject myGameObject;
    public new GameObject gameObject {
        get {
            if (myGameObject == null) {
                myGameObject = base.gameObject;
            }
            return myGameObject;
        }
    }

    private Transform myTransform;
    public new Transform transform {
        get {
            if (myTransform == null) {
                myTransform = base.transform;
            }
            return myTransform;
        }
    }

    private RectTransform myRectTransform;
    public RectTransform RectT {
        get {
            if (myRectTransform == null) {
                myRectTransform = GetComponent<RectTransform>();
            }

            return myRectTransform;
        }
    }

    public void ForceAwake() {
        if (gameObject.activeSelf) {
            return;
        }

        gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}