using MessageSystem;

namespace UpSurgeOn.MetaverseRuntime.Authentication {
    public class SignRequestErrorListener : ErrorListener {

        private SignPanels errorPanel;

        protected override void ErrorRequestMessage(APIMessageTypes types, MessageClass message) {
            base.ErrorRequestMessage(types, message);

            OnErrorAction(AccessMenu.Show, errorPanel);
            SignAdvisor.LogMessage("[SignIn] - Error");
        }

        public void ErrorPanel(SignPanels panel) => errorPanel = panel;
    }
}
