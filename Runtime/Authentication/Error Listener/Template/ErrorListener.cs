using MessageSystem;
using UnityEngine;
using UpSurgeOnRuntime.Network;
using UpSurgeOnRuntime.StaticClass;

namespace UpSurgeOn.MetaverseRuntime.Authentication {
    public class ErrorListener : MonoBehaviour {
        protected static bool attachedErrorReq;

        protected void OnDestroy() {
            DetachFromErrorChannel();
        }

        protected virtual void ErrorRequestMessage(APIMessageTypes types, MessageClass message) {
            DetachFromErrorChannel();

            if (!MessageClass.TryCastTo(message, out JsonMessage content)) {
                Log.ErrorCastMessage(nameof(JsonMessage));
                return;
            }

            if (content.Success) //TODO: verify bug for 200 error
                return;
        }

        protected virtual void OnErrorAction<T>(System.Action<T> action, T obj) {
            action(obj);
        }

        public void AttachToErrorChannel() {
            if (attachedErrorReq)
                return;

            MessageManager.API.AttachListener(APIMessageTypes.ErrorRequestMessage, ErrorRequestMessage);
            attachedErrorReq = true;
        }

        public void DetachFromErrorChannel() {
            if (!attachedErrorReq)
                return;

            MessageManager.API.DetachListener(APIMessageTypes.ErrorRequestMessage, ErrorRequestMessage);
            attachedErrorReq = false;
        }
    }
}
