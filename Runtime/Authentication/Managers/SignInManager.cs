using UnityEngine;
using UnityEngine.SceneManagement;
using MessageSystem;
using UpSurgeOnRuntime.Network;
using UpSurgeOnRuntime.StaticClass;
using UpSurgeOn.MetaverseRuntimeRuntime.Singletons;

namespace UpSurgeOn.MetaverseRuntime.Authentication {
    public class SignInManager : MonoBehaviour {
        [Header("References")]
        [SerializeField] private AuthenticationInfoContainer authenticationInfo;
        [SerializeField] private SignRequestErrorListener error;

        private void OnDestroy() {
            MessageManager.API.DetachListener(APIMessageTypes.SignIn, OnSignIn);
        }

        private void Awake() {
            MessageManager.API.AttachListener(APIMessageTypes.SignIn, OnSignIn);
        }

        public void SignIn() {
            if (!authenticationInfo.AreCredentialsValid())
                return;

            IRequestProvider<SignInRequest> requestProvider = authenticationInfo.GetComponent<IRequestProvider<SignInRequest>>();
            if (requestProvider != null) {
                SignInRequest request = requestProvider.GetRequest();

                if (!ServiceProvider.API.SignIn(request))
                    return;
            } else return;

            AccessMenu.Show(SignPanels.Loading);
            error.ErrorPanel(SignPanels.ErrorSingIn);
            error.AttachToErrorChannel();
            SignAdvisor.LogMessage("[SignIn] - Requested");
        }

        private void OnSignIn(APIMessageTypes type, MessageClass message) {
            if (!MessageClass.TryCastTo(message, out JsonData<SignInResponse> content)) {
                Log.ErrorCastMessage(nameof(JsonData<SignInResponse>));
                return;
            }

            error.DetachFromErrorChannel();
            SignAdvisor.LogMessage("[SignIn] - Completed");
            //SceneLoader.Instance.LoadSceneAsync(SceneTypes.Lobby, out AsyncOperation operation, true, UnityEngine.SceneManagement.LoadSceneMode.Additive);
            SceneLoader.Instance.LoadScene(SceneTypes.Lobby, true, false, LoadSceneMode.Additive);
        }
    }
}
