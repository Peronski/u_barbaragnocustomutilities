using UnityEngine;
using UnityEngine.SceneManagement;
using MessageSystem;
using UpSurgeOnRuntime.Network;
using UpSurgeOnRuntime.StaticClass;
using UpSurgeOn.MetaverseRuntimeRuntime.Singletons;

namespace UpSurgeOn.MetaverseRuntime.Authentication {
    public class SignUpManager : MonoBehaviour {
        [Header("References")]
        [SerializeField] private AuthenticationInfoContainer authenticationInfo;
        [SerializeField] private SignRequestErrorListener error;

        private void OnDestroy() {
            MessageManager.API.DetachListener(APIMessageTypes.SignUp, OnSignUp);
        }

        private void Awake() {
            MessageManager.API.AttachListener(APIMessageTypes.SignUp, OnSignUp);
        }

        public void SignUp() {
            if (!authenticationInfo.AreCredentialsValid())
                return;

            IRequestProvider<SignUpRequest> requestProvider = authenticationInfo.GetComponent<IRequestProvider<SignUpRequest>>();
            if (requestProvider != null) {
                SignUpRequest request = requestProvider.GetRequest();

                if (!ServiceProvider.API.SignUp(request))
                    return;
            } else return; //TODO: valid?

            AccessMenu.Show(SignPanels.Loading);
            error.ErrorPanel(SignPanels.ErrorSignUp);
            error.AttachToErrorChannel();
            SignAdvisor.LogMessage("[SignUp] - Requested");
        }

        private void OnSignUp(APIMessageTypes type, MessageClass message) {
            if (!MessageClass.TryCastTo(message, out JsonData<SignUpResponse> content)) {
                Log.ErrorCastMessage(nameof(JsonData<SignUpResponse>));
                return;
            }

            error.DetachFromErrorChannel();
            SignAdvisor.LogMessage("[SignUp] - Completed");
            //SceneLoader.Instance.LoadSceneAsync(SceneTypes.Lobby, out AsyncOperation operation, true, UnityEngine.SceneManagement.LoadSceneMode.Additive);
            SceneLoader.Instance.LoadScene(SceneTypes.Lobby, true, false, LoadSceneMode.Additive);
        }
    }
}
