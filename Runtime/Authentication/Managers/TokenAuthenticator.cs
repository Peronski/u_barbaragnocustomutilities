using MessageSystem;
using UnityEngine;
using UnityEngine.SceneManagement;
using UpSurgeOn.MetaverseRuntimeRuntime.Singletons;
using UpSurgeOnRuntime.Network;
using UpSurgeOnRuntime.StaticClass;

namespace UpSurgeOn.MetaverseRuntime.Authentication {
    public class TokenAuthenticator : MonoBehaviour {
        [Header("Settings")]
        [TextArea][SerializeField] private string token;

        [Header("References")]
        [SerializeField] private SignRequestErrorListener error;

        private const string TOKEN_KEY = "Token";

        private void OnDestroy() {
            MessageManager.API.DetachListener(APIMessageTypes.SignInWithToken, OnSignInWithToken);
        }

        private void Awake() {
            MessageManager.API.AttachListener(APIMessageTypes.SignInWithToken, OnSignInWithToken);
        }

        private void Start() {
            if (!GetToken(out string token)) {
                AccessMenu.Show(SignPanels.SignIn);
                return;
            }

            if (token == null) {
                AccessMenu.Show(SignPanels.SignIn);
                return;
            }

            if (!ServiceProvider.API.SignInWithToken(token)) {
                RemoveToken();
            }

            error.ErrorPanel(SignPanels.ErrorSingIn);
            error.AttachToErrorChannel();
            AccessMenu.Show(SignPanels.Loading);
            SignAdvisor.LogMessage("[SignInWithToken] - Requested");
        }

        private void OnSignInWithToken(APIMessageTypes type, MessageClass message) {
            if (!MessageClass.TryCastTo(message, out JsonData<User> response)) {
                Log.ErrorCastMessage(nameof(JsonData<User>));
                return;
            }

            error.DetachFromErrorChannel();
            SignAdvisor.LogMessage("[SignInWithToken] - Completed");
            //SceneLoader.Instance.LoadSceneAsync(SceneTypes.Lobby, out AsyncOperation operation, true, UnityEngine.SceneManagement.LoadSceneMode.Additive);
            SceneLoader.Instance.LoadScene(SceneTypes.Lobby, true, false, LoadSceneMode.Additive);
        }

        private bool GetToken(out string token) {
            if (!PlayerPrefs.HasKey(TOKEN_KEY)) {
                token = null;
                return false;
            }

            token = PlayerPrefs.GetString(TOKEN_KEY);
            return true;
        }

        private void RemoveToken() {
            if (!PlayerPrefs.HasKey(TOKEN_KEY))
                return;

            PlayerPrefs.SetString(TOKEN_KEY, null);
            PlayerPrefs.Save();
        }

        public void AddToken(string token) {
            PlayerPrefs.SetString(TOKEN_KEY, token);
            PlayerPrefs.Save();
        }

#if UNITY_EDITOR
        [ContextMenu(nameof(TEST_ADD_TOKEN))]
        public void TEST_ADD_TOKEN() {
            AddToken(token);
            SignAdvisor.LogMessage("[TEST] - Token added");
        }

        [ContextMenu(nameof(TEST_REMOVE_TOKEN))]
        public void TEST_REMOVE_TOKEN() {
            RemoveToken();
            SignAdvisor.LogMessage("[TEST] - Token removed");
        }
#endif
    }
}
