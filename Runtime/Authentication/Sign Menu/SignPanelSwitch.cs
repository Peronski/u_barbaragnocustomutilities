using MessageSystem;

namespace UpSurgeOn.MetaverseRuntime.Utilities {
    public class SignPanelSwitch : Panel<SignPanels> {
        private void OnDestroy() {
            MessageManager.Authentication.DetachListener(AuthenticationMessageTypes.UI, Switch);
        }
        private void Awake() {
            MessageManager.Authentication.AttachListener(AuthenticationMessageTypes.UI, Switch);
        }

        private void Switch(AuthenticationMessageTypes types, MessageClass message) {
            if (!MessageClass.TryCastTo(message, out ValueMessage<SignPanels> content))
                return;

            SwitchState(content.Value);
        }

        protected virtual void SwitchState(SignPanels type) {
            if (type == panelType)
                gameObject.SetActive(true);
            else
                gameObject.SetActive(false);
        }
    }
}
