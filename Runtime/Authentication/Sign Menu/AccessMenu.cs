using MessageSystem;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Authentication {
    public class AccessMenu : MonoBehaviour {
        public void EnableCanvasObject(GameObject uiObject) {
            uiObject.SetActive(true);
        }

        public void DisableCanvasObject(GameObject uiObject) {
            uiObject.SetActive(false);
        }

        public static void Show(SignPanels type) {
            MessageManager.Authentication.SendMessage(AuthenticationMessageTypes.UI, new ValueMessage<SignPanels>(type));
        }
    }
}
