namespace MessageSystem {

    public static partial class MessageManager {

        public readonly static MessageChannel<AuthenticationMessageTypes> Authentication = new MessageChannel<AuthenticationMessageTypes>();
    }

    public enum AuthenticationMessageTypes {
        None,
        UI
    }
}
