using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Authentication {
    public class AuthenticationInfoContainer : MonoBehaviour {
        [Header("References")]
        [SerializeField] private AuthenticationInfoValidator validator;

        protected string email;
        protected string password;

        public string Email {
            get => email;
            set => email = value;
        }
        public string Password {
            get => password;
            set => password = value;
        }

        public void ResetEmail() {
            if (string.IsNullOrEmpty(Email))
                return;

            Email = null;
        }
        public void ResetPassword() {
            if (string.IsNullOrEmpty(Password))
                return;

            Password = null;
        }
        public bool AreCredentialsValid() => validator.AreCredentialsValid(Email, Password);
    }
}
