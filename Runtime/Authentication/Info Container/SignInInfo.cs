using UpSurgeOnRuntime.Network;

namespace UpSurgeOn.MetaverseRuntime.Authentication {
    public class SignInInfo : AuthenticationInfoContainer, IRequestProvider<SignInRequest> {
        public SignInRequest GetRequest() {
            return new SignInRequest() { Email = Email, Password = Password };
        }
    }
}
