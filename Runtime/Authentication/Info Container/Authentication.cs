public interface IRequestProvider<T> {
    public abstract T GetRequest();
}
