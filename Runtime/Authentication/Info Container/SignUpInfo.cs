using UpSurgeOnRuntime.Network;

namespace UpSurgeOn.MetaverseRuntime.Authentication {
    public class SignUpInfo : AuthenticationInfoContainer, IRequestProvider<SignUpRequest> {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Role { get; set; }

        public void ResetName() => Name = null;
        public void ResetSurname() => Surname = null;
        public void ResetRole() => Role = null;

        public SignUpRequest GetRequest() {
            return new SignUpRequest() { GivenName = Name, Surname = Surname, Email = Email, Password = Password, Role = Role };
        }
    }
}
