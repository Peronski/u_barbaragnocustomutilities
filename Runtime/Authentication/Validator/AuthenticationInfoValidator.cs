using TMPro;
using UnityEngine;
using UpSurgeOnRuntime.StaticClass;

namespace UpSurgeOn.MetaverseRuntime.Authentication {
    public class AuthenticationInfoValidator : MonoBehaviour {
        [Header("References")]
        [SerializeField] protected TextMeshProUGUI emailError;
        [SerializeField] protected TextMeshProUGUI passwordError;

        public virtual bool ValidateMail(string mail) {
            if (!Validator.IsValidEmail(mail, out string mailError)) {
                DisplayEmailError(mailError);
                return false;
            }

            ResetEmailError();
            return true;
        }

        public virtual bool ValidatePassword(string password) {
            if (!Validator.IsValidPass(password, out string passError)) {
                DisplayPasswordError(passError);
                return false;
            }

            ResetPassError();
            return true;
        }

        public void OnEndEditEmail(string email) {
            ValidateMail(email);
        }
        public void OnEndEditPassword(string password) {
            ValidatePassword(password);
        }

        public virtual bool AreCredentialsValid(string mail, string password) => ValidateMail(mail) && ValidatePassword(password);
        public void DisplayEmailError(string message) => emailError.text = message;
        public void ResetEmailError() => emailError.text = "";
        public void DisplayPasswordError(string message) => passwordError.text = message;
        public void ResetPassError() => passwordError.text = "";
    }
}
