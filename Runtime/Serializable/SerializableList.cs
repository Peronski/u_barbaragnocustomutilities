using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Barbaragno.CustomUtilities.SerializableUtility.List {
    /// <summary>
    /// This class is a serializable alternative to the <see cref="List{TValue}"/> class.
    /// </summary>
    /// <typeparam name="T">The type to be stored in the list</typeparam>
    [System.Serializable]
    public class SerializableList<T> : IEnumerable<T> {
        #region Public Variables

        [SerializeField] List<T> list;
        
        #endregion

        #region Private Variables
        #endregion

        #region Properties

        public List<T> List => list;
        public int Count => list.Count;
        
        #endregion

        #region Constructors

        public SerializableList() {
            list = new List<T>();
        }
        
        public SerializableList(List<T> list) {
            this.list = list;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Adds a new value to the list
        /// </summary>
        /// <param name="value"></param>
        public void Add(T value) => list.Add(value);

        /// <summary>
        /// Replaces the existing list with a new one
        /// </summary>
        /// <param name="newList">The new list</param>
        public void UpdateList(List<T> newList) => list = newList;

        public IEnumerator<T> GetEnumerator() {
            return list.GetEnumerator();
        }
        
        #endregion

        #region Private Methods

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
        
        #endregion 
    }
}