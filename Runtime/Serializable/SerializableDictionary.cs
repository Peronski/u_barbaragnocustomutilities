using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Barbaragno.CustomUtilities.SerializableUtility.Dictionary {
    /// <summary>
    /// This class is a serializable alternative to the <see cref="Dictionary{TKey,TValue}"/> class.
    /// </summary>
    /// <typeparam name="K">Any type that implements <see cref="IComparable{T}"/> with itself.</typeparam>
    /// <typeparam name="V">Any type.</typeparam>
    [Serializable]
    public class SerializableDictionary<K, V> : IEnumerator<KeyValuePair<K, V>>, IEnumerable<KeyValuePair<K, V>> where K : IComparable {
        #region Public Variables
        [SerializeField] private List<KeyValuePair<K, V>> values = new List<KeyValuePair<K, V>>();
        #endregion

        #region Private Variables
        private int _index;
        #endregion

        #region Properties
        /// <summary>
        /// Grants access to the dictionary's items by their key value.
        /// </summary>
        /// <param name="key">The first field of type <b>K</b>. to match.</param>
        public V this[K key] {
            get => values.Find(k => k.Key.Equals(key)).Value;
            set => values.Find(k => k.Key.Equals(key)).Value = value;
        }

        public int Count => values.Count;
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        /// <summary>
        /// Searches the dictionary for an item which key is <paramref name="key"/>.
        /// </summary>
        /// <param name="key">The key to look for.</param>
        /// <returns><b>True</b> if the given key is present in the dictionary, <b>False</b> otherwise.</returns>
        public bool ContainsKey(K key) => values.Any(k => k.Key.Equals(key));

        public KeyValuePair<K, V> AtIndex(int index) {
            if (index >= values.Count || index < 0) {
                Debug.LogError(new IndexOutOfRangeException());
                return default;
            }

            return values[index];
        }

        /// <summary>
        /// Searches for the item with the given key and determines its index.
        /// </summary>
        /// <param name="key">The <i>unique</i> to look for.</param>
        /// <returns>The index of the item if found, -1 otherwise.</returns>
        public int IndexOf(string key) {
            for (var i = 0; i < values.Count; i++) {
                if (values[i].Key.Equals(key))
                    return i;
            }

            return -1;
        }

        /// <summary>
        /// Adds a new item to the dictionary.
        /// <br/>It can fail if an item with the same key is already present.
        /// </summary>
        /// <param name="key">The <i>unique</i> key to use for the item.</param>
        /// <param name="value">The value of the item.</param>
        public void AddDefinition(K key, V value) {
            if (ContainsKey(key)) {
                Debug.LogError($"Key {key} was already present.\n");
                return;
            }

            values.Add(new KeyValuePair<K, V>(key, value));
        }

        /// <summary>
        /// Removes the item with the corresponding <paramref name="key"/> from the dictionary.
        /// <br/>It can fail if an item with that key is not present.
        /// </summary>
        /// <param name="key">The <i>unique</i> key to look for.</param>
        public void RemoveDefinition(K key) {
            if (!ContainsKey(key)) {
                Debug.LogError($"Key {key} was not present.\n");
                return;
            }

            var itemIndex = values.FindIndex(k => k.Key.Equals(key));
            values.RemoveAt(itemIndex);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Interfaces
        public bool MoveNext() {
            ++_index;

            return _index < values.Count && _index >= 0;
        }

        public void Reset() => _index = -1;

        public KeyValuePair<K, V> Current => values[_index];

        object IEnumerator.Current => Current;

        public void Dispose() {}

        public IEnumerator<KeyValuePair<K, V>> GetEnumerator() {
            foreach (var keyValuePair in values) {
                yield return keyValuePair;
            }
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
        #endregion
    }

    /// <summary>
    /// This class is a utility class used by the <see cref="SerializableDictionary{K,V}"/> one.
    /// </summary>
    /// <typeparam name="K">Any type that implements <see cref="IComparable{T}"/> with itself.</typeparam>
    /// <typeparam name="V">Any type.</typeparam>
    [System.Serializable]
    public class KeyValuePair<K, V> where K : IComparable {
        #region Public Variables
        /// <summary>
        /// The Key value of type <b>K</b>.
        /// </summary>
        public K Key;
        /// <summary>
        /// The actual value  of type <b>V</b> stored in this object.
        /// </summary>
        public V Value;
        #endregion

        #region Constructors
        public KeyValuePair() {
            
        }

        public KeyValuePair(K key, V value) {
            Key = key;
            Value = value;
        }
        #endregion
    }
}