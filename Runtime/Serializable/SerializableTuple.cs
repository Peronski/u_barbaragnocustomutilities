using UnityEngine;

namespace Barbaragno.CustomUtilities.SerializableUtility.Tuple {
    /// <summary>
    /// This is a serializable alternative to the <see cref="System.Tuple"/> class.
    /// </summary>
    /// <typeparam name="T1">Any type.</typeparam>
    /// <typeparam name="T2">Any type.</typeparam>
    [System.Serializable]
    public class SerializableTuple<T1, T2> {
        #region Public Variables
        [SerializeField] private T1 item1;
        [SerializeField] private T2 item2;
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        /// <summary>
        /// The item of type <b>T1</b>
        /// </summary>
        public T1 Item1 => item1;
        /// <summary>
        /// The item of type <b>T2</b>
        /// </summary>
        public T2 Item2 => item2;
        #endregion

        #region Constructors
        public SerializableTuple () {}

        public SerializableTuple(T1 item1, T2 item2) {
            this.item1 = item1;
            this.item2 = item2;
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}