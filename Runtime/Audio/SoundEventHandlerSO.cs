using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Barbaragno.CustomUtilities.Data.EventSO;
using Barbaragno.CustomUtilities.Audio;

namespace Barbaragno.CustomUtilities.Data.EventSO.Audio {
    [CreateAssetMenu(fileName = "New SoundEventHandlerSO", menuName = "Project R/EventHandler/SoundEventHandler")]
    public class SoundEventHandlerSO : EventHandlerSO {
        #region Private Variables
        #endregion

        #region Public Variables
        [SerializeField] private AudioInfo audioClip;

        public System.Action<AudioClip,int> OnAudioRequestEvent;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        private void Awake() {
            OnAudioRequestEvent = null;
        }
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        public override void RaiseEvent() {
            base.RaiseEvent();

            OnAudioRequestEvent?.Invoke(audioClip.GetAudio(), audioClip.Priority);
        }

        public void Subscribe(System.Action<AudioClip, int> callback) {
            OnAudioRequestEvent += callback;
        }

        public void Unsubscribe(System.Action<AudioClip, int> callback) {
            OnAudioRequestEvent -= callback;
        }
        #endregion
    }
}
