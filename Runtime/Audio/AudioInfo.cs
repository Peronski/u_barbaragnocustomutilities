using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomUtilities.Operations;

namespace Barbaragno.CustomUtilities.Audio {
    public enum ExtractionLogic { First, Random, Order, RandomWithoutRep, Fixed }

    /// <summary>
    /// Serializable class used to store a list of AudioClips, a priority identifier and an extraction algorithm. 
    /// </summary>
    [System.Serializable]
    public class AudioInfo {
        private int _currentCount = -1;
        private List<AudioClip> _alreadyPlayed = new List<AudioClip>();

        [Tooltip("The starting fixed index.")] [SerializeField] private int fixedIndex;
        [Tooltip("All AudioClips.")] [SerializeField] private List<AudioClip> clips = new List<AudioClip>();
        [Tooltip("The priority of these AudioClips respect others.")] [SerializeField] private int priority;
        [Tooltip("How the AudioClip will be extracted from list.")] [SerializeField] private ExtractionLogic extractionLogic;

        public int Priority => priority;

        public AudioClip GetAudio() {
            switch (extractionLogic) {
                case ExtractionLogic.First:
                    return clips[0];
                case ExtractionLogic.Random:
                    return clips[Random.Range(0, clips.Count)];
                case ExtractionLogic.Order:
                    _currentCount = (_currentCount + 1).Cycle(0, clips.Count);
                    return clips[_currentCount];
                case ExtractionLogic.RandomWithoutRep:
                    AudioClip audio = clips[Random.Range(0, clips.Count)];
                    _alreadyPlayed.Add(audio);
                    clips.Remove(audio);
                    if (clips.Count == 0) {
                        _alreadyPlayed.ForEach(x => clips.Add(x));
                        _alreadyPlayed.Clear();
                    }
                    return audio;
                case ExtractionLogic.Fixed:
                    return clips[fixedIndex];
                default:
                    return null;
            }
        }
    }
}
