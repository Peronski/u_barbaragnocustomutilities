using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Barbaragno.CustomUtilities.Data.EventSO.Audio;
using Barbaragno.CustomUtilities.Singletons;

namespace Barbaragno.CustomUtilities.Audio {
    /// <summary>
    /// Component class for audio management. 
    /// </summary>
    public class AudioController : MonoBehaviour {
        #region Private Variables
        private SoundEventHandlerSO _soundEventSO;
        #endregion

        #region Public Variables
        [SerializeField] private bool playMusic;
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private SoundEventHandlerSO soundEventSOSource;
        #endregion

        #region Properties
        public bool IsIndipendent => _audioSource;
        public SoundEventHandlerSO SoundEventSO => _soundEventSO; 
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        private void Awake() {
            _soundEventSO = Instantiate(soundEventSOSource);

            if (playMusic) {
                _soundEventSO.Subscribe(PlayMusic);
                return;
            }

            _soundEventSO.Subscribe(PlaySound);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        public void PlayMusic(AudioClip audioClip, int priority) {
            AudioManager.Instance.PlayMusic(audioClip, priority);
        }

        public void PlaySound(AudioClip audioClip, int priority) {
            if (IsIndipendent)
                _audioSource.PlayOneShot(audioClip);
            else
                AudioManager.Instance.PlaySound(audioClip, priority);
        }
        #endregion
    }
}