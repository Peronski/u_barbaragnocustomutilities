using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using Barbaragno.CustomUtilities.Audio;

namespace Barbaragno.CustomUtilities.Singletons {
    /// <summary>
    /// Singleton class for Audio management between scenes.
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class AudioManager : GeneralSingleton<AudioManager> {
        #region Private Variables
        private int _currentPriority = 0;
        private int _lastPriority = 0;
        private int _queuePriority = 0;
        private AudioClip _queue;
        private AudioOptions _settings = new AudioOptions();
        private const string _audioData = "AudioData";
        #endregion

        #region Public Variables
        [Header("Music Audio Source")]
        [SerializeField] private AudioSource _musicSource;
        [Header("Sound Audio Source")]
        [SerializeField] private AudioSource _soundSource;

        [Header("Audio Settings")]
        [SerializeField] private float fadeRate;

        public event System.Action OnSaveAudioDataEvent;
        public event System.Action OnLoadAudioDataEvent;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        private IEnumerator Start() {
            yield return null;
            //Load();
        }

        private void OnApplicationQuit() {
            //Save();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Checks the major priority between the current soundtrack and the passed new one. 
        /// </summary>
        /// <param name="priority">Priority value of AudioClip.</param>
        /// <returns></returns>
        private bool HasPriority(int priority) {
            if (priority < _currentPriority)
                return false;

            _lastPriority = _currentPriority;
            _currentPriority = priority;
            return true;
        }

        /// <summary>
        /// Sets the clip of MusicSource with the passed parameter.
        /// </summary>
        /// <param name="clip">AudioClip to play.</param>
        private void SetMusicClip(AudioClip clip) {
            if (ReferenceEquals(null, clip))
                return;

            _musicSource.clip = clip;
            _musicSource.Play();
        }

        //private void Load() {
        //    var list = SaveManager.Instance.Load<SerializableList<SerializableTuple<string, AudioSetting>>>(_audioData);

        //    _settings.TransmuteList(list);

        //    OnLoadAudioDataEvent?.Invoke();
        //}

        //private void Save() {
        //    OnSaveAudioDataEvent?.Invoke();

        //    SaveManager.Instance.Save(_settings.TransmuteDictionary(), _audioData, true);
        //}
        #endregion

        #region Public Methods
        /// <summary>
        /// Reproduces on SoundSource the passed AudioClip with PlayOneShot method.
        /// </summary>
        /// <param name="clip">AudioClip to play.</param>
        /// <param name="priority">Priority value of AudioClip (not used with general sounds).</param>
        public void PlaySound(AudioClip clip, int priority) {
            if (ReferenceEquals(null, clip))
                return;

            _soundSource.PlayOneShot(clip);
        }

        /// <summary>
        /// Checks priority of passed AudioClip and if it's major (of current) starts coroutine to fade music.
        /// Otherwise the AudioClip is added to queue.
        /// </summary>
        /// <param name="soundtrack">AudioClip to play.</param>
        /// <param name="priority">Priority value of AudioClip.</param>
        public void PlayMusic(AudioClip soundtrack, int priority) {
            if (ReferenceEquals(null, soundtrack))
                return;

            if (HasPriority(priority)) {
                StartCoroutine(CO_ChangeMusic(_musicSource, soundtrack));
                return;
            }

            _queuePriority = priority;
            _queue = soundtrack;
        }

        /// <summary>
        /// If queue is not empty, fade coroutine will be started to last queue's AudioClip.
        /// </summary>
        public void NextOne() {
            if (ReferenceEquals(null, _queue))
                return;

            _currentPriority = _queuePriority;
            StartCoroutine(CO_ChangeMusic(_musicSource, _queue));
        }

        public void SaveAudioSetting(string key, AudioSetting setting) {
            _settings.audioSettingDB[key] = setting;
        }

        public AudioSetting LoadAudioSetting(string key) {
            if (ReferenceEquals(null, _settings))
                return new AudioSetting();

            return _settings.GetAudioSetting(key);
        }
        #endregion


        #region Coroutines
        /// <summary>
        /// Coroutine to fade out AudioSource's volume.
        /// </summary>
        /// <param name="source">AudioSource selected.</param>
        /// <returns></returns>
        private IEnumerator CO_FadeOut(AudioSource source) {
            while (source.volume > 0.1) {
                source.volume = Mathf.Lerp(source.volume, 0.0f, fadeRate * Time.deltaTime);
                yield return null;
            }

            source.volume = 0.0f;
        }

        /// <summary>
        /// Coroutine to fade in AudioSource's volume.
        /// </summary>
        /// <param name="source">AudioSource selected.</param>
        /// <returns></returns>
        private IEnumerator CO_FadeIn(AudioSource source) {
            while (source.volume < 0.9) {
                source.volume = Mathf.Lerp(source.volume, 1.0f, fadeRate * Time.deltaTime);
                yield return null;
            }

            source.volume = 1.0f;
        }

        /// <summary>
        /// Coroutine to change AudioClip fading AudioSource's volume.
        /// </summary>
        /// <param name="source">AudioSource selected.</param>
        /// <param name="clip">AudioClip to play.</param>
        /// <returns></returns>
        private IEnumerator CO_ChangeMusic(AudioSource source, AudioClip clip) {
            if (ReferenceEquals(null, source))
                yield break;

            if (ReferenceEquals(null, clip))
                yield break;

            yield return CO_FadeOut(source);
            SetMusicClip(clip);
            yield return CO_FadeIn(source);
        }
        #endregion
    }
}