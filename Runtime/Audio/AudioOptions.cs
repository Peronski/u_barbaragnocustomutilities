using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Barbaragno.CustomUtilities.SerializableUtility.Dictionary;
using Barbaragno.CustomUtilities.SerializableUtility.Tuple;
using Barbaragno.CustomUtilities.SerializableUtility.List;

namespace Barbaragno.CustomUtilities.Audio {
    [System.Serializable]
    public struct AudioSetting {
        [SerializeField] public float CurrentValue;
        [SerializeField] public float SavedValue;
        [SerializeField] public bool IsMute;

        public AudioSetting(float currentV = 1.0f, float savedV = 1.0f, bool isMute = false) { CurrentValue = currentV; SavedValue = savedV; IsMute = isMute; }
    }

    [System.Serializable]
    public class AudioOptions {
        #region Private Variables
        #endregion

        #region Public Variables
        public SerializableDictionary<string, AudioSetting> audioSettingDB;
        #endregion

        #region Properties
        public AudioSetting GetAudioSetting(string key) => audioSettingDB[key];
        #endregion

        #region Constructors
        public AudioOptions() { audioSettingDB = new SerializableDictionary<string, AudioSetting>(); }
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        public SerializableList<SerializableTuple<string, AudioSetting>> TransmuteDictionary() {
            SerializableList<SerializableTuple<string, AudioSetting>> list = new SerializableList<SerializableTuple<string, AudioSetting>>();

            foreach(var pair in audioSettingDB) {
                list.Add(new SerializableTuple<string, AudioSetting>(pair.Key, pair.Value));
            }
            return list;
        }

        public void TransmuteList(SerializableList<SerializableTuple<string, AudioSetting>> list) {
            if (ReferenceEquals(null, list)) {
                audioSettingDB = new SerializableDictionary<string, AudioSetting>();
                return;
            }

            foreach(var tupla in list) {
                audioSettingDB.AddDefinition(tupla.Item1, tupla.Item2);
            }
        }
        #endregion
    }
}
