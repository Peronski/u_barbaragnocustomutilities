using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using Barbaragno.CustomUtilities.Singletons;

namespace Barbaragno.CustomUtilities.Audio {
    public class VolumeSelector : MonoBehaviour {
        #region Private Variables
        private bool _disableToogleEvent;
        private float _currentValue = 1.0f;
        private float _savedValue = 1.0f;
        #endregion

        #region Public Variables
        [Header("Master Mixer")]
        [SerializeField] private AudioMixer audioMixer;

        [Header("Exposed Parameter Group")]
        [SerializeField] private string exposedParameter;

        [Header("Player Prefs Key")]
        [SerializeField] private string currentKey;
        [SerializeField] private string savedKey;

        [Header("UI Elements")]
        [SerializeField] private Slider volumeSlider;
        [SerializeField] private Toggle muteToggle;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        private void Start() {
            AudioManager.Instance.OnLoadAudioDataEvent += LoadAudioSetting;
            AudioManager.Instance.OnSaveAudioDataEvent += SaveAudioSetting;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Loads volume settings referred to a key.
        /// </summary>
        /// <param name="prefKey">Key links to specific volume setting.</param>
        private void LoadVolume(string prefKey) {
            float mixerVol = PlayerPrefs.GetFloat(prefKey, 1.0f);
            volumeSlider.value = mixerVol;
            audioMixer.SetFloat(exposedParameter, 30.0f * Mathf.Log10(mixerVol));
        }

        private void LoadVolume(float value = 1.0f) {
            volumeSlider.value = value;
            audioMixer.SetFloat(exposedParameter, 30.0f * Mathf.Log10(value));
        }

        /// <summary>
        /// Saves volume settings referred to a key.
        /// </summary>
        /// <param name="prefKey">Key links to specific volume setting.</param>
        private void SaveVolume(string prefKey) {
            PlayerPrefs.SetFloat(prefKey, volumeSlider.value);
        }

        private void SaveAudioSetting() {
            AudioSetting audioSetting = new AudioSetting(_currentValue, _savedValue, !muteToggle.isOn);
            AudioManager.Instance.SaveAudioSetting(exposedParameter, audioSetting);
        }

        private void LoadAudioSetting() {
            AudioSetting audioSetting = AudioManager.Instance.LoadAudioSetting(exposedParameter);

            if (ReferenceEquals(null, audioSetting))
                return;

            _currentValue = audioSetting.CurrentValue;
            _savedValue = audioSetting.SavedValue;
            muteToggle.SetIsOnWithoutNotify(!audioSetting.IsMute);

            LoadVolume(_savedValue);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Sets volume's value to new value. 
        /// Linked to slider's OnValueChanged event.
        /// </summary>
        /// <param name="value">Passed value.</param>
        public void SetVolume(float value) {
            _savedValue = value;
            float volume = 30.0f * Mathf.Log10(value);
            //PlayerPrefs.SetFloat(savedKey, value);
            audioMixer.SetFloat(exposedParameter, volume);
            _disableToogleEvent = true;
            muteToggle.isOn = volumeSlider.value > volumeSlider.minValue;
            _disableToogleEvent = false;
        }

        /// <summary>
        /// Mutes volume.
        /// Linked to toggle's OnValueChanged event.
        /// </summary>
        /// <param name="enabledSound">Enabled(true)/Disabled(false).</param>
        public void Mute(bool enabledSound) {
            if (_disableToogleEvent)
                return;

            if (!enabledSound) {
                //SaveVolume(currentKey);
                _currentValue = volumeSlider.value;
                SetVolume(volumeSlider.minValue);
                volumeSlider.value = volumeSlider.minValue;
                return;
            }

            //LoadVolume(currentKey);
            LoadVolume(_currentValue);
        }
        #endregion
    }
}
