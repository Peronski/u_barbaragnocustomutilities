using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Barbaragno.CustomUtilities.Data.EventSO;

namespace Barbaragno.CustomUtilities.Data.EventSO.Audio {
    [System.Serializable]
    public class MusicEventHandlerSO : EventHandlerSO {
        #region Private Variables
        #endregion

        #region Public Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
