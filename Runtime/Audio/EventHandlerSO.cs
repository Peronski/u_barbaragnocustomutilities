using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Barbaragno.CustomUtilities.Data.EventSO {
    public class EventHandlerSO : ScriptableObject {
        #region Private Variables
        #endregion

        #region Public Variables
        public System.Action OnEvent;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        private void Awake() {
            OnEvent = null;
        }
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        public virtual void RaiseEvent() {
            OnEvent?.Invoke();
        }
        #endregion
    }
}
