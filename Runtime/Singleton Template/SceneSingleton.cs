using UnityEngine;

namespace Barbaragno.CustomUtilities.Singletons {
    /// <summary>
    /// This base class can be used to generate a singleton that lives inside a scene and 'dies' with it.
    /// </summary>
    /// <typeparam name="T">The type of the class that's inheriting from this one. </typeparam>
    public class SceneSingleton<T> : MonoBehaviour where T : MonoBehaviour {
        #region Public Variables
        #endregion

        #region Private Variables
        private static T _instance;
        #endregion

        #region Properties
        public static T Instance => _instance;
        #endregion

        #region Behaviour Callbacks
        protected virtual void Awake () {
            if(_instance != null)
                Destroy(_instance.gameObject);

            _instance = this as T;
        }
        #endregion

        #region Private Methods
        #endregion

        #region Public Functions
        #endregion
    }
}