using UnityEngine;

namespace Barbaragno.CustomUtilities.Singletons {
    /// <summary>
    /// This base class can be used to generate a singleton that survives to scene changes.
    /// </summary>
    /// <typeparam name="T">The type of the class that's inheriting from this one. </typeparam>
    public class GeneralSingleton<T> : MonoBehaviour where T : MonoBehaviour {
        #region Public Variables
        #endregion

        #region Private Variables
        private static T _instance;
        #endregion

        #region Properties
        public static T Instance => _instance;
        #endregion

        #region Behaviour Callbacks
        protected virtual void Awake () {
            if(_instance != null)
                Destroy(_instance.gameObject);

            _instance = this as T;
            DontDestroyOnLoad(_instance);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Public Functions
        #endregion
    }
}