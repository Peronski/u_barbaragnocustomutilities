using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Barbaragno.DataAsset.SingletonSO {
    public abstract class SingletonSO<T> : ScriptableObject where T : SingletonSO<T> {
        #region Private Variables
        protected static bool _isInitialized = false;
        protected static T _instance;
        #endregion

        #region Public Variables
        #endregion

        #region Properties
        public static T Instance {
            get {
                if (!_isInitialized) {
                    if (_instance == null) {
                        T[] resources = Resources.LoadAll<T>("");
                        if (resources == null || resources.Length < 1) {
                            new System.Exception("Relative SingletonSO asset not found in Resources");
                        } else if (resources.Length > 1) {
                            Debug.LogWarning($"Finding more than one fittable SingletonSO asset in Resources. Loading the first one.");
                        }
                        _instance = resources[0];
                    }
                }
                return _instance;
            }
        }
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
