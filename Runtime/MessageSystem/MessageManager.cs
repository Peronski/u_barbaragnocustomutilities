namespace MessageSystem {

    public static partial class MessageManager {

        public readonly static MessageChannel<GlobalMessageTypes> Global = new MessageChannel<GlobalMessageTypes>();

    }

    public enum GlobalMessageTypes {
        None,
        OnApplicationFocus,
        OnApplicationPause,
        OnApplicationQuit,
    }
}