using MessageSystem;
using UnityEditor;
using UnityEngine;

namespace UpSurgeOnEditor {

    [CustomEditor(typeof(MessageInstance))]
    public class MessageInstanceEditor : Editor {

        GlobalMessageTypes messageType = GlobalMessageTypes.None;

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            EditorGUILayout.BeginHorizontal();
            {
                messageType = (GlobalMessageTypes)EditorGUILayout.EnumPopup("Message Type", messageType);

                if (messageType != GlobalMessageTypes.None) {
                    if (GUILayout.Button("Send Message")) {
                        MessageClass message = null;
                        MessageManager.Global.SendMessage(messageType, message);
                    }
                }
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}