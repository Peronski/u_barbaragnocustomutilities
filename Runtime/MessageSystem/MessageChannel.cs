using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MessageSystem {

    public class MessageChannel<T> {

        #region Static

        private static void Attach(Dictionary<T, List<Action<T, MessageClass>>> listeners, T type, Action<T, MessageClass> handler) {
            MessageInstance.CreateIfNotExist();

            if (!listeners.ContainsKey(type)) {
                listeners.Add(type, new List<Action<T, MessageClass>>());
            }

            List<Action<T, MessageClass>> listenerList = listeners[type];

            if (listenerList.Contains(handler)) {
                Debug.LogError($"[MessageSystem] Attached duplicate listener: {type}");
            } else {
                listenerList.Add(handler);
            }
        }

        private static void Detach(Dictionary<T, List<Action<T, MessageClass>>> listeners, T type, Action<T, MessageClass> handler) {
            if (!listeners.ContainsKey(type)) {
                Debug.LogError($"[MessageManager] Detached non-existant listener: {type}");
            } else {
                listeners[type].Remove(handler);
                if (listeners[type].Count == 0) {
                    listeners.Remove(type);
                }
            }
        }

        private static bool Send(Dictionary<T, List<Action<T, MessageClass>>> listeners, Dictionary<T, List<MessageClass>> pending, T type, MessageClass messageClass, bool isAsyncCall) {
            if (!listeners.ContainsKey(type)) {
                return false;
            }

            if (isAsyncCall) {
                MessageInstance.CreateIfNotExist();

                if (pending.TryGetValue(type, out List<MessageClass> messages)) {
                    messages.Add(messageClass);
                } else {
                    pending.Add(type, new List<MessageClass> {
                        messageClass
                    });
                }
            } else {
                Send(type, listeners[type], messageClass);
            }

            return true;
        }

        private static void Send(T type, List<Action<T, MessageClass>> handlers, MessageClass message) {
            for (int i = handlers.Count - 1; i >= 0; i--) { // The order of the call is not important, the important thing is that it does not explode if one is detached during the loop.
                handlers[i](type, message);
            }
        }

        private static void OnUpdate(Dictionary<T, List<Action<T, MessageClass>>> listeners, Dictionary<T, List<MessageClass>> pendings) {
            KeyValuePair<T, List<MessageClass>>[] messageToSend = pendings.ToArray();

            for (int i = 0; i < messageToSend.Length; i++) {
                for (int j = 0; j < messageToSend[i].Value.Count; j++) {
                    Send(listeners, pendings, messageToSend[i].Key, messageToSend[i].Value[j], false);
                }
            }

            pendings.Clear();
        }

        #endregion Static

        #region C-Sharp Object

        private bool globalUpdateAttached = false;
        private readonly Dictionary<T, List<Action<T, MessageClass>>> globalListeners = new Dictionary<T, List<Action<T, MessageClass>>>();
        private readonly Dictionary<T, List<MessageClass>> globalPending = new Dictionary<T, List<MessageClass>>();

        public void AttachListener(T type, Action<T, MessageClass> handler) {
            Attach(globalListeners, type, handler);
        }

        public void DetachListener(T type, Action<T, MessageClass> handler) {
            Detach(globalListeners, type, handler);
        }

        public bool SendMessage(T type, MessageClass message = null, bool isAsyncCall = false) {
            if (isAsyncCall && !globalUpdateAttached) {
                globalUpdateAttached = true;
                MessageInstance.AddUpdateHandler(OnUpdateGlobal);
            }
            return Send(globalListeners, globalPending, type, message, isAsyncCall);
        }

        private void OnUpdateGlobal() {
            globalUpdateAttached = false;
            MessageInstance.RemoveUpdateHandler(OnUpdateGlobal);
            OnUpdate(globalListeners, globalPending);
        }

        #endregion C-Sharp Object
    }
}