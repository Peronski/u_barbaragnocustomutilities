﻿using System;
using UnityEngine;

namespace MessageSystem {

    [Serializable]
    public class MessageClass {
        public static bool TryCastTo<T>(MessageClass fromMessage, out T toMessage) where T : MessageClass {
            toMessage = fromMessage as T;

            if (toMessage == null) {
                Debug.LogError($"Error Message Cast: {fromMessage}");
                return false;
            }

            return true;
        }
    }

    public class ValueMessage<T> : MessageClass {
        public T Value { get; private set; }

        public ValueMessage(T value) {
            Value = value;
        }
    }
}