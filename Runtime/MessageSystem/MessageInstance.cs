using System;
using System.Collections.Generic;
using UnityEngine;

namespace MessageSystem {

    public class MessageInstance : MonoBehaviour {

        private static MessageInstance Instance { get; set; }
        private static List<Action> actions = new List<Action>();

        private void Awake() {
            if (Instance == null || Instance.gameObject == null) {
                Initialize();
            } else {
                Destroy(gameObject);
            }
        }

        private void Update() {
            for (int i = actions.Count - 1; i >= 0; i--) {
                actions[i]?.Invoke();
            }
        }

        private void OnApplicationFocus(bool focus) {
            MessageManager.Global.SendMessage(GlobalMessageTypes.OnApplicationFocus, new ValueMessage<bool>(focus));
        }

        private void OnApplicationPause(bool pause) {
            MessageManager.Global.SendMessage(GlobalMessageTypes.OnApplicationPause, new ValueMessage<bool>(pause));
        }

        private void OnApplicationQuit() {
            MessageManager.Global.SendMessage(GlobalMessageTypes.OnApplicationQuit);
        }

        private void Initialize() {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }

        internal static void AddUpdateHandler(Action action) {
            if (actions.Contains(action)) {
                Debug.LogError("[MessageSystem] Action duplicated");
                return;
            }

            actions.Add(action);
        }

        internal static void RemoveUpdateHandler(Action action) {
            if (!actions.Contains(action)) {
                Debug.LogError("[MessageSystem] Action non-existant");
                return;
            }

            actions.Remove(action);
        }

        public static void CreateIfNotExist() {
            if (Instance == null) {
                new GameObject("Message Instance").AddComponent<MessageInstance>();
            }
        }
    }
}