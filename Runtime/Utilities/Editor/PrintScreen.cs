﻿using System;
using UnityEditor;
using UnityEngine;

public static class PrintScreen {

	[MenuItem("Tools/Print Screen %F1")]
	public static void PrtScr() {
		string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
		ScreenCapture.CaptureScreenshot($"{path}/PrtScr.png");
	}
}