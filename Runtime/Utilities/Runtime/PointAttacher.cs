using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Interactions {
    public class PointAttacher : MonoBehaviour {
        [Header("References")]
        [SerializeField] private Transform attachPoint;

        public void AttachToPoint(Transform transformToAttach) {
            transformToAttach.SetParent(attachPoint);
        }

        public void MoveToAttachPoint(Transform transformToAttach) {
            transformToAttach.position = attachPoint.position + (attachPoint.forward * transformToAttach.localScale.z);
        }

        public void AlignRotation(Transform transformToAttach) {
            transformToAttach.rotation = attachPoint.rotation;
        }

        public Transform GetAttachPoint() => attachPoint;
    }
}
