using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Utilities {
    public class GameDebugConsole : MonoBehaviour {
        [Header("References")]
        [SerializeField] private TextMeshProUGUI debugText;
        [SerializeField] private TextMeshProUGUI warningText;
        [SerializeField] private TextMeshProUGUI errorText;

        private Dictionary<LogType, Action<string, string>> actions = new Dictionary<LogType, Action<string, string>>();

        private void Awake() {
            ConfigureTextColors();

            actions.Add(LogType.Warning, DisplayWarning);
            actions.Add(LogType.Error, DisplayError);
            actions.Add(LogType.Log, DisplayLog);

            Application.logMessageReceived += DisplayLog;
        }

        private void DisplayLog(string condition, string stackTrace, LogType type) {
            if (!actions.ContainsKey(type))
                return;

            actions[type]?.Invoke(condition, stackTrace);
        }

        private void ConfigureTextColors() {
            debugText.color = Color.white;
            warningText.color = Color.yellow;
            errorText.color = Color.red;
        }
        private void DisplayWarning(string condition, string stackTrace) {
            warningText.text = condition;
        }
        private void DisplayError(string condition, string stackTrace) {
            errorText.text = condition;
        }
        private void DisplayLog(string condition, string stackTrace) {
            debugText.text = condition;
        }
    }
}
