using UnityEngine;

public class DebugManager : GeneralSingleton<DebugManager> {
    [Header("Settings")]
    [SerializeField] private bool onDebug = false;
    [SerializeField] private bool fpsEnabled = false;

    [Header("References")]
    [SerializeField] private Canvas fpsCanvas;
    [SerializeField] private Canvas consoleCanvas;

    private void Start() {
        Configure();
    }
    private void Configure() {
        fpsCanvas.enabled = fpsEnabled;
        consoleCanvas.enabled = onDebug;
    }
}
