using TMPro;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Utilities {
    public class FPSCounter : MonoBehaviour {
        [Header("References")]
        [SerializeField] private TextMeshProUGUI textMesh = default;

        private int fps;
        private float updateTime;
        private int targetFPS =
#if UNITY_ANDROID // GEARVR
        60;
#else
        75;
#endif

        private void Update() {
            if (textMesh == null)
                return;

            fps++;
            updateTime += Time.unscaledDeltaTime;

            if (updateTime >= 1f) {
                updateTime = 0;
                textMesh.text = $"{fps} FPS";
                textMesh.color = (fps > (targetFPS - 5) ? Color.green :
                             (fps > (targetFPS - 30) ? Color.yellow :
                              Color.red));
                fps = 0;
            }
        }
    }
}