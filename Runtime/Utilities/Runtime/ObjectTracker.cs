using UnityEngine;
using Microsoft.MixedReality.Toolkit;
using System;

namespace UpSurgeOn.MetaverseRuntime.Utilities {
    public class ObjectTracker : MonoBehaviour {
        [Header("Base Settings")]
        [SerializeField] protected float moveLerpSpeed;
        [SerializeField] protected float rotationLerpSpeed;
        [Space]
        [SerializeField] protected Vector3 offsetPosition;
        [Space]
        [SerializeField] protected float offsetRotationAxisX = 0;
        [SerializeField] protected float offsetRotationAxisY = 0;
        [SerializeField] protected float offsetRotationAxisZ = 0;
        [Space]
        [SerializeField] protected bool usePositionConstraint;
        [EnumFlags][SerializeField] protected Axis axisConstraintPosition;
        [Space]
        [SerializeField] protected bool useRotationConstraint;
        [EnumFlags][SerializeField] protected Axis axisConstraintRotation;
        [Space]
        [SerializeField] protected bool confOnEnable;

        [Header("Base References")]
        [SerializeField] protected Transform trackedObject;

        protected Vector3 startingPosition;
        protected Quaternion startingRotation;

        protected virtual void Start() {
            transform.position = DefaultTargetPosition();
            startingPosition = transform.position;
            startingRotation = transform.rotation;
        }

        protected virtual void Update() {
            UpdatePosition();
            UpdateRotation();
        }

        protected virtual void OnEnable() {
            if (confOnEnable)
                transform.position = DefaultTargetPosition();
        }

        private Vector3 DefaultTargetPosition() {
            return trackedObject.right * (offsetPosition.x + trackedObject.position.x) + trackedObject.up * (offsetPosition.y + trackedObject.position.y) + trackedObject.forward * (offsetPosition.z + trackedObject.position.z);
        }

        private Quaternion DefaultTargetRotation() {
            return Quaternion.Euler(trackedObject.rotation.eulerAngles.x + offsetRotationAxisX, trackedObject.rotation.eulerAngles.y + offsetRotationAxisY, trackedObject.rotation.eulerAngles.z + offsetRotationAxisZ);
        }

        private Quaternion DefaultTargetRotationWithConstraintX() {
            Vector3 rotableAxis = trackedObject.rotation.eulerAngles;
            return Quaternion.Euler(startingRotation.x + offsetRotationAxisX, rotableAxis.y + offsetRotationAxisY, rotableAxis.z + offsetRotationAxisZ);
        }

        private Quaternion DefaultTargetRotationWithConstraintY() {
            Vector3 rotableAxis = trackedObject.rotation.eulerAngles;
            return Quaternion.Euler(rotableAxis.x + offsetRotationAxisX, startingRotation.y + offsetRotationAxisY, rotableAxis.z + offsetRotationAxisZ);
        }

        private Quaternion DefaultTargetRotationWithConstraintZ() {
            Vector3 rotableAxis = trackedObject.rotation.eulerAngles;
            return Quaternion.Euler(rotableAxis.x + offsetRotationAxisX, rotableAxis.y + offsetRotationAxisY, startingRotation.z + offsetRotationAxisZ);
        }

        private Quaternion DefaultTargetRotationWithConstraintXY() {
            Vector3 rotableAxis = trackedObject.rotation.eulerAngles;
            return Quaternion.Euler(startingRotation.x + offsetRotationAxisX, startingRotation.y + offsetRotationAxisY, rotableAxis.z + offsetRotationAxisZ);
        }

        private Quaternion DefaultTargetRotationWithConstraintXZ() {
            Vector3 rotableAxis = trackedObject.rotation.eulerAngles;
            return Quaternion.Euler(startingRotation.x + offsetRotationAxisX, rotableAxis.y + offsetRotationAxisY, startingRotation.z + offsetRotationAxisZ);
        }

        private Quaternion DefaultTargetRotationWithConstraintYZ() {
            Vector3 rotableAxis = trackedObject.rotation.eulerAngles;
            return Quaternion.Euler(rotableAxis.x + offsetRotationAxisX, startingRotation.y + offsetRotationAxisY, startingRotation.z + offsetRotationAxisZ);
        }

        private Quaternion DefaultTargetRotationWithConstraintAllAxis() {
            return startingRotation;
        }

        private Vector3 DefaultTargetPositionWithConstraintX() {
            Vector3 movableAxis = trackedObject.up * (offsetPosition.y + trackedObject.position.y) + trackedObject.forward * (offsetPosition.z + trackedObject.position.z);
            return new Vector3(startingPosition.x, movableAxis.y, movableAxis.z);
        }

        private Vector3 DefaultTargetPositionWithConstraintY() {
            Vector3 movableAxis = trackedObject.right * (offsetPosition.x + trackedObject.position.x) + trackedObject.forward * (offsetPosition.z + trackedObject.position.z);
            return new Vector3(movableAxis.x, startingPosition.y, movableAxis.z);
        }

        private Vector3 DefaultTargetPositionWithConstraintZ() {
            Vector3 movableAxis = trackedObject.right * (offsetPosition.x + trackedObject.position.x) + trackedObject.up * (offsetPosition.y + trackedObject.position.y);
            return new Vector3(movableAxis.x, movableAxis.y, startingPosition.z);
        }

        private Vector3 DefaultTargetPositionWithConstraintXY() {
            Vector3 movableAxis = trackedObject.forward * (offsetPosition.z + trackedObject.position.z);
            return new Vector3(startingPosition.x, startingPosition.y, movableAxis.z);
        }

        private Vector3 DefaultTargetPositionWithConstraintYZ() {
            Vector3 movableAxis = trackedObject.right * (offsetPosition.x + trackedObject.position.x);
            return new Vector3(movableAxis.x, startingPosition.y, startingPosition.z);
        }

        private Vector3 DefaultTargetPositionWithConstraintXZ() {
            Vector3 movableAxis = trackedObject.up * (offsetPosition.y + trackedObject.position.y);
            return new Vector3(startingPosition.x, movableAxis.y, startingPosition.z);
        }

        private Vector3 DefaultTargetPositionWithConstraintAllAxis() {
            return new Vector3(startingPosition.x + offsetPosition.x, startingPosition.y + offsetPosition.y, startingPosition.z + offsetPosition.z);
        }

        private Vector3 TargetPositionWithConstraint(Axis axis) {

            if ((int)axis == -1)
                return DefaultTargetPositionWithConstraintAllAxis();

            if ((int)axis == 5)
                return DefaultTargetPositionWithConstraintXZ();

            if ((int)axis == 3)
                return DefaultTargetPositionWithConstraintXY();

            if ((int)axis == 6)
                return DefaultTargetPositionWithConstraintYZ();

            switch (axis) {
                case Axis.X: return DefaultTargetPositionWithConstraintX();
                case Axis.Y: return DefaultTargetPositionWithConstraintY();
                case Axis.Z: return DefaultTargetPositionWithConstraintZ();
            }

            return DefaultTargetPosition();
        }

        private Quaternion TargetRotationWithConstraint(Axis axis) {

            if ((int)axis == -1)
                return DefaultTargetRotationWithConstraintAllAxis();

            if ((int)axis == 5)
                return DefaultTargetRotationWithConstraintXZ();

            if ((int)axis == 3)
                return DefaultTargetRotationWithConstraintXY();

            if ((int)axis == 6)
                return DefaultTargetRotationWithConstraintYZ();

            switch (axis) {
                case Axis.X: return DefaultTargetRotationWithConstraintX();
                case Axis.Y: return DefaultTargetRotationWithConstraintY();
                case Axis.Z: return DefaultTargetRotationWithConstraintZ();
            }

            return DefaultTargetRotation();
        }

        private void UpdatePosition() {
            if (!usePositionConstraint)
                transform.position = Vector3.Lerp(transform.position, DefaultTargetPosition(), moveLerpSpeed * Time.deltaTime);
            else
                transform.position = Vector3.Lerp(transform.position, TargetPositionWithConstraint(axisConstraintPosition), moveLerpSpeed * Time.deltaTime);
        }

        private void UpdateRotation() {
            if (!useRotationConstraint)
                transform.rotation = Quaternion.Lerp(transform.rotation, DefaultTargetRotation(), rotationLerpSpeed * Time.deltaTime);
            else
                transform.rotation = Quaternion.Lerp(transform.rotation, TargetRotationWithConstraint(axisConstraintRotation), rotationLerpSpeed * Time.deltaTime);
        }
    }
}
