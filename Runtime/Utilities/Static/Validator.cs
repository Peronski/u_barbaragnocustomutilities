using System.Linq;
using System.Text.RegularExpressions;

namespace UpSurgeOnRuntime.StaticClass {
    public static class Validator {

        private static readonly Regex regex = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$", RegexOptions.None);

        private static readonly int[] EXPECTED_ERRORS = new int[] {
            8, // IncorrectEmail
            16, // AlreadyExistsEmail
            17, // AlreadyExistsUserName
            18, // IncorrectPassword
        };

        public static bool IsExpectedError(int errorCode) {
            for (int i = 0; i < EXPECTED_ERRORS.Length; i++) {
                if (errorCode == EXPECTED_ERRORS[i]) {
                    return true;
                }
            }

            return false;
        }

        public static bool IsValidSignUpData(string email, string password, bool termsAndConditionsAccepted, out string errorMessage) {
            if (!termsAndConditionsAccepted) {
                errorMessage = "You must accept the terms and conditions ans the privacy policy.";
                return false;
            }

            if (!IsValidSignInData(email, password, out errorMessage)) {
                return false;
            }

            return true;
        }

        public static bool IsValidSignInData(string email, string password, out string errorMessage) {
            if (!IsValidEmail(email, out errorMessage)) {
                return false;
            }

            if (!IsValidPass(password, out errorMessage)) {
                return false;
            }

            return true;
        }

        public static bool IsValidEmail(string email, out string errorMessage) {
            if (string.IsNullOrEmpty(email)) {
                errorMessage = "You must add your Email.";
                return false;
            }

            if (!regex.IsMatch(email)) {
                errorMessage = "Email invalid.";
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }

        public static bool IsValidPass(string password, out string errorMessage) {
            if (!IsPossibleCorrectPass(password, out errorMessage)) {
                return false;
            }

            if (string.IsNullOrEmpty(password)) {
                errorMessage = "You must add your Password.";
                return false;
            }

            if (password.Length < 8) {
                errorMessage = "The password must contain a minimum of 8 characters.";
                return false;
            }

            if (!password.Any(char.IsLower)) {
                errorMessage = "The password must contain at least one lower character.";
                return false;
            }

            if (!password.Any(char.IsDigit)) {
                errorMessage = "The password must contain at least one digit.";
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }

        public static bool IsPossibleCorrectPass(string password, out string errorMessage) {
            if (string.IsNullOrEmpty(password)) {
                errorMessage = "You must add your Password.";
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }

        public static bool IsValidConfirmPass(string confirmPassword, string pass, out string errorMessage) {
            if (string.IsNullOrEmpty(pass)) {
                errorMessage = string.Empty; // The fuction IsValidPass will write the error.
                return false;
            }

            if (confirmPassword != pass) {
                errorMessage = "Does not match with Password.";
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }

        public static bool IsValidUsername(string username, out string errorMessage) {
            if (string.IsNullOrEmpty(username)) {
                errorMessage = "You must add your username.";
                return false;
            }

            if (username.Contains(' ')) {
                errorMessage = "The username cannot contain spaces.";
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }
    }
}