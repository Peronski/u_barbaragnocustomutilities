﻿using System;

namespace UpSurgeOnRuntime.StaticClass {

    public static class Log {

        private const string PREFIX_LOG = "UPSURGEON_LOG: ";
        private const string PREFIX_WARNING = "UPSURGEON_WARNING: ";
        private const string PREFIX_ERROR = "UPSURGEON_ERROR: ";

        public static void Debug(string text) => UnityEngine.Debug.Log($"{PREFIX_LOG}{text}");

        public static void Warning(string text) => UnityEngine.Debug.LogWarning($"{PREFIX_WARNING}{text}");

        public static void Error(Exception exception) => UnityEngine.Debug.LogError($"{PREFIX_ERROR}{exception}");

        public static void Error(string text) => UnityEngine.Debug.LogError($"{PREFIX_ERROR}{text}");

        private static void Error(string text, ErrorTypes errorType) => UnityEngine.Debug.LogError($"{PREFIX_ERROR}{errorType}\n{text}");

        public static void ErrorIndex(int index) => Error($"{index}", ErrorTypes.Index);

        public static void ErrorNullReference(string name) => Error($"{name}", ErrorTypes.NullReference);

        public static void ErrorCastMessage(string nameClass) => Error($"{nameClass}", ErrorTypes.Cast);

        public static void EnumNotImplemented(string nameEnum) => Error($"Enum not implemented: {nameEnum}");

        private enum ErrorTypes {
            Index,
            NullReference,
            Cast,
        }
    }
}