﻿using Newtonsoft.Json;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace UpSurgeOnRuntime.StaticClass {

    public static class BinariesManager {

        public static bool Exist(BinaryFileTypes binaryFileType) {
            string filename = binaryFileType.ToString();
            return Application.platform == RuntimePlatform.WebGLPlayer ? PlayerPrefs.HasKey(filename) : File.Exists(GetFilepath(filename));
        }

        public static T Load<T>(BinaryFileTypes binaryFileType) where T : class {
            string filename = binaryFileType.ToString();
            T data = null;

            if (Application.platform == RuntimePlatform.WebGLPlayer) {
                string jsonData = PlayerPrefs.GetString(filename, string.Empty);

                if (!string.IsNullOrEmpty(jsonData)) {
                    data = JsonConvert.DeserializeObject<T>(jsonData);
                }

            } else {
                string filepath = GetFilepath(filename);

                if (File.Exists(filepath)) {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    FileStream fileStream = File.Open(filepath, FileMode.Open);

                    data = binaryFormatter.Deserialize(fileStream) as T;
                    fileStream.Close();
                }
            }

            if (data != null) {
                Log.Debug($"File loaded: {filename}");
            }



            return data;
        }

        public static void Save<T>(T data, BinaryFileTypes binaryFileType) where T : class {
            string filename = binaryFileType.ToString();

            if (Application.platform == RuntimePlatform.WebGLPlayer) {
                PlayerPrefs.SetString(filename, JsonConvert.SerializeObject(data));
                PlayerPrefs.Save();

            } else {
                string filepath = GetFilepath(filename);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                FileStream fileStream = File.Open(filepath, FileMode.OpenOrCreate);

                binaryFormatter.Serialize(fileStream, data);
                fileStream.Close();
            }

            Log.Debug($"File saved: {filename}");
        }

        public static void Delete(BinaryFileTypes binaryFileType) {
            string filename = binaryFileType.ToString();

            if (Application.platform == RuntimePlatform.WebGLPlayer) {
                if (PlayerPrefs.HasKey(filename)) {
                    PlayerPrefs.DeleteKey(filename);
                }

            } else {
                string filepath = GetFilepath(filename);
                if (File.Exists(filepath)) {
                    File.Delete(filepath);
                }
            }

            Log.Debug($"File deleted: {filename}");
        }

        private static string GetFilepath(string filename) => Path.Combine(Application.persistentDataPath, $"{filename}.bin");
    }

    public enum BinaryFileTypes {
        LoginResponse,
        PreferencesData,
        UserData,
    }
}