using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Utilities {
    public static class VectorUtility {

        private static float GetClampedValueOnZ(this Vector3 vector, float minValue, float maxValue) {
            return Mathf.Clamp(vector.z, minValue, maxValue);
        }
        private static float GetClampedValueOnY(this Vector3 vector, float minValue, float maxValue) {
            return Mathf.Clamp(vector.y, minValue, maxValue);
        }
        private static float GetClampedValueOnX(this Vector3 vector, float minValue, float maxValue) {
            return Mathf.Clamp(vector.x, minValue, maxValue);
        }

        public static float GetClampedValueOnAxis(this Vector3 targetPosition, Axis axis, float minValue, float maxValue) {
            switch (axis) {
                case Axis.X: return GetClampedValueOnX(targetPosition, minValue, maxValue);
                case Axis.Y: return GetClampedValueOnY(targetPosition, minValue, maxValue);
                case Axis.Z: return GetClampedValueOnZ(targetPosition, minValue, maxValue);
                default:
                    return 0;
            }
        }

        private static float GetLerpValueOnAxisZ(this Vector3 valueA, Vector3 valueB, float lerpSpeed = 1) {
            return Mathf.Lerp(valueA.z, valueB.z, lerpSpeed);
        }
        private static float GetLerpValueOnAxisY(this Vector3 valueA, Vector3 valueB, float lerpSpeed = 1) {
            return Mathf.Lerp(valueA.y, valueB.y, lerpSpeed);
        }
        private static float GetLerpValueOnAxisX(this Vector3 valueA, Vector3 valueB, float lerpSpeed = 1) {
            return Mathf.Lerp(valueA.x, valueB.x, lerpSpeed);
        }

        /// <summary>
        /// !!! Execute this method only in Update !!!
        /// </summary>
        public static float GetLerpValueOnAxis(this Vector3 valueA, Vector3 valueB, Axis axis, float lerpSpeed = 1) {
            switch (axis) {
                case Axis.X: return GetLerpValueOnAxisX(valueA, valueB, lerpSpeed);
                case Axis.Y: return GetLerpValueOnAxisY(valueA, valueB, lerpSpeed);
                case Axis.Z: return GetLerpValueOnAxisZ(valueA, valueB, lerpSpeed);
                default:
                    return 0;
            }
        }
    }
}
