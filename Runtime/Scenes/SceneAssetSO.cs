using UnityEngine;
using UnityEditor;

namespace UpSurgeOn.MetaverseRuntime.SerializableData {
    [CreateAssetMenu(fileName = "New SceneAssetSO", menuName = "ScriptableObjects/SceneAsset")]
    public class SceneAssetSO : ScriptableObject, ISerializationCallbackReceiver {
        [field: SerializeField] public SceneTypes Type { get; private set; }
        [field: SerializeField] public Object SceneAsset { get; private set; }
        public string SceneName { get; private set; }
        public string FilePath { get; private set; }

        [HideInInspector][SerializeReference] private string sceneName;

#if UNITY_EDITOR
        private void OnValidate() {
            if (SceneAsset is SceneAsset) {
                SceneName = SceneAsset.name;
                FilePath = AssetDatabase.GetAssetPath(SceneAsset);
            }
        }
#endif

        public void OnBeforeSerialize() {
            sceneName = SceneName;
        }

        public void OnAfterDeserialize() {
            SceneName = sceneName;
        }
    }
}
