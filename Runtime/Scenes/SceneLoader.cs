using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UpSurgeOn.MetaverseRuntime.SerializableData;

namespace UpSurgeOn.MetaverseRuntimeRuntime.Singletons {
    public class SceneLoader : GeneralSingleton<SceneLoader> {
        [Header("References")]
        public List<SceneAssetSO> scenes = new List<SceneAssetSO>();

        private SceneTypes previousBaseScene;
        private SceneTypes currentBaseScene;
        private SceneTypes previousAdditiveScene;
        private SceneTypes currentAdditiveScene;

        private AsyncOperation asyncOperation;
        private Coroutine coroutine;

        public event Action<SceneTypes> OnLoadSceneBegin;
        public event Action<float> OnLoadingScene;
        public event Action<SceneTypes> OnLoadSceneEnd;

        public event Action<SceneTypes> OnUnloadSceneBegin;
        public event Action<float> OnUnloadingScene;
        public event Action<SceneTypes> OnUnloadSceneEnd;

        protected override void Awake() {
            base.Awake();
            SceneManager.sceneLoaded += SceneLoaded;
            SceneManager.sceneUnloaded += SceneUnloaded;
        }

        private void SceneLoaded(Scene scene, LoadSceneMode mode) {
            SceneTypes type = GetSceneType(scene.name);
            OnLoadSceneEnd?.Invoke(type);
        }
        private void SceneUnloaded(Scene scene) {
            SceneTypes type = GetSceneType(scene.name);
            OnUnloadSceneEnd?.Invoke(type);
        }

        //Main Function
        public void LoadScene(SceneTypes type, bool asyncOperation, bool unloadCurrent = false, LoadSceneMode mode = LoadSceneMode.Single) {
            SceneAssetSO sceneSO = scenes.First(scene => scene.Type == type);

            if (sceneSO == null) {
                Debug.Log($"[SceneLoader] - SceneAssetSO is null. Invalid result with SceneTypes {type}.");
                return;
            }

            if (asyncOperation) {
                if (mode == LoadSceneMode.Single)
                    StartCO_LoadBaseScene(sceneSO, unloadCurrent);
                else
                    StartCO_LoadAdditiveScene(sceneSO, unloadCurrent);
            } else {
                if (mode == LoadSceneMode.Additive)
                    LoadBaseScene(sceneSO);
                else
                    LoadAdditiveScene(sceneSO);
            }
        }

        //Track Scene Loading Coroutines
        private void StartCO_TrackSceneLoading() {
            coroutine = StartCoroutine(CO_TrackSceneLoading());
        }
        private IEnumerator CO_TrackSceneLoading() {
            asyncOperation.allowSceneActivation = false;

            while (!asyncOperation.isDone) {
                OnLoadingScene?.Invoke(asyncOperation.progress);

                if (asyncOperation.progress >= 0.9f) {
                    asyncOperation.allowSceneActivation = true;
                }
                yield return null;
            }
            asyncOperation = null;
        }
        private void StartCO_TrackSceneUnloading() {
            coroutine = StartCoroutine(CO_TrackSceneUnloading());
        }
        private IEnumerator CO_TrackSceneUnloading() {
            asyncOperation.allowSceneActivation = false;

            while (!asyncOperation.isDone) {
                OnUnloadingScene?.Invoke(asyncOperation.progress);

                if (asyncOperation.progress >= 0.9f) {
                    asyncOperation.allowSceneActivation = true;
                }
                yield return null;
            }
            asyncOperation = null;
        }

        //Coroutine LoadBaseScene
        private void StartCO_LoadBaseScene(SceneAssetSO sceneSO, bool unloadCurrent) {
            StartCoroutine(CO_LoadBaseScene(sceneSO, unloadCurrent));
        }
        private IEnumerator CO_LoadBaseScene(SceneAssetSO sceneSO, bool unloadCurrent) {
            if (unloadCurrent) {
                OnUnloadSceneBegin?.Invoke(currentBaseScene);
                string currentScene = GetSceneName(currentBaseScene);
                asyncOperation = SceneManager.UnloadSceneAsync(currentScene);
                previousBaseScene = currentBaseScene;
                StartCO_TrackSceneUnloading();
                yield return coroutine;
            }

            currentBaseScene = sceneSO.Type;
            OnLoadSceneBegin?.Invoke(currentBaseScene);
            asyncOperation = SceneManager.LoadSceneAsync(sceneSO.SceneName, LoadSceneMode.Single);
            StartCO_TrackSceneLoading();
            yield return coroutine;
        }

        //Coroutine LoadAdditiveScene
        private void StartCO_LoadAdditiveScene(SceneAssetSO sceneSO, bool unloadCurrent) {
            StartCoroutine(CO_LoadAdditiveScene(sceneSO, unloadCurrent));
        }
        private IEnumerator CO_LoadAdditiveScene(SceneAssetSO sceneSO, bool unloadCurrent) {
            if (unloadCurrent) {
                OnUnloadSceneBegin?.Invoke(currentAdditiveScene);
                string currentScene = GetSceneName(currentAdditiveScene);
                asyncOperation = SceneManager.UnloadSceneAsync(currentScene);
                previousAdditiveScene = currentAdditiveScene;
                StartCO_TrackSceneUnloading();
                yield return coroutine;
            }

            currentAdditiveScene = sceneSO.Type;
            OnLoadSceneBegin?.Invoke(currentAdditiveScene);
            asyncOperation = SceneManager.LoadSceneAsync(sceneSO.SceneName, LoadSceneMode.Additive);
            StartCO_TrackSceneLoading();
            yield return coroutine;
        }

        //Sync Operations
        private void LoadBaseScene(SceneAssetSO sceneSO) {
            if (sceneSO == null)
                return;

            OnLoadSceneBegin?.Invoke(sceneSO.Type);
            SetupNewScene(sceneSO.Type);
            SceneManager.LoadScene(sceneSO.SceneName, LoadSceneMode.Single);
        }
        private void LoadAdditiveScene(SceneAssetSO sceneSO) {
            if (sceneSO == null)
                return;

            OnLoadSceneBegin?.Invoke(sceneSO.Type);
            SetupNewScene(sceneSO.Type);
            SceneManager.LoadScene(sceneSO.SceneName, LoadSceneMode.Additive);
        }

        //Utilities
        private string GetSceneName(SceneTypes type) {
            SceneAssetSO sceneSO = scenes.First(scene => scene.Type == type);

            if (sceneSO == null)
                return "";

            return sceneSO.SceneName;
        }
        private SceneTypes GetSceneType(string name) {
            SceneAssetSO sceneSO = scenes.First(scene => scene.SceneName == name);

            if (sceneSO != null)
                return sceneSO.Type;

            return SceneTypes.MRTK;
        }
        private void SetupNewScene(SceneTypes scene) {
            previousAdditiveScene = currentAdditiveScene;
            currentAdditiveScene = scene;
        }
    }
}
