namespace MessageSystem {

    public static partial class MessageManager {

        public readonly static MessageChannel<ServerMessageTypes> Server = new MessageChannel<ServerMessageTypes>();
    }

    public enum ServerMessageTypes {
        None,
        OnConnected,
        OnDisconnected,
    }
}
