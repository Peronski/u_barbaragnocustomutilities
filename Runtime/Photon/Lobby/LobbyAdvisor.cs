using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Lobby {
    public class LobbyAdvisor : MonoBehaviour {
        public static void Advise(string message) {
            Debug.Log(message);
        }
    }
}
