using MessageSystem;
using Photon.Realtime;
using System.Collections.Generic;

namespace UpSurgeOn.MetaverseRuntime.Lobby {
    public class LobbyMessage : MessageClass {
        public List<RoomInfo> Rooms { get; private set; }
        public LobbyMessage(List<RoomInfo> roomInfo) {
            Rooms = roomInfo;
        }
    }
}
