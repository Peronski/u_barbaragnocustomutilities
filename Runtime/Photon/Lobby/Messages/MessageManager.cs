namespace MessageSystem {

    public static partial class MessageManager {

        public readonly static MessageChannel<LobbyMessageTypes> Lobby = new MessageChannel<LobbyMessageTypes>();
    }

    public enum LobbyMessageTypes {
        None,
        OnConnecting,
        OnConnectedToMaster,
        OnDisconnectedToMaster,
        OnJoinedLobby,
        OnLeftLobby,
        OnRoomListUpdate,
        OnLobbyStatisticsUpdate
    }
}
