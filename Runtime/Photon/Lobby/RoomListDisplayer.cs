using Photon.Realtime;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UpSurgeOn.MetaverseRuntime.Room;

namespace UpSurgeOn.MetaverseRuntime.Lobby {
    public class RoomListDisplayer : MonoBehaviour {
        [Header("Settings")]
        [SerializeField] private bool asyncOperation;

        [Header("References")]
        [SerializeField] private LobbyController lobbyController;
        [SerializeField] private Transform roomListCanvas;

        [Header("Prefabs")]
        [SerializeField] private RoomIdentity roomPrefab;

        private Dictionary<string, RoomIdentity> roomList = new Dictionary<string, RoomIdentity>();

        private void Awake() {
            if (lobbyController == null)
                return;

            if (!asyncOperation)
                lobbyController.OnRoomListUpdateEvent += UpdateUI;
            else
                lobbyController.OnRoomListUpdateEvent += AsyncUpdateUI;
        }

        private void OnDestroy() {
            if (lobbyController == null)
                return;

            if (!asyncOperation)
                lobbyController.OnRoomListUpdateEvent -= UpdateUI;
            else
                lobbyController.OnRoomListUpdateEvent -= AsyncUpdateUI; 
        }

        private void UpdateUI(List<RoomInfo> infos) {
            foreach (RoomInfo info in infos) {

                if (roomList.TryGetValue(info.Name, out RoomIdentity room)) {
                    if (info.RemovedFromList)
                        DisableAndRemoveRoom(room);
                    else
                        UpdateRoom(info);
                    return;
                }

                if (!info.RemovedFromList)
                    AddRoom(info);
            }
        }

        private async void AsyncUpdateUI(List<RoomInfo> rooms) {
            await Task.Run(() => {
                UpdateUI(rooms);
            });
        }

        private void DisableAndRemoveRoom(RoomIdentity room) {
            room.gameObject.SetActive(false);
            roomList.Remove(room.Info.Name);
        }

        private void UpdateRoom(RoomInfo info) {
            roomList[info.Name].ConfigureInfo(info);
        }

        private void AddRoom(RoomInfo info) {
            RoomIdentity newRoom = Instantiate(roomPrefab, roomListCanvas);
            newRoom.ConfigureInfo(info);
            roomList.Add(info.Name, newRoom);
        }
    }
}
