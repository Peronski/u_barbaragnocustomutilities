using MessageSystem;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UpSurgeOn.MetaverseRuntime.Room;
using UpSurgeOn.MetaverseRuntime.Server;
using UpSurgeOn.MetaverseRuntimeRuntime.Singletons;

namespace UpSurgeOn.MetaverseRuntime.Lobby {
    public class LobbyController : MonoBehaviour {
        [Header("Settings")]
        [SerializeField] private bool automaticConnection;

        [Header("References")]
        [SerializeField] private RoomConfigurator roomConfigurator;

        [Header("Data")]
        [SerializeField] private ServerSettings serverSettings;

        public event Action<List<RoomInfo>> OnRoomListUpdateEvent;

        private void Awake() {
            MessageManager.Room.AttachListener(RoomMessageTypes.OnJoinedRoom, OnJoinedRoom);
            MessageManager.Lobby.AttachListener(LobbyMessageTypes.OnRoomListUpdate, OnRoomListUpdate);
        }
        private void OnDestroy() {
            MessageManager.Room.DetachListener(RoomMessageTypes.OnJoinedRoom, OnJoinedRoom);
            MessageManager.Lobby.DetachListener(LobbyMessageTypes.OnRoomListUpdate, OnRoomListUpdate);
        }
        private void Start() {
            if (!automaticConnection)
                return;

            ServerConnector.TryConnectionToServer(serverSettings.AppSettings);
        }
        private void OnJoinedRoom(RoomMessageTypes type, MessageClass message) {
            if (type != RoomMessageTypes.OnJoinedRoom)
                return;

            if (!PhotonNetwork.IsMasterClient)
                PhotonNetwork.IsMessageQueueRunning = false;

            LoadRoomScene();
        }
        private void OnRoomListUpdate(LobbyMessageTypes type, MessageClass message) {
            if (!MessageClass.TryCastTo(message, out LobbyMessage content)) {
                LobbyAdvisor.Advise("[LobbyController] - OnRoomListUpdate. Error: invalid cast.");
                return;
            }

            OnRoomListUpdateEvent?.Invoke(content.Rooms);

            if (automaticConnection) {
                if (content.Rooms.Count <= 0) {
                    LobbyAdvisor.Advise("[LobbyController] - No valid rooms have been found to connect. Trying to create a new one.");

                    RoomConnector.TryCreateRoom(roomConfigurator);
                    return;
                }

                RoomInfo room = content.Rooms[0];
                if (room != null)
                    RoomConnector.TryConnectionToRoom(room);
            }
        }

        private void LoadRoomScene() {
            SceneLoader.Instance.LoadScene(SceneTypes.Room, true, true, LoadSceneMode.Additive);
        }
    }
}
