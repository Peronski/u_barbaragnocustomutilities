using ExitGames.Client.Photon;
using MessageSystem;
using Photon.Pun;
using Photon.Realtime;
using UpSurgeOnRuntime.Singletons;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public class RoomManager : NetworkSingleton<RoomManager> {
        public override void OnCreatedRoom() {
            base.OnCreatedRoom();

            RoomAdvisor.Advise($"[Room] - On Created Room.");
            MessageManager.Room.SendMessage(RoomMessageTypes.OnCreatedRoom);
        }

        public override void OnCreateRoomFailed(short returnCode, string message) {
            base.OnCreateRoomFailed(returnCode, message);

            RoomAdvisor.Advise($"[Room] - On Create Room Failed. Error message: {message}. Error code: {returnCode}. ");
            MessageManager.Room.SendMessage(RoomMessageTypes.OnCreateRoomFailed);
        }

        public override void OnJoinedRoom() {
            base.OnJoinedRoom();

            RoomAdvisor.Advise($"[Room] - On Joined Room.");
            MessageManager.Room.SendMessage(RoomMessageTypes.OnJoinedRoom);
        }

        public override void OnJoinRoomFailed(short returnCode, string message) {
            base.OnJoinRoomFailed(returnCode, message);

            RoomAdvisor.Advise($"[Room] - On Joined Room Failed. Error message: {message}. Error code: {returnCode}. ");
            MessageManager.Room.SendMessage(RoomMessageTypes.OnJoinRoomFailed);
        }

        public override void OnJoinRandomFailed(short returnCode, string message) {
            base.OnJoinRandomFailed(returnCode, message);

            RoomAdvisor.Advise($"[Room] - On Join Random Failed.");
            MessageManager.Room.SendMessage(RoomMessageTypes.OnJoinRandomFailed);
        }

        public override void OnLeftRoom() {
            base.OnLeftRoom();

            RoomAdvisor.Advise($"[Room] - On Left Room.");
            MessageManager.Room.SendMessage(RoomMessageTypes.OnLeftRoom);
        }

        public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged) {
            base.OnRoomPropertiesUpdate(propertiesThatChanged);

            RoomAdvisor.Advise($"[Room] - On Room Propertes Update.");
            MessageManager.Room.SendMessage(RoomMessageTypes.OnRoomPropertiesUpdate);
        }

        public override void OnMasterClientSwitched(Player newMasterClient) {
            base.OnMasterClientSwitched(newMasterClient);

            RoomAdvisor.Advise($"[Room] - On MasterClient Changed.");
            MessageManager.Room.SendMessage(RoomMessageTypes.OnMasterClientSwitched, new ValueMessage<Player>(newMasterClient));
        }

        public override void OnPlayerEnteredRoom(Player newPlayer) {
            base.OnPlayerEnteredRoom(newPlayer);

            RoomAdvisor.Advise($"[Room] - On Player Entered Room.");
            MessageManager.Room.SendMessage(RoomMessageTypes.OnPlayerEnteredRoom, new ValueMessage<Player>(newPlayer));
        }

        public override void OnPlayerLeftRoom(Player newPlayer) {
            base.OnPlayerEnteredRoom(newPlayer);

            RoomAdvisor.Advise($"[Room] - On Player Left Room.");
            MessageManager.Room.SendMessage(RoomMessageTypes.OnPlayerLeftRoom, new ValueMessage<Player>(newPlayer));
        }

        public override void OnDisconnected(DisconnectCause cause) {
            base.OnDisconnected(cause);

            RoomAdvisor.Advise($"[Room] - Disconnect from Photon, cause: {cause}.");
            MessageManager.Room.SendMessage(RoomMessageTypes.OnDisconnect, new ValueMessage<DisconnectCause>(cause));
        }
    }
}