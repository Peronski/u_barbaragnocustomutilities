using Photon.Pun;
using Photon.Realtime;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public static class RoomConnector {
        public static bool TryConnectionToRoom(RoomInfo room) {
            if (!PhotonNetwork.InLobby) {
                RoomAdvisor.Advise("[Lobby] - Error: not in lobby to join room.");
                return false;
            }

            if (!room.IsOpen) {
                RoomAdvisor.Advise("[Lobby] - Error: room is not open.");
                return false;
            }

            if (string.IsNullOrEmpty(room.Name)) {
                RoomAdvisor.Advise("[Lobby] - Error: invalid room's name.");
                return false;
            }

            return room.ExpectedUsers == null ? JoinRoom(room.Name) : JoinRoomWithExpectedUsers(room.Name, room.ExpectedUsers);
        }

        public static bool TryCreateRoom(RoomConfigurator configurator) {
            if (!PhotonNetwork.InLobby) {
                RoomAdvisor.Advise("[Lobby] - Error: not in lobby to create room.");
                return false;
            }

            if (PhotonNetwork.InRoom) {
                RoomAdvisor.Advise("[Lobby] - Error: already in a room.");
                return false;
            }

            if (!configurator) {
                RoomAdvisor.Advise("[Lobby] - Error: configurator is null.");
                return false;
            }

            RoomConfiguration configuration = configurator.GetCurrentConfiguration();
            return CreateRoom(configuration.RoomName, configuration.Options);
        }

        private static void JoinRandomRoom() => PhotonNetwork.JoinRandomRoom();
        private static bool JoinRoom(string roomName) => PhotonNetwork.JoinRoom(roomName);
        private static bool JoinRoomWithExpectedUsers(string roomName, string[] expectedUsers) => PhotonNetwork.JoinRoom(roomName, expectedUsers);
        private static bool CreateRoom(string roomName, RoomOptions options = null, TypedLobby type = null, string[] expectedUsers = null) => PhotonNetwork.CreateRoom(roomName, options, type, expectedUsers);
    }
}
