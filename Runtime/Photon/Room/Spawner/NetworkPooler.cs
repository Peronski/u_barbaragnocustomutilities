using MessageSystem;
using Photon.Pun;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UpSurgeOn.MetaverseRuntime.Utilities;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public class NetworkPooler : MonoBehaviourPunCallbacks {
        private Dictionary<Prefab, string> spawnablesDictionary = new Dictionary<Prefab, string>();
        private List<PrefabIdentifier> spawnables = new List<PrefabIdentifier>();

        private void Awake() {
            MessageManager.Pooler.AttachListener(SpawnMessageTypes.NetworkSpawn, Instantiate);

            spawnablesDictionary.Add(Prefab.Rig, PhotonConsts.PHOTON_USER_RIG_PREFAB);
            spawnablesDictionary.Add(Prefab.Head, PhotonConsts.PHOTON_HEAD_PREFAB);
            spawnablesDictionary.Add(Prefab.BrainDecomposable, PhotonConsts.PHOTON_BRAIN_DECOMPOSABLE_PREFAB);
            spawnablesDictionary.Add(Prefab.SkullDecomposable, PhotonConsts.PHOTON_SKULL_DECOMPOSABLE_PREFAB);
        }
        private void OnDestroy() {
            MessageManager.Pooler.DetachListener(SpawnMessageTypes.NetworkSpawn, Instantiate);
        }

        private string GetPrefabPhotonName(Prefab type) => spawnablesDictionary[type];

        private bool VerifyMessage(SpawnMessageTypes type, MessageClass message, out PoolMessage content) {
            content = null;

            if (type != SpawnMessageTypes.NetworkSpawn)
                return false;

            if (!MessageClass.TryCastTo(message, out content))
                return false;

            return true;
        }

        public void Instantiate(SpawnMessageTypes type, MessageClass message) {
            if (!VerifyMessage(type, message, out PoolMessage content))
                return;

            if (PhotonNetwork.IsMasterClient) {
                GameObject spawnedObject = PhotonNetwork.InstantiateRoomObject(GetCompletePath(content.TypeToSpawn), content.Position, content.Rotation);

                if (gameObject.TryGetComponent(out PrefabIdentifier spawnable)) {
                    spawnables.Add(spawnable);
                    //spawnable.OnSpawn();
                }

                Debug.Log($"[Network Pooler] - Network spawn.");
            }
        }



        private string GetCompletePath(Prefab type) {
            return Path.Combine(PhotonConsts.PHOTON_RESOURCES_FOLDER, GetPrefabPhotonName(type));
        }
    }
}
