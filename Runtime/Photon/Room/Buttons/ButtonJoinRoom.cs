using UIPro;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public class ButtonJoinRoom : MonoBehaviour {
        [Header("References")]
        [SerializeField] private ButtonListener listener;
        [SerializeField] private RoomIdentity identity;

        private void Awake() {
            listener.OnClick += RequestConnectionToRoom;
        }

        private void OnDestroy() {
            listener.OnClick -= RequestConnectionToRoom;
        }

        private void RequestConnectionToRoom(ButtonListener listener) {
            RoomConnector.TryConnectionToRoom(identity.Info);
        }
    }
}
