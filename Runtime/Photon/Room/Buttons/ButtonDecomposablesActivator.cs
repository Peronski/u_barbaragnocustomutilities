using UIPro;
using UnityEngine;

public class ButtonDecomposablesActivator : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private bool areDecomposablesActive = true;

    [Header("References")]
    [SerializeField] private ButtonListener buttonListener;
    [Space]
    [SerializeField] private DecomposablesController controller;

    private void Awake() {
        if (buttonListener != null)
            buttonListener.OnClick += OnClick;
    }

    private void OnClick(ButtonListener listener) {
        controller.EnableMultiPart(areDecomposablesActive);
        areDecomposablesActive = !areDecomposablesActive;
    }

    public void MRTK_OnClick() {
        controller.EnableMultiPart(areDecomposablesActive);
        areDecomposablesActive = !areDecomposablesActive;
    }
}
