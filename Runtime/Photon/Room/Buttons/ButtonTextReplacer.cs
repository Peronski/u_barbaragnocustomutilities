using MessageSystem;
using TMPro;
using UIPro;
using UnityEngine;

public class ButtonTextReplacer : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private string textDecomposition = "Decompose"; 
    [SerializeField] private string textComposition = "Compose";

    [Header("References")]
    [SerializeField] private ButtonListener buttonListener;
    [Space]
    [SerializeField] private TextMeshProUGUI textComponent;

    private void Awake() {
        if (buttonListener != null)
            buttonListener.OnClick += OnClick;

        MessageManager.Operation.AttachListener(OperationMessageTypes.DecompositionEnd, Replace);
        MessageManager.Operation.AttachListener(OperationMessageTypes.CompositionEnd, Replace);
    }
    private void OnDestroy() {
        if (buttonListener != null)
            buttonListener.OnClick -= OnClick;

        MessageManager.Operation.DetachListener(OperationMessageTypes.DecompositionEnd, Replace);
        MessageManager.Operation.DetachListener(OperationMessageTypes.CompositionEnd, Replace);
    }

    private void Replace(OperationMessageTypes types, MessageClass message) {
        if(types == OperationMessageTypes.CompositionEnd) {
            textComponent.text = textDecomposition;
            return;
        }

        if(types == OperationMessageTypes.DecompositionEnd) {
            textComponent.text = textComposition;
            return;
        }
    }

    private void OnClick(ButtonListener listener) {
        string temp = textComponent.text;
        textComponent.text = textDecomposition;
        textDecomposition = temp;
    }

    public void MRTK_OnClick() {
        string temp = textComponent.text;
        textComponent.text = textDecomposition;
        textDecomposition = temp;
    }

    public void MRTK_OnClick(bool status) {
        textComponent.text = status ? textDecomposition : textComposition;
    }
}
