using UIPro;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public class ButtonCreateRoom : MonoBehaviour {
        [Header("References")]
        [SerializeField] private ButtonListener listener;
        [SerializeField] private RoomConfigurator configurator;

        private void Awake() {
            listener.OnClick += RequestRoomCreation;
        }

        private void OnDestroy() {
            listener.OnClick -= RequestRoomCreation;
        }

        private void RequestRoomCreation(ButtonListener listener) {
            RoomConnector.TryCreateRoom(configurator);
        }
    }
}
