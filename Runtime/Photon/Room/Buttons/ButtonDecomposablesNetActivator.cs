using MessageSystem;
using Photon.Pun;
using UIPro;
using UnityEngine;

public class ButtonDecomposablesNetActivator : MonoBehaviour {
    [Header("Settings")]
    [SerializeField] private bool onServerControl = true;

    [Header("References")]
    [SerializeField] private ButtonListener buttonListener;
    [Space]
    [SerializeField] private ButtonTextReplacer textReplacer;
    [Space]
    [SerializeField] private DecomposablesNetController decomposablesNetController;

    private void Awake() {
        if (buttonListener != null)
            buttonListener.OnClick += OnClick;
        if (decomposablesNetController != null)
            decomposablesNetController.OnDecomposedStatusChanged += textReplacer.MRTK_OnClick;
    }
    private void OnDestroy() {
        if (buttonListener != null)
            buttonListener.OnClick -= OnClick;
        if (decomposablesNetController != null)
            decomposablesNetController.OnDecomposedStatusChanged -= textReplacer.MRTK_OnClick;
    }

    private void OnClick(ButtonListener listener) {
        if (onServerControl) {
            if (!PhotonNetwork.IsMasterClient)
                return;
        }

        decomposablesNetController.MultiPartNetworkCall();
    }

    public void MRTK_OnClick() {
        if (onServerControl) {
            if (!PhotonNetwork.IsMasterClient)
                return;
        }

        decomposablesNetController.MultiPartNetworkCall();
    }
}
