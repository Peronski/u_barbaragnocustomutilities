using MessageSystem;
using UIPro;
using UnityEngine;
using UpSurgeOn.MetaverseRuntime.Utilities;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public class ButtonNetworkSpawner : ButtonSpawner {
        protected override void OnClick(ButtonListener buttonListener) {
            MessageManager.Pooler.SendMessage(SpawnMessageTypes.NetworkSpawn, new PoolMessage(prefabType, spawn.position, spawn.rotation));
            Debug.Log($"[ButtonSpawner] - Request network spawn.");
        }

        public override void MRTK_OnClick() {
            MessageManager.Pooler.SendMessage(SpawnMessageTypes.NetworkSpawn, new PoolMessage(prefabType, spawn.position, spawn.rotation));
            Debug.Log($"[ButtonSpawner] - Request network spawn.");
        }
    }
}
