using Photon.Realtime;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public class RoomConfigurator : MonoBehaviour {
        [Header("Settings")]
        [SerializeField] private string roomName;
        [Space]
        [SerializeField] private byte maxPlayerNumber;
        [Space]
        [SerializeField] private bool isVisible;
        [SerializeField] private bool isOpen;
        [SerializeField] private bool publishUserId;

        private RoomConfiguration CreateConfiguration() {
            RoomOptions basicOptions = new RoomOptions();
            basicOptions.MaxPlayers = maxPlayerNumber;
            basicOptions.IsVisible = isVisible;
            basicOptions.IsOpen = isOpen;
            basicOptions.PublishUserId = publishUserId;

            RoomConfiguration roomOptions = new RoomConfiguration();
            roomOptions.RoomName = roomName;
            roomOptions.Options = basicOptions;
            return roomOptions;
        }

        public RoomConfiguration GetCurrentConfiguration() => CreateConfiguration();
    }

    public class RoomConfiguration {
        public string RoomName { get; set; }
        public RoomOptions Options { get; set; }
    }
}
