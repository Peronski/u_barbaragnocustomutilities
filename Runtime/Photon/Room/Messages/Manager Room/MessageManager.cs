namespace MessageSystem {

    public static partial class MessageManager {

        public readonly static MessageChannel<RoomMessageTypes> Room = new MessageChannel<RoomMessageTypes>();
    }

    public enum RoomMessageTypes {
        None,
        OnCreatedRoom,
        OnCreateRoomFailed,
        OnJoinedRoom,
        OnJoinRoomFailed,
        OnJoinRandomFailed,
        OnLeftRoom,
        OnRoomPropertiesUpdate,
        OnMasterClientSwitched,
        OnPlayerEnteredRoom,
        OnPlayerLeftRoom,
        OnDisconnect,
        OnResetRoom
    }
}
