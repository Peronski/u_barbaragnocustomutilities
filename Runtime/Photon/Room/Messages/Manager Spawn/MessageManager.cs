using UnityEngine;

namespace MessageSystem {
    public static partial class MessageManager {
        public readonly static MessageChannel<SpawnMessageTypes> Pooler = new MessageChannel<SpawnMessageTypes>();
    }

    public enum SpawnMessageTypes {
        None,
        LocalSpawn,
        NetworkSpawn
    }

    public class PoolMessage : MessageClass {
        public Prefab TypeToSpawn { get; private set; }
        public Vector3 Position { get; private set; }
        public Quaternion Rotation { get; private set; }
        public PoolMessage(Prefab prefabType, Vector3 position, Quaternion rotation) {
            TypeToSpawn = prefabType;
            Position = position;
            Rotation = rotation;
        }
    }
}
