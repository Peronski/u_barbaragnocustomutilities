using System.IO;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using PhotonRoom = Photon.Realtime.Room;
using UnityEngine;
using MessageSystem;
using UpSurgeOn.MetaverseRuntimeRuntime.Singletons;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public class RoomController : MonoBehaviour {
        [Header("Settings - Optionals")]
        [SerializeField] private int serializationRate;
        [SerializeField][Range(0,60)] private int sendRate = 30;

        [Header("References")]
        [SerializeField] private List<DecomposablesNetController> decomposablesNetControllers = new List<DecomposablesNetController>();

        private PhotonRoom currentRoom;
        private void Awake() {
            MessageManager.Room.AttachListener(RoomMessageTypes.OnJoinedRoom, OnJoinedRoom);
            MessageManager.Room.AttachListener(RoomMessageTypes.OnPlayerEnteredRoom, OnPlayerEnteredRoom);
            MessageManager.Room.AttachListener(RoomMessageTypes.OnPlayerLeftRoom, OnPlayerLeftRoom);
            MessageManager.Room.AttachListener(RoomMessageTypes.OnDisconnect, OnDisconnect);
        }
        private void OnDestroy() {
            MessageManager.Room.DetachListener(RoomMessageTypes.OnJoinedRoom, OnJoinedRoom);
            MessageManager.Room.DetachListener(RoomMessageTypes.OnPlayerEnteredRoom, OnPlayerEnteredRoom);
            MessageManager.Room.DetachListener(RoomMessageTypes.OnPlayerLeftRoom, OnPlayerLeftRoom);
            MessageManager.Room.DetachListener(RoomMessageTypes.OnDisconnect, OnDisconnect);
        }
        private void Start() {
            GameObject playerObject = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "User - Rig"), Vector3.zero, Quaternion.identity);

            if (!PhotonNetwork.IsMasterClient) {
                PhotonNetwork.IsMessageQueueRunning = true;
            }
        }

        private void OnJoinedRoom(RoomMessageTypes type, MessageClass message) {
            if (type != RoomMessageTypes.OnJoinedRoom)
                return;

            currentRoom = PhotonNetwork.CurrentRoom;
        }
        private void OnPlayerEnteredRoom(RoomMessageTypes type, MessageClass message) {
            if (!PhotonNetwork.IsMasterClient)
                return;

            if (!MessageClass.TryCastTo(message, out ValueMessage<Player> content))
                return;

            NetContainer.Update(content.Value);
        }
        private void OnPlayerLeftRoom(RoomMessageTypes type, MessageClass message) {
            if (!MessageClass.TryCastTo(message, out ValueMessage<Player> content))
                return;

            if (content.Value.IsMasterClient) {
                if(PhotonNetwork.PlayerList.Length > 0) {
                    PhotonNetwork.SetMasterClient(PhotonNetwork.PlayerList[0]);
                }
            }
        }
        private void OnDisconnect(RoomMessageTypes type, MessageClass message) {
            if (!MessageClass.TryCastTo(message, out ValueMessage<DisconnectCause> content))
                return;

            if (content.Value == DisconnectCause.ClientTimeout || 
                content.Value == DisconnectCause.DisconnectByServerReasonUnknown || 
                content.Value == DisconnectCause.ServerTimeout ||
                content.Value == DisconnectCause.DisconnectByClientLogic ||
                content.Value == DisconnectCause.DisconnectByOperationLimit ||
                content.Value == DisconnectCause.DisconnectByServerLogic) {
                SceneLoader.Instance.LoadScene(SceneTypes.Lobby, true, true, UnityEngine.SceneManagement.LoadSceneMode.Additive);
            }
        }
    }
}
