using Photon.Realtime;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public class RoomIdentity : MonoBehaviour {
        public string Name { get; private set; }
        public RoomInfo Info { get; private set; }
        public void ConfigureInfo(RoomInfo info) => Info = info;
    }
}