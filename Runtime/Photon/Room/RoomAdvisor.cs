using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public class RoomAdvisor : MonoBehaviour {
        public static void Advise(string message) => Debug.Log(message);
    }
}
