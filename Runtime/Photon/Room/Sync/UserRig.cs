using Photon.Pun;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public class UserRig : MonoBehaviour {
        [Header("References")]
        [SerializeField] private HeadNetSynchronizer head;
        [Space]
        [SerializeField] private HandNetSynchronizer left;
        [SerializeField] private HandNetSynchronizer right;
        [SerializeField] private PhotonView photonView;

        private void Start() {
            NetworkObjectInitialization();
        }

        private void NetworkObjectInitialization() {
            if (photonView.IsMine) {
                head.LocalInitialization();
                left.LocalInitialization();
                right.LocalInitialization();
            }

            if (!photonView.IsMine) {
                head.NetworkInitialization();
                left.NetworkInitialization();
                right.NetworkInitialization();
            }
        }
    }
}
