using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UpSurgeOn.MetaverseRuntime.Interactions;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public class HandNetSynchronizer : GenericNetSynchronizer {
        [Header("Settings")]
        [SerializeField] private Handedness hand;
        [Header("References")]
        [SerializeField] private GameObject handObject;

        public void LocalInitialization() {
            XRBaseController controller = InputSourceProvider.Instance.GetControllerByHand(hand);
            if (controller != null) { 
                syncObject = controller.transform;
            }
        }

        public void NetworkInitialization() {
            handObject.SetActive(true);
        }
    }
}
