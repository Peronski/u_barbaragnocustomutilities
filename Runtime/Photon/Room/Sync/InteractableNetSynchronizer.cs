using Microsoft.MixedReality.Toolkit.SpatialManipulation;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Room {
    [RequireComponent(typeof(PhotonView), typeof(ObjectManipulator), typeof(OwnershipTransfer))]
    public class InteractableNetSynchronizer : GenericNetSynchronizer {
        [Header("Settings")]
        [SerializeField] private bool startingStatus;

        [Header("References")]
        [SerializeField] private OwnershipTransfer ownershipTransfer;
        [SerializeField] private ObjectManipulator objectManipulator;

        private bool isInteracted;
        public bool HookInteracted {
            get => isInteracted;
            private set {
                isInteracted = value;
                this.photonView.RPC(nameof(RPC_IsInteracted), RpcTarget.Others, isInteracted);
            }
        }

        private void OnValidate() {
            if (ownershipTransfer == null)
                TryGetComponent(out ownershipTransfer);
            if (objectManipulator == null)
                TryGetComponent(out objectManipulator);
        }
        private void Awake() {
            objectManipulator.firstSelectEntered.AddListener((selected) => RequestInteractionCall());
            objectManipulator.lastSelectExited.AddListener((selected) => ReleaseInteractionCall());

            NetContainer.Subscribe(this);
        }
        private void OnDestroy() {
            objectManipulator.firstSelectEntered.RemoveListener((selected) => RequestInteractionCall());
            objectManipulator.lastSelectExited.RemoveListener((selected) => ReleaseInteractionCall());
        }
        protected override void Start() {
            base.Start();
            syncObject = transform;
        }

        // LOCAL
        public void RequestInteractionCall() {
            Debug.Log($"[INetSync][Local] - Request interaction on {gameObject.name}.");

            if (PhotonNetwork.IsMasterClient) {
                RPC_InteractionRequest(PhotonNetwork.LocalPlayer);
                return;
            }

            this.photonView.RPC(nameof(RPC_InteractionRequest), RpcTarget.MasterClient, PhotonNetwork.LocalPlayer);
        }

        // LOCAL
        public void ReleaseInteractionCall() {
            Debug.Log($"[INetSync][Local] - Request release on {gameObject.name}.");
            
            this.photonView.RPC(nameof(RPC_ReleaseRequest), RpcTarget.MasterClient);
        }

        // LOCAL
        public void UpdateInteractedTargetPlayerCall(Player targetPlayer) {
            if (!PhotonNetwork.IsMasterClient)
                return;

            this.photonView.RPC(nameof(RPC_IsInteracted), targetPlayer, HookInteracted);
        }

        // MASTER
        [PunRPC]
        private void RPC_InteractionRequest(Player player) {
            if (!PhotonNetwork.IsMasterClient)
                return;

            Debug.Log($"[INetSync][Master] - Player {player.UserId} request interaction on {gameObject.name}.");

            if (isInteracted) {
                Debug.Log($"[INetSync][Master] - {gameObject.name} already interacted.");
                return;
            }

            Debug.Log($"[INetSync][Master] - Concede to Player {player.UserId} interaction.");

            this.photonView.TransferOwnership(player);
            HookInteracted = true;
            // master can interact without restriction 
        }

        // MASTER
        [PunRPC]
        private void RPC_ReleaseRequest() {
            if (!PhotonNetwork.IsMasterClient)
                return;

            Debug.Log($"[INetSync][Master] - Request release on {gameObject.name}.");

            HookInteracted = false;
            objectManipulator.enabled = true;
        }

        // CLIENT
        [PunRPC]
        private void RPC_IsInteracted(bool status) {
            if (PhotonNetwork.IsMasterClient) {
                Debug.Log($"[INetSync][Master] - {gameObject.name} is interacted.");
                HookInteracted = status;
                return;
            }

            Debug.Log($"[INetSync][Client] - {gameObject.name} is interacted.");
            Debug.Log($"[INetSync][Client] - {gameObject.name} is mine: {photonView.IsMine}.");

            isInteracted = status;

            if (!photonView.IsMine)
                objectManipulator.enabled = !status;
        }
    }
}
