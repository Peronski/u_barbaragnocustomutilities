using Photon.Pun;
using Photon.Realtime;
using System;
using UnityEngine;

public class OwnershipTransfer : MonoBehaviourPun, IPunOwnershipCallbacks {
    public event Action<PhotonView, Player> OnOwnershipRequestEvent;
    public event Action<PhotonView, Player> OnOwnershipTransferedEvent;
    public event Action<PhotonView, Player> OnOwnershipTransferFailedEvent;

    private void Awake() {
        PhotonNetwork.AddCallbackTarget(this);
    }
    private void OnDestroy() {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public void RequestOwnership() {
        Debug.Log("[Ownership] - Requesting Ownership.");

        if (this.photonView.IsMine) {
            OnOwnershipTransferedEvent?.Invoke(this.photonView, PhotonNetwork.LocalPlayer);
            return;
        }

        this.photonView.RequestOwnership();
    }

    public void OnOwnershipRequest(PhotonView targetView, Player requestingPlayer) {
        Debug.Log("[Ownership] - Request Transfer.");

        if (this.photonView.OwnershipTransfer == OwnershipOption.Fixed || this.photonView.OwnershipTransfer == OwnershipOption.Takeover) {
            Debug.Log("Ownership Transfer isn't set to 'Request'. Request isn't valid.");
            return;
        }

        if (targetView != base.photonView)
            return;

        OnOwnershipRequestEvent?.Invoke(targetView, requestingPlayer);
        this.photonView.TransferOwnership(requestingPlayer);
    }

    public void OnOwnershipTransfered(PhotonView targetView, Player previousOwner) {
        if (targetView != base.photonView)
            return;

        Debug.Log("[Ownership] - Ownership Transfered.");
        OnOwnershipTransferedEvent?.Invoke(targetView, previousOwner);
    }

    public void OnOwnershipTransferFailed(PhotonView targetView, Player senderOfFailedRequest) {
        if (targetView != base.photonView)
            return;

        Debug.Log("[Ownership] - Ownership Transfer Failed.");
        OnOwnershipTransferFailedEvent?.Invoke(targetView, senderOfFailedRequest);
    }
}
