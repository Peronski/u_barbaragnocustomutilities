using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public class HeadNetSynchronizer : GenericNetSynchronizer {
        [Header("References")]
        [SerializeField] private GameObject headObject;

        public void LocalInitialization() {
            syncObject = Camera.main.transform;
        }

        public void NetworkInitialization() {
            headObject.SetActive(true);
        }
    }
}
