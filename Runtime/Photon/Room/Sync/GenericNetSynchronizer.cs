using Photon.Pun;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Room {
    public class GenericNetSynchronizer : MonoBehaviourPun, IPunObservable {
        [Header("Base Settings")]
        [SerializeField] protected float lerpMoveSpeed = 1.5f;
        [SerializeField] protected float lerpRotationSpeed = 1.5f;
        [SerializeField] protected float lerpScaleSpeed = 1.5f;

        protected Transform syncObject;

        protected Vector3 networkLocalPosition;
        protected Quaternion networkLocalRotation;
        protected Vector3 networkLocalScale;

        protected Vector3 startingLocalPosition;
        protected Quaternion startingLocalRotation;
        protected Vector3 startingLocalScale;

        void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
            if (stream.IsWriting) {
                stream.SendNext(transform.localPosition);
                stream.SendNext(transform.localRotation);
                stream.SendNext(transform.localScale);
            } else {
                networkLocalPosition = (Vector3)stream.ReceiveNext();
                networkLocalRotation = (Quaternion)stream.ReceiveNext();
                networkLocalScale = (Vector3)stream.ReceiveNext();
            }
        }

        protected virtual void Start() {
            var trans = transform;
            startingLocalPosition = trans.localPosition;
            startingLocalRotation = trans.localRotation;
            startingLocalScale = trans.localScale;

            networkLocalPosition = startingLocalPosition;
            networkLocalRotation = startingLocalRotation;
            networkLocalScale = startingLocalScale;
        }

        protected virtual void Update() {
            if (!photonView.IsMine) {
               NetworkUpdate();
            }

            if (photonView.IsMine) {
                LocalUpdate();
            }
        }

        protected virtual void LocalUpdate() {
            if (syncObject != null) {
                var trans = transform;
                var syncTransform = syncObject;
                trans.position = syncTransform.position;
                trans.rotation = syncTransform.rotation;
                trans.localScale = syncTransform.localScale;
            }
        } 

        protected virtual void NetworkUpdate() {
            var trans = transform;
            trans.localPosition = Vector3.Lerp(trans.localPosition, networkLocalPosition, lerpMoveSpeed * Time.deltaTime);
            trans.localRotation = Quaternion.Lerp(trans.localRotation, networkLocalRotation, lerpRotationSpeed * Time.deltaTime);
            trans.localScale = Vector3.Lerp(trans.localScale, networkLocalScale, lerpMoveSpeed * Time.deltaTime);
        }

        public virtual void NetworkReset()  {
            var trans = transform;
            trans.position = startingLocalPosition;
            trans.rotation = startingLocalRotation;
            trans.localScale = startingLocalScale;
        }
    }
}
