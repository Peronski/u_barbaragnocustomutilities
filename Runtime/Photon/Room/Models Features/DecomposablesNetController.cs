using Photon.Pun;
using Photon.Realtime;
using System;
using UnityEngine;

[RequireComponent(typeof(DecomposablesController))]
public class DecomposablesNetController : MonoBehaviourPun {
    [Header("Settings")]
    [SerializeField] private bool startingStatus;

    [Header("References")]
    [SerializeField] private DecomposablesController localController;

    private bool isDecomposed;
    public bool HookDecomposed {
        get => isDecomposed;
        private set {
            isDecomposed = value;
            this.photonView.RPC(nameof(RPC_MultiPart), RpcTarget.Others, isDecomposed);
            OnDecomposedStatusChanged?.Invoke(value);
        }
    }

    public event Action<bool> OnDecomposedStatusChanged;

    private void OnValidate() {
        if (localController == null)
            TryGetComponent(out localController);
    }
    private void Awake() {
        if (localController != null)
            OnDecomposedStatusChanged += localController.EnableMultiPart;

        if (PhotonNetwork.IsMasterClient)
            LocalStatusUpdate(startingStatus);

        NetContainer.Subscribe(this);
    }
    private void OnDestroy() {
        if (localController != null)
            OnDecomposedStatusChanged -= localController.EnableMultiPart;
    }

    // LOCAL
    private void LocalStatusUpdate(bool status) {
        Debug.Log($"[DecomposableNetController][Local] - Updating local status from masterclient. {gameObject.name} is decomposed: {status}.");

        isDecomposed = status;
        OnDecomposedStatusChanged?.Invoke(status);
    }

    //LOCAL
    public void MultiPartNetworkCall() {
        Debug.Log($"[DecomposableNetController][Local] - MultiPart on {gameObject.name} called. ");

        this.photonView.RPC(nameof(RPC_MultiPartRequest), RpcTarget.MasterClient);
    }

    // LOCAL
    public void UpdateMultiPartTargetPlayerCall(Player targetPlayer) {
        if (!PhotonNetwork.IsMasterClient)
            return;

        this.photonView.RPC(nameof(RPC_MultiPart), targetPlayer, HookDecomposed);
    }

    // MASTER
    [PunRPC]
    private void RPC_MultiPartRequest() {
        if (!PhotonNetwork.IsMasterClient)
            return;

        Debug.Log($"[DecomposableNetController][Master] - Requested factorization of {gameObject.name}.");

        HookDecomposed = !HookDecomposed;
    }

    // CLIENT
    [PunRPC]
    private void RPC_MultiPart(bool status) {
        Debug.Log($"[DecomposableNetController][Client] - MultiPart call received.");

        LocalStatusUpdate(status);
    }
}
