using System;
using System.Collections.Generic;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Interactions {
    [RequireComponent(typeof(Decomposable))]
    public class SnapBackBehaviour : MonoBehaviour {
        [Header("Settings")]
        [SerializeField] private Decomposable decomposable;
        [Space]
        [SerializeField] private Transition transitionType = Transition.Smooth;
        [SerializeField] private AnimationCurve transitionCurve = default;
        [SerializeField][Range(0.0f, 1.0f)] private float transitionTime = 0.2f;

        private Vector3 startingPos;
        private Quaternion startingRot;
        private Vector3 startingScale;

        private LTDescr ltdescr;

        private Dictionary<Transition, Action> actions = new Dictionary<Transition, Action>();

        private void OnValidate() {
            if (decomposable == null)
                TryGetComponent(out decomposable);

            SetStartingValues();
        }
        private void Awake() {
            actions.Add(Transition.Instantly, InstantMotion);
            actions.Add(Transition.Smooth, SmoothMotion);

            if (decomposable != null) {
                decomposable.OnCollidersDisabled += Snapback;
            }
        }
        private void Start() {
            SetStartingValues();
        }

        private void SetStartingValues() {
            startingPos = transform.localPosition;
            startingRot = transform.localRotation;
            startingScale = transform.localScale;
        }

        private void InstantMotion() {
            transform.localPosition = startingPos;
            transform.localRotation = startingRot;
            transform.localScale = startingScale;
        }
        private void SmoothMotion() {
            LeanTweenExtentions.Clear(ltdescr, out ltdescr);

            Vector3 pos = transform.localPosition;
            Quaternion rot = transform.localRotation;
            Vector3 scale = transform.localScale;

            ltdescr = LeanTween.value(0f, 1f, transitionTime).setOnUpdate((float value) => {
                transform.localPosition = Vector3.LerpUnclamped(pos, startingPos, transitionCurve.Evaluate(value));
                transform.localRotation = Quaternion.LerpUnclamped(rot, startingRot, transitionCurve.Evaluate(value));
                transform.localScale = Vector3.LerpUnclamped(scale, startingScale, transitionCurve.Evaluate(value));
            });
        }
        private void Snapback() {
            actions[transitionType]?.Invoke();
        }
    }
}
