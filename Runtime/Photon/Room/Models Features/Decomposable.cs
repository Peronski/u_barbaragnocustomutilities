using System;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.SpatialManipulation;

[RequireComponent(typeof(ObjectManipulator))]
public class Decomposable : MonoBehaviour {
    [Header("References")]
    [SerializeField] private ObjectManipulator manipulator;

    private LTDescr ltdescr;
    private List<Collider> colliders = new List<Collider>();

    public event Action OnCollidersEnabled;
    public event Action OnCollidersDisabled;

    private void Awake() {
        Initialize();
    }

    private void Initialize() {
        Collider[] childrenColliders = GetComponentsInChildren<Collider>();
        if (childrenColliders.Length <= 0)
            return;

        for (int i = 0; i < childrenColliders.Length; i++) {
            Collider collider = childrenColliders[i];
            if (!colliders.Contains(collider))
                colliders.Add(collider);
        }

        if (manipulator == null)
            TryGetComponent(out manipulator);
    }

    private void SetDecomposableStatus(bool status, Action callback) {
        colliders.ForEach(c => c.enabled = status); //HOOK
        manipulator.enabled = status; //HOOK
        ltdescr = null;
        callback?.Invoke();
    }

    public void EnableColliders(float time) {
        LeanTweenExtentions.Clear(ltdescr, out ltdescr);
        ltdescr = LeanTween.delayedCall(time, () => SetDecomposableStatus(true, OnCollidersEnabled));
    }
    public void DisableColliders(float time) {
        LeanTweenExtentions.Clear(ltdescr, out ltdescr);
        ltdescr = LeanTween.delayedCall(time, () => SetDecomposableStatus(false, OnCollidersDisabled));
    }
}
