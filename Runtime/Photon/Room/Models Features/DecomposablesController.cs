using System;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.SpatialManipulation;
using MessageSystem;

public class DecomposablesController : MonoBehaviour {
    [Header("Settings")]
    [SerializeField] private float delayTime = 1f;

    [Header("References")]
    [SerializeField] private Collider singlePartCollider;
    [SerializeField] private ObjectManipulator singlePartManipulator;

    private LTDescr ltdescr;

    private event Action<float> OnDecompositionStart;
    private event Action OnDecompositionEnd;
    private event Action<float> OnCompositionStart;
    private event Action OnCompositionEnd;

    private void Awake() {
        Initialize();
    }

    public void EnableMultiPart(bool status) {
        if (ltdescr != null) {
            return;
        }

        if (status) {
            Factorization(OnDecompositionStart, DecompositionComplete, delayTime);
            MessageManager.Operation.SendMessage(OperationMessageTypes.DecompositionStart);
        } else {
            Factorization(OnCompositionStart, CompositionComplete, delayTime);
            MessageManager.Operation.SendMessage(OperationMessageTypes.CompositionStart);
        }
    }

    private void Initialize() {
        Decomposable[] decomposableChildren = GetComponentsInChildren<Decomposable>();

        if (decomposableChildren.Length <= 0)
            return;

        for (int i = 0; i < decomposableChildren.Length; i++) {
            Decomposable decomposable = decomposableChildren[i];
            RegisterDecomposableAsListener(decomposable);
        }
    }
    private void RegisterDecomposableAsListener(Decomposable decomposable) {
        OnDecompositionStart += decomposable.EnableColliders;
        OnCompositionStart += decomposable.DisableColliders;
    }

    private void Factorization(Action<float> callback, Action operation, float delayTime) {
        callback?.Invoke(delayTime);

        if (delayTime <= 0f) {
            operation();
            return;
        }

        ltdescr = LeanTween.delayedCall(delayTime, operation);
    }

    private void MainCollider(bool status) {
        singlePartCollider.enabled = status;
        singlePartManipulator.enabled = status; 
    }

    private void DecompositionComplete() {
        MainCollider(false);
        ltdescr = null;
        OnDecompositionEnd?.Invoke();
        MessageManager.Operation.SendMessage(OperationMessageTypes.DecompositionEnd);
    }
    private void CompositionComplete() {
        MainCollider(true);
        ltdescr = null;
        OnCompositionEnd?.Invoke();
        MessageManager.Operation.SendMessage(OperationMessageTypes.CompositionEnd);
    }
}
