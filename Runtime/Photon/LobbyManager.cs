using Photon.Pun;
using Photon.Realtime;
using UpSurgeOnRuntime.Singletons;
using MessageSystem;
using System.Collections.Generic;

namespace UpSurgeOn.MetaverseRuntime.Lobby {
    public class LobbyManager : NetworkSingleton<LobbyManager> {
        public override void OnConnectedToMaster() {
            LobbyAdvisor.Advise($"[Lobby] - Connected to Master.");

            PhotonNetwork.JoinLobby();
            //PhotonNetwork.AutomaticallySyncScene = true;
        }

        public override void OnJoinedLobby() {
            base.OnJoinedLobby();

            LobbyAdvisor.Advise($"[Lobby] - Joined Lobby.");
            MessageManager.Lobby.SendMessage(LobbyMessageTypes.OnJoinedLobby);
        }

        public override void OnLeftLobby() {
            base.OnLeftLobby();

            LobbyAdvisor.Advise($"[Lobby] - Left Lobby.");
            MessageManager.Lobby.SendMessage(LobbyMessageTypes.OnLeftLobby);
        }

        public override void OnLobbyStatisticsUpdate(List<TypedLobbyInfo> lobbyStatistics) {
            base.OnLobbyStatisticsUpdate(lobbyStatistics);

            LobbyAdvisor.Advise($"[Lobby] - Left Lobby.");
            MessageManager.Lobby.SendMessage(LobbyMessageTypes.OnLobbyStatisticsUpdate);
        }

        public override void OnRoomListUpdate(List<RoomInfo> roomList) {
            base.OnRoomListUpdate(roomList);

            LobbyAdvisor.Advise($"[Lobby] - On Room List Update.");
            MessageManager.Lobby.SendMessage(LobbyMessageTypes.OnRoomListUpdate, new LobbyMessage(roomList));
        }
    }
}
