using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Server {
    public class ServerAdvisor : MonoBehaviour {
        public static void Advise(string message) {
            Debug.Log(message);
        }
    }
}
