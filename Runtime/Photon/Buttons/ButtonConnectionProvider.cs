using Photon.Pun;
using UIPro;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Server {
    [RequireComponent(typeof(ButtonListener))]
    public class ButtonConnectionProvider : MonoBehaviour {
        [Header("References")]
        [SerializeField] private ButtonListener listener;
        [SerializeField] private ServerSettings serverSettings;

        private void Awake() {
            listener.OnClick += RequestConnection;
        }

        private void OnDestroy() {
            listener.OnClick -= RequestConnection;
        }

        private void RequestConnection(ButtonListener listener) {
            ServerConnector.TryConnectionToServer(serverSettings.AppSettings);
        }
    }
}
