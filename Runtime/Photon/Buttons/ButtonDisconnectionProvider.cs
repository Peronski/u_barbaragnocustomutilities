using UIPro;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Server {
    [RequireComponent(typeof(ButtonListener))]
    public class ButtonDisconnectionProvider : MonoBehaviour {
        [Header("References")]
        [SerializeField] private ButtonListener listener;

        private void Awake() {
            listener.OnClick += RequestDisconnection;
        }

        private void OnDestroy() {
            listener.OnClick -= RequestDisconnection;
        }

        private void RequestDisconnection(ButtonListener listener) {
            ServerConnector.DisconnectFromPhotonServer();
        }
    }
}
