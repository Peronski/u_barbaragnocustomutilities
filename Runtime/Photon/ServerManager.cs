using UpSurgeOnRuntime.Singletons;
using MessageSystem;
using Photon.Realtime;

namespace UpSurgeOn.MetaverseRuntime.Server {
    public class ServerManager : NetworkSingleton<ServerManager> {
        public override void OnConnected() {
            base.OnConnected();

            ServerAdvisor.Advise($"[Server] - Connected to Photon.");
            MessageManager.Server.SendMessage(ServerMessageTypes.OnConnected);
        }

        public override void OnDisconnected(DisconnectCause cause) {
            base.OnDisconnected(cause);

            ServerAdvisor.Advise($"[Server] - Disconnected from Photon. Cause: {cause}.");
            MessageManager.Server.SendMessage(ServerMessageTypes.OnDisconnected);
        }
    }
}
