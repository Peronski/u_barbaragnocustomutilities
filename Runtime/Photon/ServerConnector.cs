using Photon.Pun;
using Photon.Realtime;
using System.Threading.Tasks;
using Unity.Mathematics;

namespace UpSurgeOn.MetaverseRuntime.Server {
    public static class ServerConnector {
        public static bool TryConnectionToServer(AppSettings settings = null) {
            if (PhotonNetwork.IsConnected) {
                ServerAdvisor.Advise($"[Connector] - Error: client is already connected to server.");

                if (!PhotonNetwork.InLobby) {
                    ConnectionToLobby();
                    ServerAdvisor.Advise($"[Connector] - Client is connecting to lobby.");
                }

                return false;
            }
            return settings == null ? DefaultConnectToPhotonServer() : ConnectionToPhotonServer(settings);
        }

        public static async Task<bool> TryConnecionToServerAsync(AppSettings settings = null) {
            if (PhotonNetwork.IsConnected) {
                ServerAdvisor.Advise($"[Connector] - Error: client is already connected to server.");

                if (!PhotonNetwork.InLobby) {
                    await ConnectionToLobbyAsync();
                    ServerAdvisor.Advise($"[Connector] - Client is connecting to lobby.");
                }

                return false;
            }
            return settings == null ? await DefaultConnectionToPhotonServerAsync() : await ConnectionToPhotonServerAsync(settings);
        }

        public static bool DefaultConnectToPhotonServer() => PhotonNetwork.ConnectUsingSettings();
        public static async Task<bool> DefaultConnectionToPhotonServerAsync() => await Task.Factory.StartNew(() => { return PhotonNetwork.ConnectUsingSettings(); });
        public static bool ConnectionToPhotonServer(AppSettings settings) => PhotonNetwork.ConnectUsingSettings(settings);
        public static async Task<bool> ConnectionToPhotonServerAsync(AppSettings settings) => await Task.Factory.StartNew(() => { return PhotonNetwork.ConnectUsingSettings(settings); });
        public static void ConnectionToLobby() => PhotonNetwork.JoinLobby();
        public static async Task<bool> ConnectionToLobbyAsync() => await Task.Factory.StartNew(() => { return PhotonNetwork.JoinLobby(); });

        public static void DisconnectFromPhotonServer() {
            if (!PhotonNetwork.IsConnected)
                return;
            PhotonNetwork.Disconnect();
        }
    }
}
