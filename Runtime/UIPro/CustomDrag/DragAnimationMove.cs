// This was created by Lucas Bica, the best guy in the world, he's really sexy and a good developer: https://www.linkedin.com/in/lucasbica/
// You can use this without paying me. Good luck :)
using UnityEngine;
using UnityEngine.EventSystems;
using UpSurgeOnRuntime;

namespace UIPro {

    [RequireComponent(typeof(DragListener))]
    public class DragAnimationMove : UnityObject, IDragAnimation {

        [Header("Settings")]
        [SerializeField] private Vector2 offset = default;
        [SerializeField, Range(0f, 1f)] private float smoothValue = 1f;
        [SerializeField] private AnimationCurve curve = default;

        [Header("References")]
        [SerializeField] private RectTransform target = default;

        private Vector2 targetPosition;
        private Vector2 currentLerpPosition;

        private DragListener dragComponent;
        private DragListener DragComponent {
            get {
                if (dragComponent == null) {
                    dragComponent = GetComponent<DragListener>();
                }
                return dragComponent;
            }
        }

        public void OnValidate() {
            
        }

        private void Awake() {
            DragComponent.OnBeginEvent += OnBegin;
            DragComponent.OnDragEvent += OnUpdate;
            DragComponent.OnEndEvent += OnEnd;
        }

        private void Start() {
            currentLerpPosition = targetPosition = target.position;
        }

        private void Update() {
            if ((Vector2)target.position != currentLerpPosition) { // The position was changed for other script.
                currentLerpPosition = targetPosition = target.position;
            }

            currentLerpPosition = target.position = Vector2.LerpUnclamped(target.position, targetPosition, curve.Evaluate(smoothValue));
        }

        private void OnBegin(DragListener dragListener, PointerEventData eventData) {
            targetPosition = eventData.position + offset;
        }

        private void OnUpdate(DragListener dragListener, PointerEventData eventData) {
            targetPosition = eventData.position + offset;
        }

        private void OnEnd(DragListener dragListener, PointerEventData eventData) {
            targetPosition = eventData.position + offset;
        }
    }
}