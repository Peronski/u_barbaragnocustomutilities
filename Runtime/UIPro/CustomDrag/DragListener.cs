// This was created by Lucas Bica, the best guy in the world, he's really sexy and a good developer: https://www.linkedin.com/in/lucasbica/
// You can use this without paying me. Good luck :)
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UpSurgeOnRuntime;

namespace UIPro {

    public class DragListener : UnityObject, IBeginDragHandler, IEndDragHandler, IDragHandler {

        [Header("Settings")]
        [SerializeField] private bool interactive = true;

        public event Action<bool> OnInteractiveValueChanged;
        public event Action<DragListener, PointerEventData> OnBeginEvent;
        public event Action<DragListener, PointerEventData> OnDragEvent;
        public event Action<DragListener, PointerEventData> OnEndEvent;

        public bool IsDraging { get; private set; }

        public bool Interactive {
            get => interactive;
            set {
                if (interactive == value) {
                    return;
                }
                interactive = value;
                OnInteractiveValueChanged?.Invoke(interactive);
            }
        }

        private void OnValidate() {
            IDragAnimation[] dragAnimations = GetComponents<IDragAnimation>();

            for (int i = 0; i < dragAnimations.Length; i++) {
                dragAnimations[i].OnValidate();
            }
        }

        public void OnBeginDrag(PointerEventData eventData) {
            if (!interactive) {
                return;
            }

            IsDraging = true;
            OnBeginEvent?.Invoke(this, eventData);
        }

        public void OnDrag(PointerEventData eventData) {
            if (!interactive) {
                return;
            }

            OnDragEvent?.Invoke(this, eventData);
        }

        public void OnEndDrag(PointerEventData eventData) {
            if (!interactive) {
                return;
            }

            IsDraging = false;
            OnEndEvent?.Invoke(this, eventData);
        }
    }
}