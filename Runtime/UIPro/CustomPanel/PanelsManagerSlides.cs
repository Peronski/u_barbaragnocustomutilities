using UnityEngine;

namespace UIPro {

    public class PanelsManagerSlides : PanelsManager {

        [SerializeField] private PanelAnimationMoveHorizontal[] panelAnimations = default;

        public override void UpdatePanels(bool forceInsta) {
            if (PrevIndex >= 0 && PrevIndex < panelAnimations.Length) {
                PanelAnimationMoveHorizontal prevAnimation = panelAnimations[PrevIndex];
                if (prevAnimation != null) {
                    prevAnimation.DisappearFrom = PrevIndex < CurrIndex ? XDirections.Left : XDirections.Right;
                }
            }

            if (CurrIndex >= 0 && CurrIndex < panelAnimations.Length) {
                PanelAnimationMoveHorizontal currAnimation = panelAnimations[CurrIndex];
                if (currAnimation != null) {
                    currAnimation.AppearFrom = PrevIndex < CurrIndex ? XDirections.Right : XDirections.Left;
                }
            }

            base.UpdatePanels(forceInsta);
        }
    }
}