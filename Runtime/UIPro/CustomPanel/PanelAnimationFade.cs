using UnityEngine;

namespace UIPro {

    [RequireComponent(typeof(PanelListener))]
    public class PanelAnimationFade : MonoBehaviour, IPanelAnimation {

        [Header("Settings")]
        [SerializeField] private float alphaFrom = default;
        [SerializeField] private float alphaTo = default;
        [SerializeField] private AnimationCurve curveAppear = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));
        [SerializeField] private AnimationCurve curveDisappear = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));

        [Header("References")]
        [SerializeField] private CanvasGroup canvasGroup = default;

        private LTDescr ltdescr;

        private PanelListener panelComponent;
        private PanelListener PanelComponent {
            get {
                if (panelComponent == null) {
                    panelComponent = GetComponent<PanelListener>();
                }
                return panelComponent;
            }
        }

        public void OnValidate() {

        }

        private void Awake() {
            PanelComponent.OnAppear += OnAppear;
            PanelComponent.OnDisappear += OnDisappear;
        }

        private void OnAppear(float duration) {
            LeanTweenExtentions.Clear(ltdescr, out ltdescr);

            canvasGroup.alpha = alphaFrom;
            canvasGroup.blocksRaycasts = true;
            ltdescr = LeanTween.alphaCanvas(canvasGroup, alphaTo, duration).setEase(curveAppear).setOnComplete(() => ltdescr = null);
        }

        private void OnDisappear(float duration) {
            LeanTweenExtentions.Clear(ltdescr, out ltdescr);

            canvasGroup.alpha = alphaTo;
            canvasGroup.blocksRaycasts = false;
            ltdescr = LeanTween.alphaCanvas(canvasGroup, alphaFrom, duration).setEase(curveDisappear).setOnComplete(() => ltdescr = null);
        }
    }
}