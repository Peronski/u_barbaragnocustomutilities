using UnityEngine;

namespace UIPro {

    [RequireComponent(typeof(PanelListener))]
    public class PanelAnimationSibling : MonoBehaviour, IPanelAnimation {

        [Header("Settings")]
        [SerializeField] private bool invertSibling = default;

        [Header("References")]
        [SerializeField] private RectTransform rectTarget = default;

        private PanelListener panelComponent;
        private PanelListener PanelComponent {
            get {
                if (panelComponent == null) {
                    panelComponent = GetComponent<PanelListener>();
                }
                return panelComponent;
            }
        }

        public void OnValidate() {

        }

        private void Awake() {
            PanelComponent.OnAppear += OnAppear;
            PanelComponent.OnDisappearComplete += OnDisappearComplete;
        }

        private void OnAppear(float duration) {
            SetFirstSibling(invertSibling);
        }

        private void OnDisappearComplete() {
            SetFirstSibling(!invertSibling);
        }

        private void SetFirstSibling(bool value) {
            if (value) {
                rectTarget.SetAsFirstSibling();
            } else {
                rectTarget.SetAsLastSibling();
            }
        }
    }
}