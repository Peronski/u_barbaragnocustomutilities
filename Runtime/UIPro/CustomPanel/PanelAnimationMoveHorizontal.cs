using UnityEngine;
using UpSurgeOnRuntime;

namespace UIPro {

    [RequireComponent(typeof(PanelListener))]
    public class PanelAnimationMoveHorizontal : UnityObject, IPanelAnimation {

        [Header("Settings")]
        [SerializeField] private XDirections defaultAppearFrom = XDirections.Right;
        [SerializeField] private XDirections defaultDisappearFrom = XDirections.Left;
        [SerializeField] private AnimationCurve animationCurve = default;

        private XDirections appearFrom = XDirections.Right;
        private XDirections disappearFrom = XDirections.Left;
        private LTDescr ltdescr;
        private Vector2 fromPos;
        private Vector2 toPos;

        #region Properties

        private PanelListener panelComponent;
        private PanelListener PanelComponent {
            get {
                if (panelComponent == null) {
                    panelComponent = GetComponent<PanelListener>();
                }
                return panelComponent;
            }
        }

        public XDirections AppearFrom {
            get => appearFrom;
            set => appearFrom = value;
        }

        public XDirections DisappearFrom {
            get => disappearFrom;
            set => disappearFrom = value;
        }

        #endregion Properties

        public void OnValidate() {

        }

        private void Awake() {
            PanelComponent.OnAppear += OnAppear;
            PanelComponent.OnDisappear += OnDisappear;

            appearFrom = defaultAppearFrom;
            disappearFrom = defaultDisappearFrom;
        }

        private void OnUpdate(float value) {
            RectT.anchoredPosition = Vector2.LerpUnclamped(fromPos, toPos, animationCurve.Evaluate(value));
        }

        private void OnAppear(float duration) {
            float direction = appearFrom == XDirections.Right ? 1f : (-1f);
            fromPos = new Vector2(RectT.rect.size.x * direction, RectT.anchoredPosition.y);
            toPos = new Vector2(0f, RectT.anchoredPosition.y);

            LeanTweenExtentions.Clear(ltdescr, out ltdescr);
            ltdescr = LeanTween.value(0f, 1f, duration).setOnUpdate(OnUpdate).setOnComplete(() => ltdescr = null);
        }

        private void OnDisappear(float duration) {
            float direction = disappearFrom == XDirections.Right ? 1f : (-1f);
            fromPos = new Vector2(0f, RectT.anchoredPosition.y);
            toPos = new Vector2(RectT.rect.size.x * direction, RectT.anchoredPosition.y);

            LeanTweenExtentions.Clear(ltdescr, out ltdescr);
            ltdescr = LeanTween.value(0f, 1f, duration).setOnUpdate(OnUpdate).setOnComplete(() => {
                ltdescr = null;
                appearFrom = disappearFrom;
            });
        }
    }
}