using UnityEngine;

namespace UIPro {

    [RequireComponent(typeof(PanelListener))]
    public class PanelAnimationAnchoredPosition : MonoBehaviour, IPanelAnimation {

        [Header("Settings")]
        [SerializeField] private Vector2 fromPosition = default;
        [SerializeField] private Vector2 toPosition = default;
        [SerializeField] private AnimationCurve curve = default;

        [Header("References")]
        [SerializeField] private RectTransform rectTarget = default;

        private LTDescr ltdescr;

        private PanelListener panelComponent;
        private PanelListener PanelComponent {
            get {
                if (panelComponent == null) {
                    panelComponent = GetComponent<PanelListener>();
                }
                return panelComponent;
            }
        }

        public void OnValidate() {

        }

        private void Awake() {
            PanelComponent.OnAppear += OnAppear;
            PanelComponent.OnDisappear += OnDisappear;
        }

        private void OnAppear(float duration) {
            LeanTweenExtentions.Clear(ltdescr, out ltdescr);

            Vector2 from = rectTarget.anchoredPosition;

            ltdescr = LeanTween.value(0f, 1f, duration).setOnUpdate((float value) => {
                rectTarget.sizeDelta = Vector2.LerpUnclamped(from, toPosition, curve.Evaluate(value));
            }).setOnComplete(() => ltdescr = null);
        }

        private void OnDisappear(float duration) {
            LeanTweenExtentions.Clear(ltdescr, out ltdescr);

            Vector2 from = rectTarget.anchoredPosition;

            ltdescr = LeanTween.value(0f, 1f, duration).setOnUpdate((float value) => {
                rectTarget.sizeDelta = Vector2.LerpUnclamped(from, fromPosition, curve.Evaluate(value));
            }).setOnComplete(() => ltdescr = null);
        }
    }
}