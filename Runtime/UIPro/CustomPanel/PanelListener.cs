using System;
using UnityEngine;

namespace UIPro {

    public class PanelListener : UnityObject {

        [Header("Settings")]
        [SerializeField] private bool setGameObjectState = true;
        [SerializeField] private float duration = default;

        private LTDescr ltdescr;

        private PanelStates panelState = PanelStates.None;
        public PanelStates PanelState {
            get => panelState;
            set {
                if (value != panelState) {
                    panelState = value;
                    OnPanelStateChanged?.Invoke(panelState);
                }
            }
        }

        public event Action<float> OnAppear;
        public event Action OnAppearComplete;

        public event Action<float> OnDisappear;
        public event Action OnDisappearComplete;

        public event Action<PanelStates> OnPanelStateChanged;

        protected virtual void OnValidate() {
            IPanelAnimation[] panelAnimation = GetComponents<IPanelAnimation>();

            for (int i = 0; i < panelAnimation.Length; i++) {
                panelAnimation[i].OnValidate();
            }
        }

        public virtual void Appear(bool forceInstaAppear = false) {
            if (panelState == PanelStates.OffToOn || panelState == PanelStates.On) {
                return;
            }

            float duration = forceInstaAppear ? 0f : this.duration;
            LeanTweenExtentions.Clear(ltdescr, out ltdescr);

            if (setGameObjectState) {
                gameObject.SetActive(true);
            }

            PanelState = PanelStates.OffToOn;
            OnAppear?.Invoke(duration);

            if (duration <= 0f) {
                AppearComplete();
            } else {
                ltdescr = LeanTween.delayedCall(duration, AppearComplete);
            }
        }

        protected virtual void AppearComplete() {
            PanelState = PanelStates.On;
            OnAppearComplete?.Invoke();
            ltdescr = null;
        }

        public virtual void Disappear(bool forceInstaDisappear = false) {
            if (panelState == PanelStates.OnToOff || panelState == PanelStates.Off) {
                return;
            }

            float duration = forceInstaDisappear ? 0f : this.duration;
            LeanTweenExtentions.Clear(ltdescr, out ltdescr);

            PanelState = PanelStates.OnToOff;
            OnDisappear?.Invoke(duration);

            if (duration <= 0f) {
                DisappearComplete();
            } else {
                ltdescr = LeanTween.delayedCall(duration, DisappearComplete);
            }
        }

        protected virtual void DisappearComplete() {
            if (setGameObjectState) {
                gameObject.SetActive(false);
            }
            
            PanelState = PanelStates.Off;
            OnDisappearComplete?.Invoke();
            ltdescr = null;
        }

        public enum PanelStates {
            None,
            On,
            OnToOff,
            Off,
            OffToOn,
        }
    }
}