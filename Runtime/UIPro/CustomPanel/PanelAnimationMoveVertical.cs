using UnityEngine;

namespace UIPro {

    [RequireComponent(typeof(PanelListener))]
    public class PanelAnimationMoveVertical : UnityObject, IPanelAnimation {

        [Header("Settings")]
        [SerializeField] private YDirections defaultAppearFrom = YDirections.Down;
        [SerializeField] private YDirections defaultDisappearFrom = YDirections.Down;
        [SerializeField] private AnimationCurve animationCurve = default;

        private YDirections appearFrom = YDirections.Down;
        private YDirections disappearFrom = YDirections.Down;
        private LTDescr ltdescr;
        private Vector2 fromPos;
        private Vector2 toPos;

        #region Properties

        private PanelListener panelComponent;
        private PanelListener PanelComponent {
            get {
                if (panelComponent == null) {
                    panelComponent = GetComponent<PanelListener>();
                }
                return panelComponent;
            }
        }

        public YDirections AppearFrom {
            get => appearFrom;
            set => appearFrom = value;
        }

        public YDirections DisappearFrom {
            get => disappearFrom;
            set => disappearFrom = value;
        }

        #endregion Properties

        public void OnValidate() {

        }

        private void Awake() {
            PanelComponent.OnAppear += OnAppear;
            PanelComponent.OnDisappear += OnDisappear;

            appearFrom = defaultAppearFrom;
            disappearFrom = defaultDisappearFrom;
        }

        private void OnUpdate(float value) {
            RectT.anchoredPosition = Vector2.LerpUnclamped(fromPos, toPos, animationCurve.Evaluate(value));
        }

        private void OnAppear(float duration) {
            float direction = appearFrom == YDirections.Up ? 1f : (-1f);
            fromPos = new Vector2(RectT.anchoredPosition.x, RectT.rect.size.y * direction);
            toPos = new Vector2(RectT.anchoredPosition.x, 0f);

            LeanTweenExtentions.Clear(ltdescr, out ltdescr);
            ltdescr = LeanTween.value(0f, 1f, duration).setOnUpdate(OnUpdate).setOnComplete(() => ltdescr = null);
        }

        private void OnDisappear(float duration) {
            float direction = disappearFrom == YDirections.Up ? 1f : (-1f);
            fromPos = new Vector2(RectT.anchoredPosition.x, 0f);
            toPos = new Vector2(RectT.anchoredPosition.x, RectT.rect.size.y * direction);

            LeanTweenExtentions.Clear(ltdescr, out ltdescr);
            ltdescr = LeanTween.value(0f, 1f, duration).setOnUpdate(OnUpdate).setOnComplete(() => {
                ltdescr = null;
                appearFrom = disappearFrom;
            });
        }
    }
}