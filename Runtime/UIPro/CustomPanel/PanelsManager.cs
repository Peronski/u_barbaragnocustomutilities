using System;
using UnityEngine;

namespace UIPro {

    public class PanelsManager : MonoBehaviour {

        [Header("References")]
        [SerializeField] protected PanelListener[] panels = default;

        public event Action OnPanelsChanged;

        #region Properties

        public int PanelsLenght => panels.Length;
        public int PrevIndex { get; private set; } = -1;
        public int CurrIndex { get; private set; } = -1;

        #endregion Properties

        public void SetPanelIndex(int index, bool forceInsta) {
            PrevIndex = CurrIndex;
            CurrIndex = index % panels.Length;
            UpdatePanels(forceInsta);
        }

        public void GoToPrev(bool forceInsta) {
            PrevIndex = CurrIndex;
            CurrIndex = (CurrIndex - 1) % panels.Length;
            UpdatePanels(forceInsta);
        }

        public void GoToNext(bool forceInsta) {
            PrevIndex = CurrIndex;
            CurrIndex = (CurrIndex + 1) % panels.Length;
            UpdatePanels(forceInsta);
        }

        public virtual void UpdatePanels(bool forceInsta) {
            PanelListener prevPanel = null;
            PanelListener currPanel = null;

            if (PrevIndex >= 0 && PrevIndex < panels.Length) {
                prevPanel = panels[PrevIndex];
            }

            if (CurrIndex >= 0 && CurrIndex < panels.Length) {
                currPanel = panels[CurrIndex];
            }

            prevPanel?.Disappear(forceInsta);
            currPanel?.Appear(forceInsta);

            OnPanelsChanged?.Invoke();
        }
    }
}