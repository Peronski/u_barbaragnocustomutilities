using UnityEngine;

namespace UIPro {

    [RequireComponent(typeof(PanelListener))]
    public class PanelAnimationAnchors : MonoBehaviour, IPanelAnimation {

        [Header("Settings")]
        [SerializeField] private Vector4 fromAnchors = default;
        [SerializeField] private Vector4 toAnchors = default;
        [SerializeField] private AnimationCurve curve = default;

        [Header("References")]
        [SerializeField] private RectTransform rectTarget = default;

        private LTDescr ltdescr;

        private PanelListener panelComponent;
        private PanelListener PanelComponent {
            get {
                if (panelComponent == null) {
                    panelComponent = GetComponent<PanelListener>();
                }
                return panelComponent;
            }
        }

        public void OnValidate() {

        }

        private void Awake() {
            PanelComponent.OnAppear += OnAppear;
            PanelComponent.OnDisappear += OnDisappear;
        }

        private void OnAppear(float duration) {
            LeanTweenExtentions.Clear(ltdescr, out ltdescr);

            Vector4 from = new Vector4(rectTarget.anchorMin.x, rectTarget.anchorMin.y, rectTarget.anchorMax.x, rectTarget.anchorMax.y);

            ltdescr = LeanTween.value(0f, 1f, duration).setOnUpdate((float value) => {
                Vector4 anchors = Vector4.LerpUnclamped(from, toAnchors, curve.Evaluate(value));
                rectTarget.anchorMin = new Vector2(anchors.x, anchors.y);
                rectTarget.anchorMax = new Vector2(anchors.z, anchors.w);
            }).setOnComplete(() => ltdescr = null);
        }

        private void OnDisappear(float duration) {
            LeanTweenExtentions.Clear(ltdescr, out ltdescr);

            Vector4 from = new Vector4(rectTarget.anchorMin.x, rectTarget.anchorMin.y, rectTarget.anchorMax.x, rectTarget.anchorMax.y);

            ltdescr = LeanTween.value(0f, 1f, duration).setOnUpdate((float value) => {
                Vector4 anchors = Vector4.LerpUnclamped(from, fromAnchors, curve.Evaluate(value));
                rectTarget.anchorMin = new Vector2(anchors.x, anchors.y);
                rectTarget.anchorMax = new Vector2(anchors.z, anchors.w);
            }).setOnComplete(() => ltdescr = null);
        }
    }
}