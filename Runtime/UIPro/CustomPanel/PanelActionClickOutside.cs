using UnityEngine;

namespace UIPro {

    public class PanelActionClickOutside : MonoBehaviour {

        [Header("References")]
        [SerializeField] private RectTransform rectContainer = default;

        private PanelListener panelComponent;
        private PanelListener PanelComponent {
            get {
                if (panelComponent == null) {
                    panelComponent = GetComponent<PanelListener>();
                }
                return panelComponent;
            }
        }

        private void Update() {
            if (PanelComponent.PanelState != PanelListener.PanelStates.On) {
                return;
            }

            if (Application.isMobilePlatform) {
                CheckTouchInput();
            } else {
                CheckMouseInput();
            }
        }

        private void CheckTouchInput() {
            if (Input.touchCount == 0) {
                return;
            }

            for (int i = 0; i < Input.touchCount; i++) {
                Touch touch = Input.GetTouch(i);
                if (IfIsOutsideClosePanel(rectContainer, touch.position)) {
                    break;
                }
            }
        }

        private void CheckMouseInput() {
            if (Input.GetMouseButtonDown(0)) {
                IfIsOutsideClosePanel(rectContainer, Input.mousePosition);
            }
        }

        private bool IfIsOutsideClosePanel(RectTransform rect, Vector2 screenPoint) {
            if (RectTransformUtility.RectangleContainsScreenPoint(rect, screenPoint)) {
                return false;
            }

            PanelComponent.Disappear(false);
            return true;
        }
    }
}