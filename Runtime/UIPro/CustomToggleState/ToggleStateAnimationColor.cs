using UnityEngine;
using UnityEngine.UI;

namespace UIPro {

    [RequireComponent(typeof(ToggleStateListener))]
    public class ToggleStateAnimationColor : MonoBehaviour, IToggleStateAnimation {

        [Header("Settings")]
        [SerializeField] private Color[] colorsStates = new Color[0];

        [Header("References")]
        [SerializeField] private Graphic graphic = default;

        private ToggleStateListener toggleComponent;
        private ToggleStateListener ToggleComponent {
            get {
                if (toggleComponent == null) {
                    toggleComponent = GetComponent<ToggleStateListener>();
                }
                return toggleComponent;
            }
        }

        private void Reset() {
            graphic = GetComponent<Image>();
        }

        public void OnValidate() {
            UpdateColor();
        }

        private void Awake() {
            ToggleComponent.OnStateNumberValueChanged += OnStateNumberValueChanged;
        }

        private void OnEnable() {
            UpdateColor();
        }

        private void UpdateColor() {
            if (ToggleComponent.NumberState >= 0 && ToggleComponent.NumberState < colorsStates.Length) {
                graphic.color = colorsStates[ToggleComponent.NumberState];
            } else {
                graphic.color = Color.white;
            }
        }

        private void OnStateNumberValueChanged(ToggleStateListener toggleListener, int numberState) {
            UpdateColor();
        }
    }
}