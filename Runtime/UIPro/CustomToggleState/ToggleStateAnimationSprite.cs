using UnityEngine;
using UnityEngine.UI;

namespace UIPro {

    [RequireComponent(typeof(ToggleStateListener))]
    public class ToggleStateAnimationSprite : MonoBehaviour, IToggleStateAnimation {

        [Header("Settings")]
        [SerializeField] private Sprite[] spritesStates = new Sprite[0];

        [Header("References")]
        [SerializeField] private Image image = default;

        private ToggleStateListener toggleComponent;
        private ToggleStateListener ToggleComponent {
            get {
                if (toggleComponent == null) {
                    toggleComponent = GetComponent<ToggleStateListener>();
                }
                return toggleComponent;
            }
        }

        private void Reset() {
            image = GetComponent<Image>();
        }

        public void OnValidate() {
            UpdateSprite();
        }

        private void Awake() {
            ToggleComponent.OnStateNumberValueChanged += OnStateNumberValueChanged;
        }

        private void OnEnable() {
            UpdateSprite();
        }

        private void UpdateSprite() {
            if (ToggleComponent.NumberState >= 0 && ToggleComponent.NumberState < spritesStates.Length) {
                image.sprite = spritesStates[ToggleComponent.NumberState];
            } else {
                image.sprite = null;
            }
        }

        private void OnStateNumberValueChanged(ToggleStateListener toggleListener, int numberState) {
            UpdateSprite();
        }
    }
}