using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UIPro {

    public class ToggleStateListener : ButtonListener {

        [SerializeField] private int stateLenght = 2;
        [SerializeField] private int numberState = 0;

        public event Action<ToggleStateListener, int> OnStateNumberValueChanged;

        public int StateLenght {
            get => stateLenght;
        }

        public int NumberState {
            get => numberState;
            set {
                if (numberState == value) {
                    return;
                }
                numberState = value;
                OnStateNumberValueChanged?.Invoke(this, numberState);
            }
        }

        protected override void OnValidate() {
            base.OnValidate();

            numberState %= stateLenght;
            IToggleStateAnimation[] toggleStateAnimation = GetComponents<IToggleStateAnimation>();

            for (int i = 0; i < toggleStateAnimation.Length; i++) {
                toggleStateAnimation[i].OnValidate();
            }
        }

        public override void OnPointerClick(PointerEventData eventData) {
            base.OnPointerClick(eventData);
            NumberState = (numberState + 1) % stateLenght;
        }

        public virtual void SetIsOnWithoutNotify(bool isOnValue) {
            numberState = (numberState + 1) % stateLenght;
        }
    }
}