// This was created by Lucas Bica, the best guy in the world, he's really sexy and a good developer: https://www.linkedin.com/in/lucasbica/
// You can use this without paying me. Good luck :)
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UIPro {

    public class ButtonListener : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerClickHandler {

        [Header("Settings")]
        [SerializeField] protected bool interactive = true;

        public event Action<bool> OnInteractiveValueChanged;
        public event Action<ButtonListener> OnEnter;
        public event Action<ButtonListener> OnDown;
        public event Action<ButtonListener> OnUp;
        public event Action<ButtonListener> OnExit;
        public event Action<ButtonListener> OnClick;

        public bool Interactive {
            get => interactive;
            set {
                if (interactive == value) {
                    return;
                }
                interactive = value; 
                OnInteractiveValueChanged?.Invoke(interactive);
            }
        }

        protected virtual void OnValidate() {
            IButtonAnimation[] buttonAnimations = GetComponents<IButtonAnimation>();

            for (int i = 0; i < buttonAnimations.Length; i++) {
                buttonAnimations[i].OnValidate();
            }
        }

        public virtual void OnPointerEnter(PointerEventData eventData) {
            if (!interactive) {
                return;
            }

            OnEnter?.Invoke(this);
        }

        public virtual void OnPointerDown(PointerEventData eventData) {
            if (!interactive) {
                return;
            }

            OnDown?.Invoke(this);
        }

        public virtual void OnPointerUp(PointerEventData eventData) {
            if (!interactive) {
                return;
            }

            OnUp?.Invoke(this);
        }

        public virtual void OnPointerExit(PointerEventData eventData) {
            if (!interactive) {
                return;
            }

            OnExit?.Invoke(this);
        }

        public virtual void OnPointerClick(PointerEventData eventData) {
            if (!interactive) {
                return;
            }

            OnClick?.Invoke(this);
        }
    }
}