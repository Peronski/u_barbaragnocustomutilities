// This was created by Lucas Bica, the best guy in the world, he's really sexy and a good developer: https://www.linkedin.com/in/lucasbica/
// You can use this without paying me. Good luck :)
using System;
using UnityEngine;
using UnityEngine.UI;

namespace UIPro {

    [RequireComponent(typeof(ButtonListener))]
    public class ButtonAnimationColorArray : MonoBehaviour, IButtonAnimation {

        [Header("Settings")]
        [SerializeField] private ColorStates colorStates = ColorStates.defaultColorBlock;

        [Header("References")]
        [SerializeField] private Graphic[] graphics = default;

        private ButtonListener buttonComponent;
        private ButtonListener ButtonComponent {
            get {
                if (buttonComponent == null) {
                    buttonComponent = GetComponent<ButtonListener>();
                }
                return buttonComponent;
            }
        }

        private void Reset() {
            Graphic graphic = GetComponent<Graphic>();
            if (graphic != null) {
                graphics = new Graphic[1] { graphic };
            } else {
                graphics = new Graphic[0];
            }
        }

        public void OnValidate() {
            if (graphics == null) {
                return;
            }

            for (int i = 0; i < graphics.Length; i++) {
                if (graphics[i] == null) {
                    continue;
                }
                graphics[i].CrossFadeColor(ButtonComponent.Interactive ? colorStates.NormalColor : colorStates.DisabledColor, 0f, true, true);
            }
        }

        private void Awake() {
            ButtonComponent.OnInteractiveValueChanged += OnInteractiveValueChanged;
            ButtonComponent.OnEnter += OnEnter;
            ButtonComponent.OnDown += OnDown;
            ButtonComponent.OnUp += OnUp;
            ButtonComponent.OnExit += OnExit;
        }

        private void OnEnable() {
            for (int i = 0; i < graphics.Length; i++) {
                if (graphics[i] == null) {
                    continue;
                }
                graphics[i].CrossFadeColor(ButtonComponent.Interactive ? colorStates.NormalColor : colorStates.DisabledColor, 0f, true, true);
            }
        }

        private void OnInteractiveValueChanged(bool interactive) {
            for (int i = 0; i < graphics.Length; i++) {
                if (graphics[i] == null) {
                    continue;
                }
                graphics[i].CrossFadeColor(interactive ? colorStates.NormalColor : colorStates.DisabledColor, 0f, true, true);
            }
        }

        private void OnEnter(ButtonListener button) {
            for (int i = 0; i < graphics.Length; i++) {
                if (graphics[i] == null) {
                    continue;
                }
                graphics[i].CrossFadeColor(colorStates.EnterColor, colorStates.FadeDuration, true, true);
            }
        }

        private void OnDown(ButtonListener button) {
            for (int i = 0; i < graphics.Length; i++) {
                if (graphics[i] == null) {
                    continue;
                }
                graphics[i].CrossFadeColor(colorStates.DownColor, colorStates.FadeDuration, true, true);
            }
        }

        private void OnUp(ButtonListener button) {
            for (int i = 0; i < graphics.Length; i++) {
                if (graphics[i] == null) {
                    continue;
                }
                graphics[i].CrossFadeColor(colorStates.NormalColor, colorStates.FadeDuration, true, true);
            }
        }

        private void OnExit(ButtonListener button) {
            for (int i = 0; i < graphics.Length; i++) {
                if (graphics[i] == null) {
                    continue;
                }
                graphics[i].CrossFadeColor(colorStates.NormalColor, colorStates.FadeDuration, true, true);
            }
        }

        [Serializable]
        public struct ColorStates {

            [SerializeField] private Color normalColor;
            [SerializeField] private Color enterColor;
            [SerializeField] private Color downColor;
            [SerializeField] private Color disabledColor;

            [Range(1f, 5f)]
            [SerializeField] private float colorMultiplier;
            [SerializeField] private float fadeDuration;

            public static ColorStates defaultColorBlock;

            public Color NormalColor {
                get => normalColor;
                set => normalColor = value;
            }

            public Color EnterColor {
                get => enterColor;
                set => enterColor = value;
            }

            public Color DownColor {
                get => downColor;
                set => downColor = value;
            }

            public Color DisabledColor {
                get => disabledColor;
                set => disabledColor = value;
            }

            public float ColorMultiplier {
                get => colorMultiplier;
                set => colorMultiplier = value;
            }

            public float FadeDuration {
                get => fadeDuration;
                set => fadeDuration = value;
            }

            static ColorStates() {
                defaultColorBlock = new ColorStates {
                    normalColor = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue),
                    enterColor = new Color32(245, 245, 245, byte.MaxValue),
                    downColor = new Color32(200, 200, 200, byte.MaxValue),
                    disabledColor = new Color32(200, 200, 200, 128),
                    colorMultiplier = 1f,
                    fadeDuration = 0.1f
                };
            }
        }
    }
}