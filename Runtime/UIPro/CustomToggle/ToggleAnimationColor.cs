using UnityEngine;
using UnityEngine.UI;

namespace UIPro {

    [RequireComponent(typeof(ToggleListener))]
    public class ToggleAnimationColor : MonoBehaviour, IToggleAnimation {

        [Header("Settings")]
        [SerializeField] private Color colorOn = Color.white;
        [SerializeField] private Color colorOff = new Color(1f, 1f, 1f, 0.5f);

        [Header("References")]
        [SerializeField] private Graphic graphic = default;

        private ToggleListener toggleComponent;
        private ToggleListener ToggleComponent {
            get {
                if (toggleComponent == null) {
                    toggleComponent = GetComponent<ToggleListener>();
                }
                return toggleComponent;
            }
        }

        private void Reset() {
            graphic = GetComponent<Graphic>();
        }

        public void OnValidate() {
            graphic.color = ToggleComponent.IsOn ? colorOn : colorOff;
        }

        private void Awake() {
            ToggleComponent.OnIsOnValueChanged += OnIsOnValueChanged;
        }

        private void OnEnable() {
            graphic.color = ToggleComponent.IsOn ? colorOn : colorOff;
        }

        private void OnIsOnValueChanged(ToggleListener toggleListener, bool isOn) {
            graphic.color = isOn ? colorOn : colorOff;
        }
    }
}