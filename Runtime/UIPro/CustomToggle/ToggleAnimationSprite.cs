using UnityEngine;
using UnityEngine.UI;

namespace UIPro {

    [RequireComponent(typeof(ToggleListener))]
    public class ToggleAnimationSprite : MonoBehaviour, IToggleAnimation {

        [Header("Settings")]
        [SerializeField] private Sprite spriteOn = default;
        [SerializeField] private Sprite spriteOff = default;

        [Header("References")]
        [SerializeField] private Image image = default;

        private ToggleListener toggleComponent;
        private ToggleListener ToggleComponent {
            get {
                if (toggleComponent == null) {
                    toggleComponent = GetComponent<ToggleListener>();
                }
                return toggleComponent;
            }
        }

        private void Reset() {
            image = GetComponent<Image>();
        }

        public void OnValidate() {
            if (image != null) {
                image.sprite = ToggleComponent.IsOn ? spriteOn : spriteOff;
            }
        }

        private void Awake() {
            ToggleComponent.OnIsOnValueChanged += OnIsOnValueChanged;
        }

        private void OnEnable() {
            if (image != null) {
                image.sprite = ToggleComponent.IsOn ? spriteOn : spriteOff;
            }
        }

        private void OnIsOnValueChanged(ToggleListener toggleListener, bool isOn) {
            if (image != null) {
                image.sprite = isOn ? spriteOn : spriteOff;
            }
        }
    }
}