using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UIPro {

    public class ToggleListener : ButtonListener {

        [SerializeField] private bool isOn = true;

        [Header("References")]
        [SerializeField] private ToggleListenerGroup toggleListenerGroup = default;

        public event Action<ToggleListener, bool> OnIsOnValueChanged;

        public bool IsOn {
            get => isOn;
            set {
                if (isOn == value) {
                    return;
                }

                bool wasOn = isOn;
                bool hasToggleGroup = toggleListenerGroup != null && toggleListenerGroup.gameObject.activeInHierarchy && toggleListenerGroup.enabled;

                if (hasToggleGroup) {
                    isOn = value || (!toggleListenerGroup.AnyToggleOnExcept(this) && !toggleListenerGroup.AllowSwitchOff);
                } else {
                    isOn = value;
                }

                if (isOn != wasOn) {
                    OnIsOnValueChanged?.Invoke(this, isOn);
                    if (hasToggleGroup) {
                        toggleListenerGroup.NotifyToggleState(this);
                    }
                }
            }
        }

        protected override void OnValidate() {
            base.OnValidate();
            IToggleAnimation[] toggleAnimations = GetComponents<IToggleAnimation>();

            for (int i = 0; i < toggleAnimations.Length; i++) {
                toggleAnimations[i].OnValidate();
            }
        }

        private void OnDisable() {
            SetToggleGroup(null, false);
        }

        private void OnEnable() {
            SetToggleGroup(toggleListenerGroup, false);
            if (toggleListenerGroup != null) {
                OnIsOnValueChanged?.Invoke(this, isOn);
            }
        }

        private void SetToggleGroup(ToggleListenerGroup newGroup, bool setMemberValue) {
            if (toggleListenerGroup != null) {
                toggleListenerGroup.UnregisterToggle(this);
            }

            if (setMemberValue) {
                toggleListenerGroup = newGroup;
            }

            if (newGroup != null) {
                newGroup.RegisterToggle(this);
                newGroup.NotifyToggleState(this);
            }
        }

        public override void OnPointerClick(PointerEventData eventData) {
            if (!interactive) {
                return;
            }

            base.OnPointerClick(eventData);
            IsOn = !isOn;
        }
    }
}