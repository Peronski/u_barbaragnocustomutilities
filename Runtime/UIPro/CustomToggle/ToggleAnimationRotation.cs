using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UIPro {

    [RequireComponent(typeof(ToggleListener))]
    public class ToggleAnimationRotation : MonoBehaviour, IToggleAnimation {

        [Header("Settings")]
        [SerializeField, Range(0f, 360f)] private float rotationZ_On = default;
        [SerializeField, Range(0f, 360f)] private float rotationZ_Off = default;

        [Header("References")]
        [SerializeField] private RectTransform rectTarget = default;

        private ToggleListener toggleComponent;
        private ToggleListener ToggleComponent {
            get {
                if (toggleComponent == null) {
                    toggleComponent = GetComponent<ToggleListener>();
                }
                return toggleComponent;
            }
        }

        private void Reset() {
            rectTarget = GetComponent<RectTransform>();
        }

        public void OnValidate() {
            if (rectTarget != null) {
                rectTarget.eulerAngles = new Vector3(rectTarget.eulerAngles.x, rectTarget.eulerAngles.y, ToggleComponent.IsOn ? rotationZ_On : rotationZ_Off);
            }
        }

        private void Awake() {
            ToggleComponent.OnIsOnValueChanged += OnIsOnValueChanged;
        }

        private void OnEnable() {
            if (rectTarget != null) {
                rectTarget.eulerAngles = new Vector3(rectTarget.eulerAngles.x, rectTarget.eulerAngles.y, ToggleComponent.IsOn ? rotationZ_On : rotationZ_Off);
            }
        }

        private void OnIsOnValueChanged(ToggleListener toggleListener, bool isOn) {
            if (rectTarget != null) {
                rectTarget.eulerAngles = new Vector3(rectTarget.eulerAngles.x, rectTarget.eulerAngles.y, isOn ? rotationZ_On : rotationZ_Off);
            }
        }
    }
}