using UnityEngine;
using Microsoft.MixedReality.Toolkit;
using UnityEngine.XR.Interaction.Toolkit;

namespace UpSurgeOn.MetaverseRuntime.Interactions {
    public class InputSourceProvider : GeneralSingleton<InputSourceProvider> {
        [SerializeField] private ControllerLookup controllerLookup;

        public XRBaseController Right {
            get => controllerLookup.RightHandController;
        }
        public XRBaseController Left {
            get => controllerLookup.LeftHandController;
        }

        public bool AreInputSourcesDetected() {
            return (Right != null) && (Left != null);
        }

        public XRBaseController GetControllerByHand(Handedness hand) {
            if (!AreInputSourcesDetected())
                return null;

            switch (hand) {
                case Handedness.Left: return Left;
                case Handedness.Right: return Right;
                    default: return null;
            }
        }
    }
}
