using UnityEngine.InputSystem;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Inputs;

namespace UpSurgeOn.MetaverseRuntime.Input {
    [System.Serializable]
    public class InputActionListener {
        [Header("Settings")]
        [SerializeField] private InputActionProperty actionProperty;
        [HideInInspector] public string ActionName => actionProperty.action.name;

        public bool SubscribeAction(System.Action<InputAction.CallbackContext> callback, ActionState state = ActionState.Performed) {
            if (state == ActionState.None)
                return false;

            if (state == ActionState.Started)
                actionProperty.action.started += callback;
            if (state == ActionState.Canceled)
                actionProperty.action.canceled += callback;
            if (state == ActionState.Performed)
                actionProperty.action.performed += callback;

            return true;
        }

        public void UnsubscribeAction(System.Action<InputAction.CallbackContext> callback, ActionState state = ActionState.Performed) {
            if (state == ActionState.None)
                return;

            if (state == ActionState.Started)
                actionProperty.action.started -= callback;
            if (state == ActionState.Canceled)
                actionProperty.action.canceled -= callback;
            if (state == ActionState.Performed)
                actionProperty.action.performed -= callback;
        }

        public void EnableAction() {
            actionProperty.EnableDirectAction();
        }
        public void DisableAction() {
            actionProperty.DisableDirectAction();
        }
    }
}
