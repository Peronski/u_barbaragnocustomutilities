public static class InputActionConsts
{
    public const string PRIMARY_BUTTON_ACTION = "Primary";
    public const string SECONDARY_BUTTON_ACTION = "Secondary";
    public const string GRIP_BUTTON_ACTION = "Grip";
    public const string GRIP_VALUE_BUTTON_ACTION = "Grip Value";
    public const string TRIGGER_BUTTON_ACTION = "Trigger";
    public const string TRIGGER_VALUE_BUTTON_ACTION = "Trigger Value";
    public const string THUMBSTICK_BUTTON_ACTION = "Thumbstick";
    public const string THUMBSTICK_VALUE_BUTTON_ACTION = "Thumbstick Value";
}
