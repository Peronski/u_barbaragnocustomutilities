using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;
using System.Linq;

namespace UpSurgeOn.MetaverseRuntime.Input {
    public class InputActionController : GeneralSingleton<InputActionController> {
        [Header("References - Right Controller")]
        [SerializeField] private List<InputActionListener> rightActionListeners;

        [Header("References - Left Controller")]
        [SerializeField] private List<InputActionListener> leftActionListeners;

        private void OnEnable() {
            rightActionListeners.ForEach(listener => listener.EnableAction());
            leftActionListeners.ForEach(listener => listener.EnableAction());

            Debug.Log($"[ActionsListenersController] - Enabled all actions.");
        }
        private void OnDisable() {
            rightActionListeners.ForEach(listener => listener.DisableAction());
            leftActionListeners.ForEach(listener => listener.DisableAction());

            Debug.Log($"[ActionsListenersController] - Disabled all actions.");
        }

        public bool AskSubscriptionAction(Handedness hand, string actionName, System.Action<InputAction.CallbackContext> callback, ActionState state) {
            if (hand == Handedness.Left) {
                InputActionListener listener = leftActionListeners.FirstOrDefault(listener => string.Equals(listener.ActionName, actionName));
                if (listener != null) {
                    return listener.SubscribeAction(callback, state);
                }
            }
            if (hand == Handedness.Right) {
                InputActionListener listener = rightActionListeners.FirstOrDefault(listener => string.Equals(listener.ActionName, actionName));
                if (listener != null) {
                    return listener.SubscribeAction(callback, state);
                }
            }
            return false;
        }

        public void AskUnsubscriptionAction(Handedness hand, string actionName, System.Action<InputAction.CallbackContext> callback, ActionState state) {
            if (hand == Handedness.Left) {
                InputActionListener listener = leftActionListeners.FirstOrDefault(listener => string.Equals(listener.ActionName, actionName));
                if (listener != null) {
                    listener.UnsubscribeAction(callback, state);
                    return;
                }
            }
            if (hand == Handedness.Right) {
                InputActionListener listener = rightActionListeners.FirstOrDefault(listener => string.Equals(listener.ActionName, actionName));
                if (listener != null) {
                    listener.UnsubscribeAction(callback, state);
                    return;
                }
            }
        }
    }
}
