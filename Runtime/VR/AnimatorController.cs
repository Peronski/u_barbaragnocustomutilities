using UnityEngine;
using UnityEngine.InputSystem;
using UpSurgeOn.MetaverseRuntime.Input;

public class AnimatorController : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private Handedness hand;

    [Header("References")]
    [SerializeField] private Animator animator;

    private float currentTriggerValue = 0f;
    private float currentGripValue = 0f;
    private float currentThumbstickValueX;
    private float currentThumbstickValueY;

    private void Awake() {
        // PRIMARY BUTTON
        InputActionController.Instance.AskSubscriptionAction(hand, InputActionConsts.PRIMARY_BUTTON_ACTION, OnPrimaryButtonInput, ActionState.Started);
        InputActionController.Instance.AskSubscriptionAction(hand, InputActionConsts.PRIMARY_BUTTON_ACTION, OnPrimaryButtonInput, ActionState.Canceled);
        // SECONDARY BUTTON
        InputActionController.Instance.AskSubscriptionAction(hand, InputActionConsts.SECONDARY_BUTTON_ACTION, OnSecondaryButtonInput, ActionState.Started);
        InputActionController.Instance.AskSubscriptionAction(hand, InputActionConsts.SECONDARY_BUTTON_ACTION, OnSecondaryButtonInput, ActionState.Canceled);
        // GRIP
        InputActionController.Instance.AskSubscriptionAction(hand, InputActionConsts.GRIP_VALUE_BUTTON_ACTION, OnGripButtonInput, ActionState.Performed);
        InputActionController.Instance.AskSubscriptionAction(hand, InputActionConsts.GRIP_VALUE_BUTTON_ACTION, OnGripButtonInput, ActionState.Canceled);
        // TRIGGER
        InputActionController.Instance.AskSubscriptionAction(hand, InputActionConsts.TRIGGER_VALUE_BUTTON_ACTION, OnTriggerButtonInput, ActionState.Performed);
        InputActionController.Instance.AskSubscriptionAction(hand, InputActionConsts.TRIGGER_VALUE_BUTTON_ACTION, OnTriggerButtonInput, ActionState.Canceled);
        // THUMBSTICK
        InputActionController.Instance.AskSubscriptionAction(hand, InputActionConsts.THUMBSTICK_VALUE_BUTTON_ACTION, OnThumbstickButtonInput, ActionState.Performed);
        InputActionController.Instance.AskSubscriptionAction(hand, InputActionConsts.THUMBSTICK_VALUE_BUTTON_ACTION, OnThumbstickButtonInput, ActionState.Canceled);
    }
    private void OnDestroy() {
        // PRIMARY BUTTON
        InputActionController.Instance.AskUnsubscriptionAction(hand, InputActionConsts.PRIMARY_BUTTON_ACTION, OnPrimaryButtonInput, ActionState.Started);
        InputActionController.Instance.AskUnsubscriptionAction(hand, InputActionConsts.PRIMARY_BUTTON_ACTION, OnPrimaryButtonInput, ActionState.Canceled);
        // SECONDARY BUTTON
        InputActionController.Instance.AskUnsubscriptionAction(hand, InputActionConsts.SECONDARY_BUTTON_ACTION, OnSecondaryButtonInput, ActionState.Started);
        InputActionController.Instance.AskUnsubscriptionAction(hand, InputActionConsts.SECONDARY_BUTTON_ACTION, OnSecondaryButtonInput, ActionState.Canceled);
        // GRIP
        InputActionController.Instance.AskUnsubscriptionAction(hand, InputActionConsts.GRIP_VALUE_BUTTON_ACTION, OnGripButtonInput, ActionState.Performed);
        InputActionController.Instance.AskUnsubscriptionAction(hand, InputActionConsts.GRIP_VALUE_BUTTON_ACTION, OnGripButtonInput, ActionState.Canceled);
        // TRIGGER
        InputActionController.Instance.AskUnsubscriptionAction(hand, InputActionConsts.TRIGGER_VALUE_BUTTON_ACTION, OnTriggerButtonInput, ActionState.Performed);
        InputActionController.Instance.AskUnsubscriptionAction(hand, InputActionConsts.TRIGGER_VALUE_BUTTON_ACTION, OnTriggerButtonInput, ActionState.Canceled);
        // THUMBSTICK
        InputActionController.Instance.AskUnsubscriptionAction(hand, InputActionConsts.THUMBSTICK_VALUE_BUTTON_ACTION, OnThumbstickButtonInput, ActionState.Performed);
        InputActionController.Instance.AskUnsubscriptionAction(hand, InputActionConsts.THUMBSTICK_VALUE_BUTTON_ACTION, OnThumbstickButtonInput, ActionState.Canceled);
    }

    private void Update() {
        if (animator == null)
            return;

        animator.SetFloat("Grip Value", currentGripValue);
        animator.SetFloat("Trigger Value", currentTriggerValue);
        animator.SetFloat("Thumbstick X", currentThumbstickValueX);
        animator.SetFloat("Thumbstick Y", currentThumbstickValueY);
    }

    private void OnPrimaryButtonInput(InputAction.CallbackContext context) {
        animator.SetBool("Primary Hold", context.ReadValueAsButton());
    }

    private void OnSecondaryButtonInput(InputAction.CallbackContext context) {
        animator.SetBool("Secondary Hold", context.ReadValueAsButton());
    }

    private void OnTriggerButtonInput(InputAction.CallbackContext context) {
        currentTriggerValue = context.ReadValue<float>();
        currentTriggerValue = Mathf.Clamp(currentTriggerValue, 0, 1);
    }

    private void OnGripButtonInput(InputAction.CallbackContext context) {
        currentGripValue = context.ReadValue<float>();
        currentGripValue = Mathf.Clamp(currentGripValue, 0, 1);
    }

    private void OnThumbstickButtonInput(InputAction.CallbackContext context) { 
        Vector2 currentThumbstickValue = context.ReadValue<Vector2>();

        currentThumbstickValueX = Mathf.Clamp(currentThumbstickValue.x, -1f, 1f);
        currentThumbstickValueY = Mathf.Clamp(currentThumbstickValue.y, -1f, 1f);
    }
}
