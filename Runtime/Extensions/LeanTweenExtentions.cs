public static class LeanTweenExtentions {

    public static void Clear(LTDescr inLTDescr, out LTDescr outLTDescr) {
        if (inLTDescr != null) {
            LeanTween.cancel(inLTDescr.id);
        }

        outLTDescr = null;
    }
}
