using MessageSystem;
using UpSurgeOn.MetaverseRuntime.Utilities;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class KeyboardPanelSwitch : Panel<KeyboardPanels> {
        private void OnDestroy() {
            MessageManager.Keyboard.DetachListener(KeyboardMessageTypes.UI, Switch);
        }

        private void Awake() {
            MessageManager.Keyboard.AttachListener(KeyboardMessageTypes.UI, Switch);
        }

        private void Switch(KeyboardMessageTypes type, MessageClass message) {
            if (!MessageClass.TryCastTo(message, out KeyboardMessage<KeyboardPanels> content))
                return;

            Switch(content.Body);
        }

        private void Switch(KeyboardPanels type) {
            if (panelType == type)
                gameObject.SetActive(true);
            else
                gameObject.SetActive(false);
        }
    }
}
