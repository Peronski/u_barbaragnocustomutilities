using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class KeyboardRequesterText : KeyboardRequester {
        [Header("References")]
        [SerializeField] private TextMeshProUGUI text;

        public override string Text => text.text;

        public override void OnInput(string letter) {
            base.OnInput(letter);
            text.text = letter;
        }
    }
}
