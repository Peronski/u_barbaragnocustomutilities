using MessageSystem;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class KeyboardProvider : MonoBehaviour {
        public event System.Action<Request> OnRequestEvent;

        private void Awake() {
            MessageManager.Keyboard.AttachListener(KeyboardMessageTypes.Request, OnRequest);
        }
        private void OnDestroy() {
            MessageManager.Keyboard.DetachListener(KeyboardMessageTypes.Request, OnRequest);
        }

        private void OnRequest(KeyboardMessageTypes types, MessageClass message) {
            if (!MessageClass.TryCastTo(message, out KeyboardMessage<Request> content))
                return;

            Request request = content.Body;
            if (request == null)
                return;

            OnRequestEvent?.Invoke(request);

            if (request.Requester == null)
                return;

            request.Requester.OnRequest();
        }
    }
}
