using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public abstract class CreationTextLogicSO : ScriptableObject {
        public abstract void AddCharLogic(char element);
        public abstract void AddStringLogic(string elements);
        public abstract void RemoveLogic(bool allText);
        public abstract void ReplaceLogic(string elements);
        public abstract string GetText();
        public abstract void CleanText();
        public abstract void AddRequester(KeyboardRequester keyboardRequester);
        public abstract void RemoveRequester();
        public abstract KeyboardRequester GetRequester();
    }
}
