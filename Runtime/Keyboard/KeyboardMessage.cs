using MessageSystem;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class KeyboardMessage<T> : MessageClass {
        public T Body { get; set; }
    }

    public class Delete {
        public bool DeleteAllText { get; set; }
    }

    public class ButtonValueCase {
        public bool Capslock { get; set; }
        public bool Upper { get; set; }
        public bool Primary { get; set; }
        public bool Secondary { get; set; }
    }

    public class Request {
        public KeyboardRequester Requester { get; set; }
        public bool KeyboardActiveStatus { get; set; }
        public bool Complete { get; set; }
    }
}