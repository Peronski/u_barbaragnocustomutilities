using MessageSystem;
using UnityEngine;
using UnityEngine.Events;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public abstract class KeyboardRequester : MonoBehaviour {
        [HideInInspector] public virtual string Text { get; private set; }
        [HideInInspector] public virtual int CaretIndex { get; set; }

        [Header("Unity Events")]
        [SerializeField] private UnityEvent OnRequestUnityEvent;
        [SerializeField] private UnityEvent<string> OnInputUnityEvent;
        [SerializeField] private UnityEvent<string> OnEndEditUnityEvent;

        public void RequestKeyboard() {
            KeyboardMessage<Request> request = new KeyboardMessage<Request>() { Body = new Request { Requester = this, KeyboardActiveStatus = true } };
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Request, request);
        }

        public virtual void OnRequest() {
            OnRequestUnityEvent?.Invoke();
        }
        public virtual void OnInput(string letter) {
            OnInputUnityEvent?.Invoke(letter);
        }
        public virtual void OnEndEdit(string text) {
            OnEndEditUnityEvent?.Invoke(text);
        }
    }
}