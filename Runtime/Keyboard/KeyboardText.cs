using MessageSystem;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    [RequireComponent(typeof(KeyboardProvider))]
    public class KeyboardText : MonoBehaviour {
        [Header("References")]
        [SerializeField] private KeyboardProvider provider;

        [Header("Data")]
        [SerializeField] private CreationTextLogicSO creationTextLogicSOSource;

        private string Text => currentCreationTextLogicSO.GetText();
        private CreationTextLogicSO currentCreationTextLogicSO;

        private event System.Action<string> OnInputEvent;
        private event System.Action<string> OnEndEditEvent;

        private void OnDestroy() {
            provider.OnRequestEvent -= BindRequesterToTextEvents;

            MessageManager.Keyboard.DetachListener(KeyboardMessageTypes.Value, OnValue);
            MessageManager.Keyboard.DetachListener(KeyboardMessageTypes.Delete, OnDelete);
            MessageManager.Keyboard.DetachListener(KeyboardMessageTypes.EndEdit, OnEndEdit);
        }

        private void Awake() {
            provider.OnRequestEvent += BindRequesterToTextEvents;

            MessageManager.Keyboard.AttachListener(KeyboardMessageTypes.Value, OnValue);
            MessageManager.Keyboard.AttachListener(KeyboardMessageTypes.Delete, OnDelete);
            MessageManager.Keyboard.AttachListener(KeyboardMessageTypes.EndEdit, OnEndEdit);

            currentCreationTextLogicSO = Instantiate(creationTextLogicSOSource);
        }

        private void OnValue(KeyboardMessageTypes type, MessageClass message) {
            if (MessageClass.TryCastTo(message, out KeyboardMessage<char> charContent)) {
                currentCreationTextLogicSO.AddCharLogic(charContent.Body);
                OnInputEvent?.Invoke(Text);
                return;
            }

            if (MessageClass.TryCastTo(message, out KeyboardMessage<string> stringContent)) {
                currentCreationTextLogicSO.AddStringLogic(stringContent.Body);
                OnInputEvent?.Invoke(Text);
                return;
            }
        }

        private void OnDelete(KeyboardMessageTypes type, MessageClass message) {
            if (string.IsNullOrEmpty(Text))
                return;

            if (MessageClass.TryCastTo(message, out KeyboardMessage<Delete> content)) {
                if (content.Body.DeleteAllText) {
                    currentCreationTextLogicSO.RemoveLogic(true); 
                    OnInputEvent?.Invoke(Text);
                    return;
                }
            }

            currentCreationTextLogicSO.RemoveLogic(false);
            OnInputEvent?.Invoke(Text);
        }

        private void OnEndEdit(KeyboardMessageTypes type, MessageClass message) {
            if (!MessageClass.TryCastTo(message, out KeyboardMessage<Request> content))
                return;

            if (content.Body != null) {
                if (content.Body.Complete) {
                    OnEndEditEvent?.Invoke(Text);
                    currentCreationTextLogicSO.CleanText();
                    currentCreationTextLogicSO.RemoveRequester();
                    OnInputEvent = null;
                    OnEndEditEvent = null;
                }
            }
        }

        private void BindRequesterToTextEvents(Request request) {
            if (!VerifyRequestCondition(request))
                return;

            CleanPreviousBinding();
            SetNewBinding(request);
        }

        private bool VerifyRequestCondition(Request request) {
            if (request == null || request.Requester == null)
                return false;

            if (currentCreationTextLogicSO.GetRequester() == request.Requester)
                return false;

            return true;
        }

        private void CleanPreviousBinding() {
            if (currentCreationTextLogicSO.GetRequester() != null) {
                OnEndEditEvent?.Invoke(Text);
                currentCreationTextLogicSO.CleanText();
                currentCreationTextLogicSO.RemoveRequester();
                OnInputEvent = null;
                OnEndEditEvent = null;
            }
        }

        private void SetNewBinding(Request request) {
            currentCreationTextLogicSO.AddRequester(request.Requester);
            currentCreationTextLogicSO.ReplaceLogic(request.Requester.Text);
            OnInputEvent += request.Requester.OnInput;
            OnEndEditEvent += request.Requester.OnEndEdit;
        }
    }
}