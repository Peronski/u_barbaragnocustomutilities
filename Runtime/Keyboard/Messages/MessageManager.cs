namespace MessageSystem {

    public static partial class MessageManager {

        public readonly static MessageChannel<KeyboardMessageTypes> Keyboard = new MessageChannel<KeyboardMessageTypes>();
    }

    public enum KeyboardMessageTypes {
        None,
        UI,
        Value,
        Delete,
        EndEdit,
        Upper,
        Letters,
        Simbols,
        Request
    }
}
