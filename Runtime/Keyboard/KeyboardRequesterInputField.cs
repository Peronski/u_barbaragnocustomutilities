using System.Collections;
using System.Reflection;
using TMPro;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class KeyboardRequesterInputField : KeyboardRequester {
        [Header("References")]
        [SerializeField] private TMP_InputField inputField;

        private Coroutine coroutine;
        private WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
        private WaitForSeconds waitForSeconds = new WaitForSeconds(0.1f); //0.03

        private int caretWidth = 1;

        private int caretIndex;
        public override int CaretIndex {
            get {
                return caretIndex;
            }
            set {
                caretIndex = value;
                SetCaretPosition();
            }
        }

        //private FieldInfo i_AllowField;
        //private MethodInfo i_SetCaretVisible;

        public bool IsFocused => inputField.isFocused;
        public override string Text => inputField.text;

        public override void OnInput(string letter) {
            base.OnInput(letter);
            inputField.text = letter;
        }

        public void Test_CaretIndex() => Debug.Log($"Is focused: {inputField.isFocused}. CaretIndex from InputField is: {inputField.caretPosition}. Local CaretIndex is: {CaretIndex}.");

        //private void Start() {
        //    i_AllowField = inputField.GetType().GetField("m_AllowInput", BindingFlags.NonPublic | BindingFlags.Instance);
        //    i_SetCaretVisible = inputField.GetType().GetMethod("SetCaretActive", BindingFlags.NonPublic | BindingFlags.InvokeMethod | BindingFlags.Instance);
        //}

        private void Update() {
            Debug.Log($"[Unfocused] - CaretIndex from InputField is: {inputField.caretPosition}. Local CaretIndex is: {CaretIndex}.");
            if (!inputField.isFocused) {
                CaretBlink();
            }

            if (inputField.isFocused) {
                if (coroutine != null) {
                    StopCoroutine(coroutine);
                    coroutine = null;
                }

                CaretIndex = inputField.caretPosition;
                Debug.Log($"[Focused] - CaretIndex from InputField is: {inputField.caretPosition}. Local CaretIndex is: {CaretIndex}.");
            }
        }

        private void SetCaretPosition() {
            StartCoroutine(Co_SetCaretPosition());
        }

        private void CaretBlink() {
            coroutine = StartCoroutine(Co_CaretBlink());
        }

        IEnumerator Co_CaretBlink() {
            //i_AllowField.SetValue(inputField, true);
            //i_SetCaretVisible.Invoke(inputField, null);
            inputField.caretWidth = 0;

            yield return waitForSeconds;

            inputField.caretWidth = caretWidth;
            //i_AllowField.SetValue(inputField, false);
        }

        IEnumerator Co_SetCaretPosition() {
            inputField.caretWidth = caretWidth;

            yield return waitForEndOfFrame;

            inputField.caretWidth = caretWidth;
            inputField.caretPosition = caretIndex;
        }
    }
}
