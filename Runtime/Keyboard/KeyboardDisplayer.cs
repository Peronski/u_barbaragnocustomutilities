using UnityEngine;


namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    [RequireComponent(typeof(KeyboardProvider))]
    public class KeyboardDisplayer : MonoBehaviour {
        [Header("References")]
        [SerializeField] private KeyboardProvider provider;

        [Header("Prefab")]
        [SerializeField] private GameObject keyboardPrefab;

        private GameObject keyboard;

        public event System.Action<bool> OnDisplayEvent;

        private void Awake() {
            provider.OnRequestEvent += DisplayKeyboard;

            KeyboardInstantiation(null);
            KeyboardInitialization();
        }
        private void OnDestroy() {
            provider.OnRequestEvent -= DisplayKeyboard;

            KeyboardAnnihilation();
        }

        private void KeyboardInstantiation(Transform parent) {
            keyboard = Instantiate(keyboardPrefab, parent);
        }

        private void KeyboardInitialization() {
            if (keyboard == null)
                return;

            if (keyboard.TryGetComponent(out KeyboardAnimator animator)) {
                animator.SetObjectToTrack(Camera.main.transform);
                OnDisplayEvent += animator.Activate;
                animator.Activate(false);
            }
        }

        private void KeyboardAnnihilation() {
            if (keyboard == null)
                return;

            if (keyboard.TryGetComponent(out KeyboardAnimator animator)) {
                OnDisplayEvent -= animator.Activate;
                animator.Activate(false);
            }
        }

        private void DisplayKeyboard(Request request) {
            bool isActive = request.KeyboardActiveStatus;
            OnDisplayEvent?.Invoke(isActive);
        }
    }
}
