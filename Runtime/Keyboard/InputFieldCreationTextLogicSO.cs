using System.Text;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    [CreateAssetMenu(fileName = "InputFieldCreationTextLogicSO", menuName = "ScriptableObjects/CreationTextLogicSO/InputField")]
    public class InputFieldCreationTextLogicSO : CreationTextLogicSO {
        private StringBuilder sb = new StringBuilder();
        private KeyboardRequesterInputField requester;

        public KeyboardRequesterInputField Requester { get; private set; }

        public override void AddCharLogic(char element) {
            Debug.Log($"Char insert at CaretIndex: {requester.CaretIndex}.");

            sb.Insert(requester.CaretIndex, element);
            requester.CaretIndex++;

            Debug.Log($"Text after char input: {sb}.");
        }

        public override void AddStringLogic(string elements) {
            Debug.Log($"String insert at CaretIndex: {requester.CaretIndex}.");

            sb.Insert(requester.CaretIndex, elements);
            requester.CaretIndex += elements.Length;

            Debug.Log($"Text after string input: {sb}.");
        }

        public override void RemoveLogic(bool allText) {
            Debug.Log($"Removing at CaretIndex: {requester.CaretIndex}.");

            if (allText) {
                CleanText();
                Debug.Log($"Text after remove all input: {sb}.");
                return;
            }

            if (requester.CaretIndex <= 0)
                return;

            sb.Remove(requester.CaretIndex - 1, 1);
            requester.CaretIndex--;

            Debug.Log($"Text after remove input: {sb}.");
        }

        public override void ReplaceLogic(string elements) {
            if (sb.Length > 0)
                sb.Clear();

            sb.Append(elements);
        }

        public override void CleanText() {
            sb.Clear();
            requester.CaretIndex = 0;
            Debug.Log($"Text after clean input: {sb}.");
        }

        public override string GetText() {
            return sb.ToString();
        }

        public override void AddRequester(KeyboardRequester keyboardRequester) => requester = keyboardRequester as KeyboardRequesterInputField;
        public override void RemoveRequester() => requester = null;
        public override KeyboardRequester GetRequester() => requester;
    }
}
