using MessageSystem;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class KeyboardMenu : MonoBehaviour {
        [Header("Settings")]
        [SerializeField] private KeyboardPanels startingPanel;

        private void Start() {
            Show(startingPanel);
        }

        public static void Show(KeyboardPanels type) {
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.UI, new KeyboardMessage<KeyboardPanels>() { Body = type });
        }
    }
}
