using UnityEngine;
using UpSurgeOn.MetaverseRuntime.Utilities;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class KeyboardAnimator : ObjectTracker {
        private bool isActive = false;
        private bool firstTime = true;

        protected override void OnEnable() {
            if (firstTime)
                return;

            base.OnEnable();
        }
        protected override void Update() {
            if (!isActive)
                return;

            base.Update();
        }

        public void Activate(bool status) {
            isActive = status;
            gameObject.SetActive(isActive);
        }

        public void SetObjectToTrack(Transform target) => trackedObject = target;
    }
}
