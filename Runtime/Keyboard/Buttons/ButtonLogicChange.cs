using System;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public abstract class ButtonLogicChange : ButtonLogicLetter {
        public Action<char> OnLetterValueChanged;
        protected abstract char Letter { get; }
        public abstract void DefaultConfiguration();
    }
}
