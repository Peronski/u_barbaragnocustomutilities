using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UIPro {
    public class ButtonPressedListener : ButtonListener {

        [SerializeField] private float delayTime;

        private LTDescr ltdescr;

        public event Action<ButtonPressedListener, PointerEventData> OnHold;

        public override void OnPointerDown(PointerEventData eventData) {
            base.OnPointerDown(eventData);

            ltdescr = LeanTween.delayedCall(delayTime, () => {
                OnHold?.Invoke(this, eventData);
            });
        }

        public override void OnPointerUp(PointerEventData eventData) {
            base.OnPointerUp(eventData);

            LeanTweenExtentions.Clear(ltdescr, out ltdescr);
        }
    }
}
