using MessageSystem;
using UIPro;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class ButtonLogicChangeLetter : ButtonLogicChange {
        [Header("Settings")]
        [SerializeField] private char secondaryLetter;

        private bool primaryEnabled;
        protected override char Letter => primaryEnabled ? letter : secondaryLetter;

        private void OnDestroy() {
            MessageManager.Keyboard.DetachListener(KeyboardMessageTypes.Simbols, Simbols);
        }

        private void OnEnable() {
            DefaultConfiguration();
        }

        public override void DefaultConfiguration() {
            primaryEnabled = true;
            OnLetterValueChanged?.Invoke(Letter);
        }

        protected override void Awake() {
            base.Awake();

            MessageManager.Keyboard.AttachListener(KeyboardMessageTypes.Simbols, Simbols);
        }

        protected override void OnClick(ButtonListener listener) {
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Value, new KeyboardMessage<char>() { Body = Letter });
        }

        private void Simbols(KeyboardMessageTypes type, MessageClass message) {
            if (!MessageClass.TryCastTo(message, out KeyboardMessage<ButtonValueCase> content))
                return;

            if (primaryEnabled == content.Body.Primary)
                return;

            primaryEnabled = content.Body.Primary;
            OnLetterValueChanged?.Invoke(Letter);
        }
    }
}
