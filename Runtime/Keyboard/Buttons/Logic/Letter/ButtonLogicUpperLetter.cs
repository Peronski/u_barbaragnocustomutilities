using MessageSystem;
using UIPro;
using System;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class ButtonLogicUpperLetter : ButtonLogicChange {

        private bool capslock;
        private bool upperCase;

        protected override char Letter => upperCase ? char.ToUpper(letter) : letter;
        public void OnEnable() {
            DefaultConfiguration();
        }

        private void OnDestroy() {
            MessageManager.Keyboard.DetachListener(KeyboardMessageTypes.Upper, OnUpper);
        }

        protected override void Awake() {
            base.Awake();

            MessageManager.Keyboard.AttachListener(KeyboardMessageTypes.Upper, OnUpper);
        }

        private void SetUpper(bool status) {
            upperCase = status;
            OnLetterValueChanged?.Invoke(Letter);
        }

        public override void DefaultConfiguration() {
            capslock = false;
            upperCase = false;
            SetUpper(upperCase);
        }

        protected override void OnClick(ButtonListener listener) {
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Value, new KeyboardMessage<char>() { Body = Letter }); 
        }

        public override void MRTK_OnClick() {
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Value, new KeyboardMessage<char>() { Body = Letter });
        }

        private void OnUpper(KeyboardMessageTypes messageType, MessageClass message) {
            if (!MessageClass.TryCastTo(message, out KeyboardMessage<ButtonValueCase> content))
                return;

            if(content.Body.Capslock) {
                EnableCapslock();
            }

            SetUpper(content.Body.Upper);
        }

        private void EnableCapslock() {
            capslock = true;
            SetUpper(capslock);
        }
    }
}

