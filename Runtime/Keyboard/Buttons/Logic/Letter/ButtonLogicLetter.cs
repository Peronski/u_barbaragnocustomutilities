using MessageSystem;
using UIPro;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class ButtonLogicLetter : MonoBehaviour {

        [Header("Settings")]
        [SerializeField] protected char letter;

        [Header("References")]
        [SerializeField] protected ButtonListener buttonListener;

        protected virtual void Awake() {
            if (buttonListener != null)
                buttonListener.OnClick += OnClick;
        }

        protected virtual void OnClick(ButtonListener listener) {
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Value, new KeyboardMessage<char>() { Body = letter });
        }

        public virtual void MRTK_OnClick() {
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Value, new KeyboardMessage<char>() { Body = letter });
        }
    }
}
