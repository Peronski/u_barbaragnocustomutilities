using TMPro;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    [RequireComponent(typeof(ButtonLogicChange))]
    public class ButtonLogicChangeLetterAspect : MonoBehaviour {

        [Header("References")]
        [SerializeField] private TextMeshProUGUI textMeshPro;
        [SerializeField] private ButtonLogicChange buttonLogicChange;

        private void Awake() {
            buttonLogicChange.OnLetterValueChanged += SetText;
        }

        private void SetText(char letter) {
            textMeshPro.text = letter.ToString();
        }
    }
}
