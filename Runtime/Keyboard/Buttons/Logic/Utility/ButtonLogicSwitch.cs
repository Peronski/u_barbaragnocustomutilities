using MessageSystem;
using UIPro;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class ButtonLogicSwitch : MonoBehaviour {
        [Header("References")]
        [SerializeField] protected ButtonListener buttonListener;

        private bool isPrimaryEnabled = true;

        protected void Awake() {
            if (buttonListener != null)
                buttonListener.OnClick += OnClick;
        }

        private void OnEnable() {
            isPrimaryEnabled = true;
        }

        protected virtual void OnClick(ButtonListener listener) {
            isPrimaryEnabled = !isPrimaryEnabled;

            KeyboardMessage<ButtonValueCase> message = new KeyboardMessage<ButtonValueCase>() { Body = new ButtonValueCase() { Primary = isPrimaryEnabled, Secondary = !isPrimaryEnabled } };
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Simbols, message);
        }

        public virtual void MRTK_OnClick() {
            isPrimaryEnabled = !isPrimaryEnabled;

            KeyboardMessage<ButtonValueCase> message = new KeyboardMessage<ButtonValueCase>() { Body = new ButtonValueCase() { Primary = isPrimaryEnabled, Secondary = !isPrimaryEnabled } };
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Simbols, message);
        }
    }
}
