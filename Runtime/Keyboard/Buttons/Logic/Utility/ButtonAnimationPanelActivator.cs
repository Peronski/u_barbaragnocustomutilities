using MessageSystem;
using UIPro;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class ButtonAnimationPanelActivator : MonoBehaviour {

        [Header("References")]
        [SerializeField] protected ButtonListener buttonListener;
        [SerializeField] protected KeyboardPanels panel;

        protected void Awake() {
            buttonListener.OnClick += OnClick;
        }

        protected virtual void OnClick(ButtonListener listener) {
            KeyboardMenu.Show(panel);
        }
    }
}
