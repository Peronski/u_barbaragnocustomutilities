using MessageSystem;
using UIPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class ButtonLogicUpper : MonoBehaviour {
        [Header("References")]
        [SerializeField] private ButtonPressedListener buttonListener;

        private bool isUpperActive = false;
        private bool isCapslockActive = false;

        protected virtual void Awake() {
            if (buttonListener != null) {
                buttonListener.OnClick += OnClick;
                buttonListener.OnHold += OnHold;
            }
        }

        private void OnEnable() {
            DefaultConfiguration();
        }

        private void DefaultConfiguration() {
            isUpperActive = false;
            isCapslockActive = false;

            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Upper, new KeyboardMessage<ButtonValueCase>() { Body = new ButtonValueCase { Capslock = isCapslockActive, Upper = isUpperActive } });
        }

        protected virtual void OnClick(ButtonListener listener) {
            if (isCapslockActive)
                isCapslockActive = false;

            isUpperActive = !isUpperActive;
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Upper, new KeyboardMessage<ButtonValueCase>() { Body = new ButtonValueCase { Capslock = isCapslockActive, Upper = isUpperActive } });
        }

        public virtual void MRTK_OnClick() {
            if (isCapslockActive)
                isCapslockActive = false;

            isUpperActive = !isUpperActive;
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Upper, new KeyboardMessage<ButtonValueCase>() { Body = new ButtonValueCase { Capslock = isCapslockActive, Upper = isUpperActive } });
        }

        protected virtual void OnHold(ButtonListener listener, PointerEventData eventData) {
            if (isCapslockActive)
                return;

            isUpperActive = true;
            isCapslockActive = true;
            ValueMessage<bool> message = new ValueMessage<bool>(isUpperActive);
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Upper, new KeyboardMessage<ButtonValueCase>() { Body = new ButtonValueCase { Capslock = isCapslockActive, Upper = isUpperActive } });
        }
    }
}
