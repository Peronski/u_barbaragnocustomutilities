using System;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class ButtonLogicStatusConfigurator : MonoBehaviour {
        [SerializeField] public ButtonLogicChange buttonLogicChange;

        public event Action OnEnableEvent;
        public event Action OnDisableEvent;
        public event Action OnValidateEvent;

        protected void OnEnable() {
            OnEnableEvent?.Invoke();
        }
        protected void OnDisable() {
            OnDisableEvent?.Invoke();
        }
        protected void OnValidate() {
            OnValidateEvent?.Invoke();
        }
    }
}
