using System.Collections.Generic;
using UIPro;
using UnityEngine;

public class ButtonAnimationContentActivator : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private bool activator;

    [Header("References")]
    [SerializeField] private ButtonListener listener;
    [SerializeField] private List<GameObject> content = new List<GameObject>();

    private void Awake() {
        if (listener != null)
            listener.OnClick += OnClick;
    }

    private void OnClick(ButtonListener obj) {

        foreach (GameObject gameObj in content) {
            gameObj.SetActive(activator);
        }
    }

    public void MRTK_OnClick() {
        foreach (GameObject gameObj in content) {
            gameObj.SetActive(activator);
        }
    }
}
