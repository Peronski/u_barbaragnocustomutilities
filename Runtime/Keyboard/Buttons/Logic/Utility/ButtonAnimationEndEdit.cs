using MessageSystem;
using UIPro;
using UnityEngine;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class ButtonAnimationEndEdit : MonoBehaviour {
        [Header("References")]
        [SerializeField] private ButtonListener buttonListener;

        private void Awake() {
            if (buttonListener != null)
                buttonListener.OnClick += OnClick;
        }

        private void OnClick(ButtonListener listener) {
            KeyboardMessage<Request> request = new KeyboardMessage<Request>() { Body = new Request() { Complete = true } };
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.EndEdit, request);
        }

        public void MRTK_OnClick() {
            KeyboardMessage<Request> request = new KeyboardMessage<Request>() { Body = new Request() { Complete = true } };
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.EndEdit, request);
        }
    }
}
