using MessageSystem;
using UIPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UpSurgeOn.MetaverseRuntime.Keyboard {
    public class ButtonLogicDelete : MonoBehaviour {

        [Header("References")]
        [SerializeField] private ButtonPressedListener buttonListener;

        private void Awake() {
            if (buttonListener != null) {
                buttonListener.OnClick += OnClick;
                buttonListener.OnHold += OnHold;
            }
        }

        private void OnClick(ButtonListener listener) {
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Delete, new KeyboardMessage<Delete>() { Body = new Delete() { DeleteAllText = false } });
        }

        public void MRTK_OnClick() {
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Delete, new KeyboardMessage<Delete>() { Body = new Delete() { DeleteAllText = false } });
        }
        private void OnHold(ButtonPressedListener listener, PointerEventData eventData) {
            MessageManager.Keyboard.SendMessage(KeyboardMessageTypes.Delete, new KeyboardMessage<Delete>() { Body = new Delete() { DeleteAllText = true } });
        }
    }
}
