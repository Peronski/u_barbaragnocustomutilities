using System;

public enum XDirections {
    Right, Left
}

public enum YDirections {
    Up, Down
}

public enum SignPanels {
    None, Start, Loading, SignIn, SingUp, ErrorSingIn, ErrorSignUp
}

public enum KeyboardPanels {
    None, Keyboard, Numbers, Letters, Simbols, Others
}

public enum SceneTypes {
    MRTK = 0, Login, Lobby, Room, Configuration
}

[Serializable]
[Flags]
public enum Axis {
    X = 1,
    Y = 2,
    Z = 4
}

public enum ActionState {
    None = 0, Started, Canceled, Performed
}

public enum Handedness {
    None, Left, Right, Both, Any
}

public enum TouchableEvent {
    None, Touch
}

public enum Prefab {
    Rig, Skull, Head, SkullDecomposable, SkullDecomposed, Brain, BrainDecomposable, BrainDecomposed
}

public enum Transition {
    Instantly, Smooth
}

public enum Authorization {
    Master, PassiveSpectator, ActiveSpectator, Ghost
}

public enum NetworkObjectState {
    Enabled = 0, Disabled = 1
}

public enum NetworkButtonState {
    Default = 0, Clicked = 1
}