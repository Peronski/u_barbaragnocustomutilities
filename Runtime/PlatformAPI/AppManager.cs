using UpSurgeOnRuntime.Network;

public class AppManager : GeneralSingleton<AppManager>
{
    protected override void Awake() {
        base.Awake();

        ServiceProvider.Initialize();
    }
}
