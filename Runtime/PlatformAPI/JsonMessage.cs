﻿using MessageSystem;
using Newtonsoft.Json;
using System;

namespace UpSurgeOnRuntime.Network {

    [Serializable]
    public class JsonMessage : MessageClass {

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("errorCode")]
        public int ErrorCode { get; set; }

        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }

        [JsonProperty("fancyError")]
        public string FancyError { get; set; }

        public override string ToString() => $"Success: {Success}; ErrorMessage: {ErrorMessage}; ErrorCode: {ErrorCode}; FancyError: {FancyError}";
    }

    public class JsonData<T> : JsonMessage where T : class {

        [JsonProperty("body")]
        public T Body { get; set; }
    }

    [Serializable]
    public class SignInRequest {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    [Serializable]
    public class SignInResponse {
        [JsonProperty("user")]
        public User User { get; set; }
        [JsonProperty("token")]
        public string Token { get; set; }
    }

    [Serializable]
    public class SignUpRequest {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
    }

    [Serializable]
    public class SignUpResponse {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("token")]
        public string Token { get; set; }
    }

    [Serializable]
    public class User {
        [JsonProperty("id")]
        public Guid Id { get; set; } = Guid.Empty;
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("role")]
        public string Role { get; set; }
        [JsonProperty("givenName")]
        public string GivenName { get; set; }
        [JsonProperty("surname")]
        public string Surname { get; set; }
        [JsonProperty("birthdayDate")]
        public DateTime BirthdayDate { get; set; }
        [JsonProperty("gender")]
        public int Gender { get; set; } = -1;
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("position")]
        public int Position { get; set; } = -1;
        [JsonProperty("discipline")]
        public int Discipline { get; set; } = -1;
        [JsonProperty("yearOfResidency")]
        public int YearOfResidency { get; set; } = -1;
        [JsonProperty("institution")]
        public string Institution { get; set; }
        public bool IsUserDataComplete() {
            if (Id.Equals(Guid.Empty)) {
                return false;
            }
            if (string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(Role) || string.IsNullOrEmpty(GivenName) || string.IsNullOrEmpty(Surname) || BirthdayDate == default
                || Gender < 0 || string.IsNullOrEmpty(Country) || Position < 0 || Discipline < 0 || YearOfResidency < 0) {
                return false;
            }
            return true;
        }
        public enum RoleTypes {
            Student,
            Admin,
        }
        public enum GenderTypes {
            Female,
            Male,
            Other,
        }
        public enum PositionTypes {
            Student,
            Resident,
            Neurosurgeon,
            University,
        }
        public enum DisciplineTypes {
            Neurosurgeon,
            Other,
        }
        public enum YearOfResidencyTypes {
            One,
            Two,
            Three,
            Four,
            Five,
            Six,
            MoreThatSix,
        }
    }
}