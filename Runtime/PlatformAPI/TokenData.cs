﻿namespace UpSurgeOnRuntime.Network {

    public class TokenData {

        public string Token { get; private set; }

        public TokenData(string token) {
            Token = token;
        }

        public bool HasToken() {
            if (string.IsNullOrEmpty(Token)) {
                return false;
            }
            return true;
        }
    }
}