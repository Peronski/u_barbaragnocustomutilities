﻿namespace MessageSystem {

    public static partial class MessageManager {

        public readonly static MessageChannel<APIMessageTypes> API = new MessageChannel<APIMessageTypes>();

    }

    public enum APIMessageTypes {
        None,
        ErrorRequestMessage,
        ErrorRequestData,
        DownloadSprite,
        DownloadFile,
        SignUp,
        SignIn,
        SignInWithToken
    }
}