﻿using BestHTTP;
using MessageSystem;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UpSurgeOnRuntime.StaticClass;

namespace UpSurgeOnRuntime.Network {

    public class NetworkAppService {

        private const string baseUrl = "https://app-upsurgeonplatform-dev.azurewebsites.net/api";
        private const string security = "/Security";
        private const string content = "/Content";

        private const string authorization = "Authorization";
        private const string bearer = "Bearer ";

        private const string contentType = "Content-Type";
        private const string applicationJson = "application/json; charset=UTF-8";
        private const string jsonEmptly = "{\"body\":\"{}\"}"; // This is because if I will send a string empty, the API will return me a error.

        private const string fancyError = "Unknown error, please contact the developer.";

        public User User { get; private set; } = new User();
        public TokenData TokenData { get; private set; }

        public int PendingRequests { get; private set; }
        public string UrlEditProfile => "https://www.upsurgeon.com/";
        public string UrlForgotPassword => "https://www.upsurgeon.com/";
        public readonly char CharDivisor = ';';

        public NetworkAppService() {
            HTTPManager.Setup();
        }

        #region Request

        private bool SendRequest(HTTPRequest request, APIMessageTypes messageType, Action<HTTPRequest, APIMessageTypes> processMessage, CancellationTokenSource cancellationTokenSource) {
            if (request == null) {
                Log.ErrorNullReference(nameof(HTTPRequest));
                return false;
            }

            request.Timeout = TimeSpan.FromSeconds(30);
            PendingRequests++;

            _ = SendRequestAsync(request, messageType, processMessage, cancellationTokenSource);

            return true;
        }

        private async Task SendRequestAsync(HTTPRequest request, APIMessageTypes messageType, Action<HTTPRequest, APIMessageTypes> processMessage, CancellationTokenSource cancellationTokenSource) {
            bool isPending = true;

            try {
                if (cancellationTokenSource != null) {
                    await request.GetHTTPResponseAsync(cancellationTokenSource.Token);

                    if (!cancellationTokenSource.IsCancellationRequested) {
                        cancellationTokenSource.Dispose();
                    }
                } else {
                    await request.GetHTTPResponseAsync();
                }

                PendingRequests--;
                isPending = false;

                if (cancellationTokenSource == null || !cancellationTokenSource.IsCancellationRequested) {
                    processMessage?.Invoke(request, messageType);
                }
            } catch (Exception exception) {
                Log.Error(exception);

                JsonMessage jsonMessage = new JsonMessage {
                    Success = false,
                    ErrorCode = request.Response != null ? request.Response.StatusCode : -1,
                    ErrorMessage = exception.Message.ToString(),
                    FancyError = fancyError,
                };

                MessageManager.API.SendMessage(APIMessageTypes.ErrorRequestMessage, jsonMessage, true);

                if (isPending) {
                    PendingRequests--;
                }
            }
        }

        private void ProcessMessage<T>(HTTPRequest request, APIMessageTypes messageType) where T : JsonMessage {
            JsonMessage jsonMessage = null;
            string errorMessage = string.Empty;

            if (request.Response == null) {
                errorMessage = "HTTPResponse is null";
            } else if (request.Response.DataAsText == null) {
                errorMessage = "HTTPResponse.DataAsText is null";
            } else if (request.Response.DataAsText == string.Empty) {
                errorMessage = "HTTPResponse.DataAsText is empty";
            }

            if (errorMessage == string.Empty) {
                jsonMessage = JsonConvert.DeserializeObject<T>(request.Response.DataAsText);
            }

            if (jsonMessage == null) {
                jsonMessage = new JsonMessage {
                    Success = false,
                    ErrorCode = request.Response != null ? request.Response.StatusCode : -1,
                    ErrorMessage = errorMessage,
                    FancyError = fancyError,
                };
            }

            if (!jsonMessage.Success) {
                string log = $"[{nameof(ProcessMessage)}] Request Failed => JsonMessage: {jsonMessage}";
                if (Validator.IsExpectedError(jsonMessage.ErrorCode)) {
                    Log.Warning(log);
                } else {
                    Log.Error($"{log}\n{request.Uri}");
                }
                MessageManager.API.SendMessage(APIMessageTypes.ErrorRequestMessage, jsonMessage, true);
                return;
            }

            InternalReadMessage(messageType, jsonMessage);
            MessageManager.API.SendMessage(messageType, jsonMessage, true);
        }

        private void ProcessData<T>(HTTPRequest request, APIMessageTypes messageType) where T : FileDataMessage, new() {
            if (request.State == HTTPRequestStates.Error) {
                Log.Error($"[{nameof(ProcessData)}] Request Failed => IRestResponse: StatusCode: {request.Response.StatusCode}; ErrorMessage: {request.Response.Message}; ErrorException: {request.Exception}");
                MessageManager.API.SendMessage(APIMessageTypes.ErrorRequestData, null, true);
                return;
            }

            T fileMessage = new T();
            fileMessage.Initialize(request.Uri.ToString(), request.Response.Data);

            InternalReadMessage(messageType, fileMessage);
            MessageManager.API.SendMessage(messageType, fileMessage, true);
        }

        public bool DownloadFile(string url, CancellationTokenSource cancellationTokenSource = null) {
            if (string.IsNullOrEmpty(url)) {
                Log.Error($"[{nameof(DownloadFile)}] Invalid URL");
                return false;
            }

            HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Get);

            return SendRequest(request, APIMessageTypes.DownloadFile, ProcessData<FileDataMessage>, cancellationTokenSource);
        }

        public bool DownloadSprite(string url, CancellationTokenSource cancellationTokenSource = null) {
            if (string.IsNullOrEmpty(url)) {
                Log.Error($"[{nameof(DownloadSprite)}] Invalid URL");
                return false;
            }

            HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Get);

            return SendRequest(request, APIMessageTypes.DownloadSprite, ProcessData<SpriteDataMessage>, cancellationTokenSource);
        }

        #endregion Request

        #region LogIn

        private void InternalReadMessage(APIMessageTypes messageType, MessageClass message) {
            switch (messageType) {
                case APIMessageTypes.SignUp: {
                        if (!(message is JsonData<SignUpResponse> jsonData)) {
                            Log.ErrorCastMessage($"{nameof(JsonData<SignUpResponse>)}");
                            return;
                        }

                        SignUpResponse signUpResponse = jsonData.Body;

                        User.Id = signUpResponse.Id;
                        TokenData = new TokenData(signUpResponse.Token);
                    }
                    break;
                case APIMessageTypes.SignIn: {
                        if (!(message is JsonData<SignInResponse> jsonData)) {
                            Log.ErrorCastMessage($"{nameof(JsonData<SignInResponse>)}");
                            return;
                        }

                        SignInResponse signInResponse = jsonData.Body;

                        User.Id = signInResponse.User.Id;
                        User.Role = signInResponse.User.Role;
                        User.GivenName = signInResponse.User.GivenName;
                        User.Surname = signInResponse.User.Surname;
                        User.BirthdayDate = signInResponse.User.BirthdayDate;
                        User.Gender = signInResponse.User.Gender;
                        User.Country = signInResponse.User.Country;
                        User.Position = signInResponse.User.Position;
                        User.Discipline = signInResponse.User.Discipline;
                        User.YearOfResidency = signInResponse.User.YearOfResidency;
                        User.Institution = signInResponse.User.Institution;

                        TokenData = new TokenData(signInResponse.Token);
                    }
                    break;
                case APIMessageTypes.SignInWithToken: {
                        if (!(message is JsonData<User> jsonData)) {
                            Log.ErrorCastMessage($"{nameof(JsonData<User>)}");
                            return;
                        }

                        User user = jsonData.Body;

                        User.Id = user.Id;
                        User.Role = user.Role;
                        User.GivenName = user.GivenName;
                        User.Surname = user.Surname;
                        User.BirthdayDate = user.BirthdayDate;
                        User.Gender = user.Gender;
                        User.Country = user.Country;
                        User.Position = user.Position;
                        User.Discipline = user.Discipline;
                        User.YearOfResidency = user.YearOfResidency;
                        User.Institution = user.Institution;
                    }
                    break;
            }
        }

        public bool HasToken() {
            if (TokenData == null || !TokenData.HasToken()) {
                SignOut();
                return false;
            }
            return true;
        }

        public void SignOut() {
            TokenData = null;
        }

        public bool SignUp(SignUpRequest signUpRequest, CancellationTokenSource cancellationTokenSource = null) {
            if (signUpRequest == null) {
                Log.ErrorNullReference(nameof(signUpRequest));
                return false;
            }

            HTTPRequest request = new HTTPRequest(new Uri(baseUrl + security + "/SignUp"), HTTPMethods.Post);
            request.AddHeader(contentType, applicationJson);
            request.RawData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(signUpRequest));

            return SendRequest(request, APIMessageTypes.SignUp, ProcessMessage<JsonData<SignUpResponse>>, cancellationTokenSource);
        }

        public bool SignIn(SignInRequest signInRequest, CancellationTokenSource cancellationTokenSource = null) {
            if (signInRequest == null) {
                Log.ErrorNullReference(nameof(signInRequest));
                return false;
            }

            HTTPRequest request = new HTTPRequest(new Uri(baseUrl + security + "/SignIn"), HTTPMethods.Post);
            request.AddHeader(contentType, applicationJson);
            request.RawData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(signInRequest));

            return SendRequest(request, APIMessageTypes.SignIn, ProcessMessage<JsonData<SignInResponse>>, cancellationTokenSource);
        }

        public bool SignInWithToken(string token, CancellationTokenSource cancellationTokenSource = null) {
            if (string.IsNullOrEmpty(token)) {
                Log.Error("The token is empty");
                return false;
            }

            TokenData = new TokenData(token);

            HTTPRequest request = new HTTPRequest(new Uri(baseUrl + security + "/SignInWithToken"), HTTPMethods.Post);
            request.AddHeader(authorization, bearer + TokenData.Token);

            return SendRequest(request, APIMessageTypes.SignInWithToken, ProcessMessage<JsonData<User>>, cancellationTokenSource);
        }

        #endregion LogIn
    }
}