﻿using MessageSystem;
using UnityEngine;

namespace UpSurgeOnRuntime.Network {

    public class FileDataMessage : MessageClass {

        public string Url { get; private set; }
        public byte[] Bytes { get; private set; }

        public virtual void Initialize(string url, byte[] bytes) {
            Url = url;
            Bytes = bytes;
        }
    }

    public class SpriteDataMessage : FileDataMessage {

        public Sprite Sprite { get; private set; }

        public override void Initialize(string url, byte[] bytes) {
            base.Initialize(url, bytes);

            Texture2D texture = new Texture2D(2, 2, TextureFormat.ARGB32, false);
            texture.LoadImage(bytes);
            texture.Apply();
            Sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        }
    }
}