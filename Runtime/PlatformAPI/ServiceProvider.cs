using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UpSurgeOnRuntime.Network {
    public static class ServiceProvider {
        public static NetworkAppService API { get; private set; }

        public static void Initialize() {
            API = new NetworkAppService();
        }
    }
}
