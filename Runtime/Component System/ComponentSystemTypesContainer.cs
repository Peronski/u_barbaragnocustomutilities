using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Barbaragno.ComponentSystem
{
    [System.Serializable]
    public struct ComponentSystemReference
    {
        [field: SerializeField] public ComponentTypes ComponentSystem { get; set; }
        [SerializeField][TypeFilter(typeof(IComponent))] public List<SerializableType> Behaviours;
    }

    public class ComponentSystemTypesContainer : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private List<ComponentSystemReference> _components = new List<ComponentSystemReference>();

        private void Awake()
        {
            ComponentSystemUtility.Inizialize(this);
        }
        private void OnDestroy()
        {
            ComponentSystemUtility.Dispose();
        }

        public bool IsComponentTypeSupported(ComponentTypes enumComponent)
        {
            if (_components == null || _components.Count <= 0)
                return false;

            ComponentSystemReference CST = _components.Where(CSType => CSType.ComponentSystem == enumComponent).FirstOrDefault();

            return CST.Behaviours != null && CST.Behaviours.Count > 0;
        }
        public bool IsComponentTypeSupported(ComponentTypes enumComponent, out ComponentSystemReference componentSystemType)
        {
            componentSystemType = default;

            if (_components == null || _components.Count <= 0)
                return false;

            ComponentSystemReference CST = _components.Where(CSType => CSType.ComponentSystem == enumComponent).FirstOrDefault();
            componentSystemType = CST;

            return CST.Behaviours != null && CST.Behaviours.Count > 0;
        }
        public bool IsComponentTypeSupported(ComponentTypes enumComponent, Type typeComponent)
        {
            if (!IsComponentTypeSupported(enumComponent, out ComponentSystemReference CST))
                return false;

            return CST.Behaviours.Contains(typeComponent);
        }
        public SerializableType GetComponentType(ComponentTypes enumComponent)
        {
            if (IsComponentTypeSupported(enumComponent, out ComponentSystemReference CST))
            {
                return CST.Behaviours.FirstOrDefault();
            }

            return null;
        }
    }

}