using System;
using UnityEngine;

namespace Barbaragno.ComponentSystem
{

    [System.Serializable]
    public enum ComponentTypes
    {
        None = 0,
        Transform = 1,
        ModelAsset = 2,
        ModelAssethandler = 3,
        MediaAsset = 4,
        MediaAssethandler = 5,
        Movable = 6,
        Selectable = 7,
        VisibleAR = 8,
        Wishable = 9
    }

    public static class ComponentSystemUtility
    {
        private static ComponentSystemTypesContainer _container = default;

        public static void Inizialize(ComponentSystemTypesContainer container)
        {
            if (container != null)
                _container = container;
        }
        public static void Dispose()
        {
            _container = null;
        }

        public static bool IsComponentTypeSupported(ComponentTypes enumComponent)
        {
            if (_container == null)
            {
                Debug.Log("[ComponentSystemTypes] - No {ComponentSystemTypesContainer} references was inizialized.");
                return false;
            }

            return _container.IsComponentTypeSupported(enumComponent);
        }
        public static bool IsComponentTypeSupported(ComponentTypes enumComponent, out ComponentSystemReference componentSystemType)
        {
            componentSystemType = default;

            if (_container == null)
            {
                Debug.Log("[ComponentSystemTypes] - No {ComponentSystemTypesContainer} references was inizialized.");
                return false;
            }

            return _container.IsComponentTypeSupported(enumComponent, out componentSystemType);
        }
        public static bool IsComponentTypeSupported(ComponentTypes enumComponent, Type typeComponent)
        {
            if (_container == null)
            {
                Debug.Log("[ComponentSystemTypes] - No {ComponentSystemTypesContainer} references was inizialized.");
                return false;
            }

            return _container.IsComponentTypeSupported(enumComponent, typeComponent);
        }
        public static Type GetComponentType(ComponentTypes enumComponent)
        {
            if (_container == null)
            {
                Debug.Log("[ComponentSystemTypes] - No {ComponentSystemTypesContainer} references was inizialized.");
                return null;
            }

            return _container.GetComponentType(enumComponent);
        }

    }

}