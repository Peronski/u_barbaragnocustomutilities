namespace Barbaragno.ComponentSystem
{

    public interface IComponent
    {
        public SerializableType Type { get; protected set; }
    }

}