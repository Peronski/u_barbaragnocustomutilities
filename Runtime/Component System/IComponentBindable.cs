using System;
using System.Collections.Generic;

namespace Barbaragno.ComponentSystem
{

    public interface IComponentBindable<T, IComponent> where T : Enum
    {
        [field: SerializeField] public Dictionary<ComponentTypes, IComponent> ComponentSystem { get; set; }

        public abstract void BindComponent(T enumComponent);
        public abstract void BindComponent<C>(T enumComponent) where C : IComponent;
        public abstract void RemoveComponent(T enumComponent);
        public abstract void RemoveComponent(T enumComponent, IComponent component);

    }

}