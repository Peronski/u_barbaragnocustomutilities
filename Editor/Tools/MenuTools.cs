using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Barbaragno.Tools {
    public static class MenuTools {
        [MenuItem("Tools/Setup/Create Default Folders")]
        public static void CreateDefaultFolders() {
            Dir("_", "Code", "Materials", "Meshes", "Data", "Plugins", "Prefabs", "Resources", "Sprites", "Textures", "Presets", "Scenes");
            AssetDatabase.Refresh();
        }

        public static void Dir(string root, params string[] directories) {
            string fullpath = Path.Combine(Application.dataPath, root);

            foreach (var dir in directories) {
                Directory.CreateDirectory(Path.Combine(fullpath, dir));
            }
        }

        [MenuItem("Tools/Setup/LoadImportedManifest")]
        public static async void LoadImportedManifest() {
            var url = GetGistUrl(".....");
            var contents = await GetContents(url);
            ReplacePackage(contents);
        }

        static string GetGistUrl(string id, string user = "Peronski") => $"https://gist.github.com/{user}/{id}#file-packages-json";

        static async Task<string> GetContents(string url) {

            HttpClient client = new HttpClient();

            HttpResponseMessage response = await client.GetAsync(url);

            string content = await response.Content.ReadAsStringAsync();

            return content;
        }

        static void ReplacePackage(string contents) {

            var existing = Path.Combine(Application.dataPath, "Packages/manifest.json");

            File.WriteAllText(existing, contents);

            UnityEditor.PackageManager.Client.Resolve();
        }

        static void InstallUnityPackage(string packageName) {
            UnityEditor.PackageManager.Client.Add($"dot.unity.{packageName}");
        }
    }
}
